# TRP-alignment
**Software for sequence- and structure-based pairwise TRP alignment** 
# WHAT IS IN THE FOLDER:
- **RepeatsDB-lite_3.6**: the main code
- Many functions called by the main code:
    - **mainFunction.py**: all the functions used for the framework of the code;
    - **insertionFunction.py**: all the functions used to detect insertions in the unit and between all the units of a region;
    - **cube_matrix.py**: additional functions  
    - **manage_unit_from_mustang.py**: all the functions used to manage the result of mustang alignment, built and draw the matrix;
    - **repeatsDB_output.py**: all the functions used when '-r, --repatsDB' output is set;
    - **second_region.py**: all the functions used for finding all regions besides the first;
    - **subprocess_function.py**: store the result of tm_align alignment in a variable instead of a file (same thing with blast);
- **configurationFile.py**: a configuration file in which is stored the paths to the programs and some constants used by the algorithm


# HOW THE CODE WORKS:
In the following link you can find a workflow of how the code works:
https://docs.google.com/presentation/d/1ENfjA5jO6pbdgQMvj-mkotHFIEPCWTzB_c8fDuR0cDc/edit?usp=sharing
## USE CASES:
 1. You want to align a protein with a specific chain to a master that you already know:

	`python3 RepeatsDB-lite_3.6.py -w /home/sara/Documents/python3 -i 1kgt -c A -o /home/sara/Documents/python3/1kgt -d /home/sara/Downloads -m 3pmoA_230_259_III_1 -g 20 -x 90 -t 76 -R 4 -T 85 -M 0.5 -O 30 -U 14`

2. You want to align **all the chains** of a protein against the library (SRUL)

	`python3 RepeatsDB-lite_2.0.py -w /home/sara/Documents/RepeatsDB_update/data -i 6yc4 -c - -o yc/6yc4 -d . -a FALSE -g 20 -x 85 -t 85 -R 10 -T 1 -M 0.44 -O 30 -U 14`
	`python3 RepeatsDB-lite_2.0.py -w /home/sara/Documents/python3 -i 1kgt -c - -o 1kgt -d /home/sara/Downloads -a FALSE -g 25 -x 85 -t 76 -R 4 -T 85 -M 0.47 -O 30 -U 14`
	
    > **NOTE**: In this case the option **-a** is set to FALSE because we want to analyze all the chain and we **not** perform a cdhit analysis

3. The simplest case is that you want to analyze **only a chain** of a protein against the library (SRUL)
	
	`python3 RepeatsDB-lite_2.0.py -w . -i 1kgt -c A -o 1kgt -d /home/sara/Downloads -g 20 -x 85 -t 85 -R 4 -T 85 -M 0.46 -O 30 -U 10`

4. You have a .fasta file ('sequence.fasta') and you want to perform sequence analysis with BLAST

    `python3 RepeatsDB-lite_3.6.py -w /home/sara/Documents -o sequence -d /home/sara/Downloads -b sequence -g 20 -x 90 -t 76 -R 4 -T 85 -M 0.5 -O 30 -U 14`
    > **NOTE**: in the case **-b** option is set, you **don't** need to put the option **-i and -c** 

5. You want to align only **one chain for cluster** of a protein and obtain a standard output 

	`python3 RepeatsDB-lite_2.0.py -w /home/sara/Documents/data -i 1kgt -c - -o 1kgt -d /home/sara/Downloads -a TRUE -g 15 -x 85 -t 85 -R 4 -T 85 -M 0.5 -O 30 -U 15 -s`

	> **NOTE**: In this case the option **-a** is set to TRUE because we want to analyze all the chain and **we perform** a cdhit analysis

## OPTIONS:
> **NOTE**: You can see the option typing **'python3 RepeatsDB-lite_3.6.py'**

| INPUT OPTION | DESCRIPTION | REQUIRED | DEFAULT | USE 
|-----|----------------|------------|---------|-------
|‘-w’ | workingdir | TRUE | - | /PATH/TO/WORKING/DIR 
|‘-i’ | pdbID | Only if -b not declared | - | Name of the protein: ex. ‘1kgt’
|‘-c’ | chain | Only if -b not declared | - | ‘-’ : if you want to analyze all the chains
| | | | | ‘A’, ‘a’, ‘1’ : only one chain
|‘-o’ | outputdir | TRUE | - | /PATH/TO/OUTPUT/DIR
|‘-d’ | databasedir | TRUE | - | /PATH/TO/DATABASE/DIR
|‘-a’ | alignment | Only if -c is ‘-’ | Every chain of the protein | TRUE, FALSE
|‘-m’ | master | FALSE | All the library (SRUL) | proteiname&chain_start_end_class_subclass
| | | | | ‘.pdb’ file
|‘-b’ | blast | FALSE | No sequence alignment | Name of the file in .fasta format (without extension)

| THRESHOLD OPTIONS | DESCRIPTION | REQUIRED | DEFAULT | USE
|-----|----------------|------------|---------|-------
|‘-g’ | gap | TRUE | - | Integer number form **0** to **length of the chain**
|‘-x’ | coverage_segment | TRUE | - | Percentual number from **1** to **100**
|‘-R’ | length_ins | TRUE | - | Integer number 
|'-t' | coverage_ins | TRUE | - | Percentual number from **1** to **100**
|'-T' | coverage_remove_unit | TRUE | - | Percentual number from **1** to **100**
|'-U' | score_unit | TRUE | - | Integer number from **1** to **100**
|'-M' | tm_score_region | TRUE | - | Float number from **0.1** to **1**
|'-O' | length_new_region | TRUE | - | Integer number

| OUTPUT OPTION | DESCRIPTION | REQUIRED | DEFAULT | USE 
|-----|----------------|------------|---------|-------
|‘-f’ | full | FALSE | - | no argument needed
|‘-s’ | standard | FALSE | - | no argument needed
|‘-r’ | repeatsDB | FALSE | - | no argument needed

- **-w** {workingdir}: set a working directory for the project in which the code stores the results ['.' in the case you want to work in the same directory of the code]
- **-i** {pdbID}: write in input the name of the protein 
- **-c** {chain}: indicate the Chain 
- **-d** {database}: indicate the folder in which there is the protein.pdb file
- **-o** {output}: set the name of an output directory in which you can find all the results for the protein 
- **-m** {master}: set the master you want
- **-a** {cdhit}: set this option if you want to analyze all the chains but if the sequences are equal 100% only one chain for cluster will be analyze [TRUE/FALSE]
- **-b** {blast}: set this option if you want to perform a sequence analysis with BLAST
- **-g** {gap}: Maximum number of gaps (-) in the chain, after the first alignment, to start the analysis
- **-x** {coverage_segment}: Pencent length of the first unit of N- and C-terminal segments after last alignment in order to be processed
- **-R** {length_ins}: Miinimum number of residues between one unit and another for the segment to be annotated as insertion
- **-t** {coverage_ins}: For each position in the line, if there are gaps in more than this percentual of units, this residue will be considered as I
- **-T** {coverage_remove_unit}: FIRST MUSTANG: after remove the insertions, if a unit has length less than this percentual value of the entire unit, remove the entire unit
- **-U** {score_unit}: tmscore x coverage: if a unit has this value under the threshold is removed
- **-M** {tm_score_region}: After obtaining all the units of a region, the average of the tmscores of all these units should be more than this value
- **-O** {length_new_region}: After removing the units belonging to a region, if the remaining chain has a length more or equal to this value, it will be analyzed
- **-f** {full}: set this output option if you want to keep the files of TMalignment to visualize the alignment with rasmol or pymol, kepp the Temporary folder, all the file generates by the code and the matrix
- **-s** {standard}: set this output option if you want to keep only the .db file and the .log file
- **-r** {repeatsDB}: set this output option if you want to generate the same output of RepeatsDBlite

# WHAT DO YOU NEED BEFORE RUNNING THE CODE:

1. Build the library (SRUL) if you don’t already have it.
2. Change the directory of **SRUL**, **tm_align**, **mustang**, **cdhit** and **blast** in the '*configurationFile*'
3. If the code run in local you need the following dependecncies:
	- TMalign (https://zhanglab.ccmb.med.umich.edu/TM-align/)
    - mustang (http://lcb.infotech.monash.edu.au/mustang/)
    - cdhit (https://github.com/weizhongli/cdhit)
    - BLAST (https://www.ncbi.nlm.nih.gov/books/NBK279671/)
    
>If you want to try a ready library (SRUL) you can download it at the GitLab repository ‘https://gitlab.com/refract-rise/repeat-unit-library/tree/master/SRUL_Cdhit’.

# OUTPUT
The output of the code will be a folder (the 'output' directory) in the working directory ('workingdir') in which there will be some files, depending on the tipe of **output option**:
1. Option **-f**:
	- one .log file
	- one 'Temporary' folder
	- one .pdb file of the protein
	- one folder for each analyzed chain [ex. A, f, 5] in which there are:
		- one matrix of the unit in .png
	 	- one .pdb file of the chain
		- one .db file with all the units 
	 	- one file named ‘units_and_correlation_final_(nameofchian).txt’ that contains the following informations:
		  	- if the chain is a repeat
	    	- the master of the chain
	    	- the version of the referred library
	    	- a list of the unit predicted
	    	- a list of the insertions found
      
2. Option **-s**:
	- one .log file
	- one folder for each analyzed chain [ex. A, f, 5] in which there is a .db file for each chain
    
3. Option **-r**:
	- one .log file
	- one .pdb/.ent file of the protein
	- the header of the .pdb file
	- one .info file
	- one folder for each analyzed chain [ex. A, f, 5] in which there are:
		- one .mapping file
		- one .pdb file of the chain
		- one .db file
		- one .reupred file in case the chain is a repeat
		- one folder for each region analyzed [ex. Region1] in which there are:
			- output .afasta of mustang
			- output .dssp for the secondary structure
			- the matrix
			- the pairwise alignment
			- the configuration file for mustang (.mus)



