#!/usr/bin/env python3

'''/home/sara/Documents/RepeatsDB-lite_2.0/RepeatsDB-lite_2.0.py
-w /home/sara/Documents/RepeatsDB_update/data
-i 6yc4
-c -
-o yc/6yc4
-d .
-a FALSE
-g 20
-x 85
-t 85
-R 10
-T 1
-M 0.44
-O 30
-U 14'''


import argparse
import getopt
import warnings
import urllib.request
import subprocess_function as sp
from Bio import BiopythonWarning
from repeatsDB_output import *

from configurationFile import *
from insertionFunction import *
from core_code import *
from mainFunction import *
from cube_matrix import *
from manage_unit_from_mustang import *
from second_region import *

warnings.simplefilter('ignore', BiopythonWarning)

'''in this code is missing the sequence alignment part and the master assignment part'''

def repeatsdb_for_each_chain(outputDir, targetname, chains, align, mustang_align, SRUL, gap, xcent, tm_score, res_ins, threshold_unit, trash_unit, ins_threshold, repeatsDB, other_region, standard):
    for i in range(len(chains)):
        region_ids, regions, units, insertions = [], [], [], []
        logging.info(f"{chains[i]}: Starting alignment with TMalign...")
        pl = open(f'{outputDir}/{chains[i]}/{targetname}_{chains[i]}.pdb').readlines()
        residues = ress_chain(outputDir, targetname, chains[i])
        ress = list(map(str, residues))

        if len(ress) < int(length_of_chain):
            logging.info(
                f"The chain {chains[i]} will not be analyzed because its length ({len(ress)}) "
                f"is less than the threshold ({length_of_chain}).")
            continue
        logging.info(f"The chain {chains[i]} has a right length ({len(ress)} > {length_of_chain}).")

        try:
            create_folder(outputDir, chains[i])
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
            pass
        fl = []
        logging.info(f'Aligning chain {chains[i]} against the SRUL...')
        for j in os.listdir(SRUL):
            fl = fl + sp.sub_tmalign(j, SRUL, outputDir, chains[i], targetname, repeatsDB, library=True)

        # Pick the higher value of tm_score for master choice
        max_score = master_choice(fl)
        df = pd.DataFrame({'unit': max_score})
        df = pd.DataFrame(df.unit.str.split('\n', 1).tolist(), columns=['unit', 'tm_score'])
        df.unit = df.unit.apply(lambda x: x.strip())
        df['tm_score'] = df['tm_score'].astype(float)
        tm_score_max = df['tm_score'].max()
        master = df[df['tm_score'] == tm_score_max]
        logging.info(f"The master for chain {chains[i]} is: {master.iloc[0]['unit']} \n\t\t\t\t\t\t with a score of: {master.iloc[0]['tm_score']}")
        master_unit = master.iloc[0]['unit']

        os.system('{0} {1}/{2} {3}/{4}/{5}_{4}.pdb > {6}/{4}/tm_output_master.txt'.format(TMalignexe, SRUL, master_unit,
                                                                                          outputDir, chains[i],
                                                                                          targetname, align))
        file_tm_master = open(f'{align}/{chains[i]}/tm_output_master.txt').readlines()
        master_seq = master_on_pdb(file_tm_master)
        start_seq = num_gap_pre_seq(master_seq, file_tm_master)
        in_seq = num_gap_in_seq(master_seq, file_tm_master)
        tot_gap = start_seq + in_seq

        coverage_master = find_coverage(file_tm_master)
        RMSD_master = extract_RMSD(file_tm_master)
        in_master = gap_master(file_tm_master, master_seq)

        if len(master_seq) < length_of_master:
            logging.info(f"The master on the chain is too short: ({len(master_seq)})")
            continue
        if int(in_master) > int(gap):
            logging.info(f"The chain {chains[i]} will not be analyzed because the number of the gap "
                         f"in the master ({int(in_master)}) is higher than the default number of gap ({gap}).")
            continue

        pre = open(f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[0]}_{ress[master_seq[0] - 1 - start_seq]}.pdb', 'w+')
        post = open(f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[master_seq[len(master_seq)-1] + 1  - tot_gap]}_{ress[len(ress)-1]}.pdb', 'w+')
        master_pdb = open(f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[master_seq[0] - start_seq]}_{ress[master_seq[len(master_seq)-1] - tot_gap]}.pdb', 'w+')

        pre_ress = pre_master_gaiden(pl, pre, ress, master_seq, start_seq)
        post_ress = post_master_gaiden(pl, post, ress, master_seq, tot_gap)
        master_ress = master_gaiden(pl, master_pdb, ress, master_seq, start_seq, tot_gap, no_master=True)

        # coverage_master = find_coverage(file_tm_master)
        # RMSD_master = extract_RMSD(file_tm_master)
        # in_master = gap_master(file_tm_master, master_seq)

        # if len(master_seq) < length_of_master:
        #     logging.info(f"The master on the chain is too short: ({len(master_seq)})")
        #     continue
        # if int(in_master) > int(gap):
        #     logging.info(f"The chain {chains[i]} will not be analyzed because the number of the gap "
        #                  f"in the master ({int(in_master)}) is higher than the default number of gap ({gap}).")
        #     continue

        percentage = (int(xcent) * len(master_seq)) / 100
        intermediate_file = []

        if not pre_ress:
            if len(post_ress) >= percentage:
                logging.info('Once aligned the master against the whole chain, enough length remains AFTER the "first unit".')
                logging.info('Realign on the chain remained...')
                intermediate_file.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(
                    int(ress[master_seq[0] - start_seq]), int(ress[master_seq[len(master_seq) - 2] - tot_gap]),
                    int(ress[master_seq[len(master_seq) - 1] - tot_gap]) - int(ress[master_seq[0] - start_seq]),
                    float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))
                iterative_function(f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[master_seq[0] - start_seq]}_{ress[master_seq[len(master_seq)-1] - tot_gap]}.pdb',
                                   f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[master_seq[len(master_seq)-1] + 1  - tot_gap]}_{ress[len(ress)-1]}.pdb',
                                   ress,
                                   align,
                                   mustang_align,
                                   chains[i],
                                   targetname,
                                   intermediate_file,
                                   percentage,
                                   other_region=False)
            else:
                logging.info('There is NOT enough chain to align more than "first unit".')
                continue
        elif not post_ress:
            if len(pre_ress) >= percentage:
                logging.info('Once aligned the master against the whole chain, enough length remains BEFORE the "first unit".')
                logging.info('Realign on the chain remained...')
                iterative_function(f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[master_seq[0] - start_seq]}_{ress[master_seq[len(master_seq)-1] - tot_gap]}.pdb',
                                   f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[0]}_{ress[master_seq[0] - 1 - start_seq]}.pdb',
                                   pre_ress,
                                   align,
                                   mustang_align,
                                   chains[i],
                                   targetname,
                                   intermediate_file,
                                   percentage,
                                   other_region=False)
                intermediate_file.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(
                    int(ress[master_seq[0] - start_seq]), int(ress[master_seq[len(master_seq) - 2] - tot_gap]),
                    int(ress[master_seq[len(master_seq) - 1] - tot_gap]) - int(ress[master_seq[0] - start_seq]),
                    float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))
            else:
                logging.info('There is NOT enough chain to align more than "first unit".')
                continue
        else:
            if (len(pre_ress) >= percentage) and (len(post_ress) >= percentage):
                logging.info('Once aligned the master against the whole chain, enough length remains BEFORE and AFTER the "first unit".')
                logging.info('Realign on the chain remained...')

                iterative_function(f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[master_seq[0] - start_seq]}_{ress[master_seq[len(master_seq)-1] - tot_gap]}.pdb',
                                   f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[0]}_{ress[master_seq[0] - 1 - start_seq]}.pdb',
                                   pre_ress,
                                   align,
                                   mustang_align,
                                   chains[i],
                                   targetname,
                                   intermediate_file,
                                   percentage,
                                   other_region=False)
                intermediate_file.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(
                    int(ress[master_seq[0] - start_seq]), int(ress[master_seq[len(master_seq) - 2] - tot_gap]),
                    int(ress[master_seq[len(master_seq) - 1] - tot_gap]) - int(ress[master_seq[0] - start_seq]),
                    float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))
                iterative_function(f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[master_seq[0] - start_seq]}_{ress[master_seq[len(master_seq)-1] - tot_gap]}.pdb',
                                   f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[master_seq[len(master_seq)-1] + 1  - tot_gap]}_{ress[len(ress)-1]}.pdb',
                                   post_ress,
                                   align,
                                   mustang_align,
                                   chains[i],
                                   targetname,
                                   intermediate_file,
                                   percentage,
                                   other_region=False)

            elif (len(pre_ress) >= percentage) and (len(post_ress) <= percentage):
                iterative_function(f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[master_seq[0] - start_seq]}_{ress[master_seq[len(master_seq)-1] - tot_gap]}.pdb',
                                   f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[0]}_{ress[master_seq[0] - 1 - start_seq]}.pdb',
                                   pre_ress,
                                   align,
                                   mustang_align,
                                   chains[i],
                                   targetname,
                                   intermediate_file,
                                   percentage,
                                   other_region=False)
                intermediate_file.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(
                    int(ress[master_seq[0] - start_seq]), int(ress[master_seq[len(master_seq) - 2] - tot_gap]),
                    int(ress[master_seq[len(master_seq) - 1] - tot_gap]) - int(ress[master_seq[0] - start_seq]),
                    float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))

            elif (len(pre_ress) <= percentage) and (len(post_ress) >= percentage):
                intermediate_file.append(f'{int(ress[master_seq[0] - start_seq])}\t'
                                         f'{int(ress[master_seq[len(master_seq) - 2] - tot_gap])}\t'
                                         f'{int(ress[master_seq[len(master_seq) - 1] - tot_gap]) - int(ress[master_seq[0] - start_seq])}\t'
                                         f'{float(master.iloc[0]["tm_score"])}\t'
                                         f'{float(RMSD_master)}\t'
                                         f'{tot_gap}\t'
                                         f'{in_master}\t'
                                         f'{coverage_master} %\t'
                                         f'master_file\n')
                iterative_function(f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[master_seq[0] - start_seq]}_{ress[master_seq[len(master_seq)-1] - tot_gap]}.pdb',
                                   f'{align}/{chains[i]}/{targetname}_{chains[i]}_{ress[master_seq[len(master_seq)-1] + 1  - tot_gap]}_{ress[len(ress)-1]}.pdb',
                                   post_ress,
                                   align,
                                   mustang_align,
                                   chains[i],
                                   targetname,
                                   intermediate_file,
                                   percentage,
                                   other_region=False)

            elif (len(pre_ress) < percentage) and (len(post_ress) < percentage):

                logging.info('There is NOT enough chain to align more than "first unit".')
                continue
            else:
                pass

        data, repeat_data = repeat_file(intermediate_file, outputDir, chains[i], tm_score, other_region=False)

        logging.info(f'Clean the units if there is any under the threshold (tm_score x coverage) {threshold_unit}')
        data, junk_names = remove_unit_under_threshold(data, targetname, chains[i], threshold_unit)
        logging.info(f'Find insertion between units...\n\t\t\t\t\t\t If the insertion '
                     f'is shorter than {res_ins} unify the unit, else annotate it as an insertion.')
        ins, data_new = find_insertion_between_units(data, ress, res_ins)
        create_file_for_mustang(data_new, mustang_align, ress, pl, targetname, chains[i])
        data = pd.DataFrame(data_new)
        if data.empty:
            logging.info('There is no unit therefore no other things will be done.')
            pass
        else:
            # if isinstance(data, list):
            #     if not data:
            #         logging.info('There is no unit therefore no other things will be done.')
            #         pass
            # elif isinstance(data, pd.DataFrame):
            #     if data.empty:
            #         logging.info('There is no unit therefore no other things will be done.')
            #         pass
            logging.info(f'Create the mustang description file for the first alignment '
                f'to find insertion in the unit and remove units that have a length of insertion '
                f'more than {100 - int(trash_unit)}% of the length of the unit itself.')
            len_must_des_file = mustang_description_file(mustang_align, chains[i], targetname)
            if len_must_des_file == 1:
                pass
            else:

            ######### MUSTANG REALIGNMENT ##########
                data_after_removing, id_ins = first_mustang_to_remove_fake_unit(mustang_align, chains[i], targetname, data,
                                                                 repeatsDB, ins_threshold, res_ins, trash_unit)
                data_after_removing.END = data_after_removing.END.astype(int)
                ins_2, data_with_ins_between = find_insertion_between_units(data_after_removing, ress, res_ins)
                data_1=pd.DataFrame(data_with_ins_between)
                if data_1.empty:
                    logging.info('There is no unit therefore no other things will be done.')
                    ins = pd.DataFrame(columns=['START', 'END', 'LENGTH'])
                    data_2 = data_after_removing
                    pass
                else:
                    logging.info('Remove the unit that are too far apart: more than {} residues.'.format(unit_far_apart))
                    remove_too_far_apart_unit(data_after_removing, data_1, mustang_align, targetname, chains[i])
                    data_insertion_between_units, ins = insertion_between_unit_manage_data(data_1, ress, ins_2, res_ins)
                    data_2 = pd.DataFrame(data_insertion_between_units)
                    if data_2.empty:
                        logging.info('There is no unit therefore no other things will be done.')
                all_insertion = order_all_insertion(ins, id_ins)
                ave_tmscore = average(data_2)
                if round(ave_tmscore, 2) < float(tm_score):
                    print('This region will not be analyzed cause the mean of the tm_score of the units is less than {1}: {0}'.format(round(ave_tmscore, 2), tm_score))
                    logging.info('This region will not be analyzed cause the mean of the tm_score of the units is less than {1}: {0}'.format(round(ave_tmscore, 2), tm_score))
                    pass
                else:
                    logging.info('This region will be analyzed. Mean of tm_score:{}'.format(round(ave_tmscore, 2)))
                    region_ids.append(f'{data_2["START"][0]}\t{data_2["END"][len(data_2)-1]}')
                    header = assemble_repeat_file(master_unit, data_2, outputDir, chains[i], all_insertion, targetname, repeatsDB, standard)
                    final_ins = last_management_ins(data_2, all_insertion)

                    ############### WRITE .DB FILE #################
                    logging.info('Create the .db file for the chain {0} of the protein {1}'.format(chains[i], targetname))
                    make_db_file(targetname, chains[i], final_ins, data_2, master_seq, ress, outputDir, master_unit)
                ress_pre, ress_post = possibly_other_region(data_2, targetname, outputDir, chains[i], pl, ress, align, mustang_align)

                if len(ress_pre) < int(other_region) or not ress_pre:
                    if len(ress_post) > int(other_region):
                        create_second_region_folder(outputDir, chains[i], align, ress_post, mustang_align)
                        logging.info('Seems that there is another region to analyze...')
                        iterative_core(outputDir, targetname, standard, align, mustang_align, ress_post,
                                       chains[i], SRUL, repeatsDB, xcent, tm_score, other_region, res_ins, threshold_unit,
                                       ins_threshold, trash_unit, region_ids)
                    if len(ress_post) < int(other_region) or not ress_post:
                        pass
                elif len(ress_pre) > int(other_region):
                    if len(ress_post) < int(other_region) or not ress_post:
                        create_second_region_folder(outputDir, chains[i], align, ress_pre, mustang_align)
                        logging.info('Seems that there is another region to analyze...')
                        iterative_core(outputDir, targetname, standard, align, mustang_align, ress_pre,
                                       chains[i], SRUL, repeatsDB, xcent, tm_score, other_region, res_ins, threshold_unit,
                                       ins_threshold, trash_unit, region_ids)
                    else:
                        logging.info('Seems that there is another region to analyze...')
                        create_second_region_folder(outputDir, chains[i], align, ress_pre, mustang_align)
                        create_second_region_folder(outputDir, chains[i], align, ress_post, mustang_align)
                        iterative_core(outputDir, targetname, standard, align, mustang_align, ress_pre,
                                       chains[i], SRUL, repeatsDB, xcent, tm_score, other_region, res_ins, threshold_unit,
                                       ins_threshold, trash_unit, region_ids)
                        iterative_core(outputDir, targetname, standard, align, mustang_align, ress_post,
                                       chains[i], SRUL, repeatsDB, xcent, tm_score, other_region, res_ins, threshold_unit,
                                       ins_threshold, trash_unit, region_ids)
                else:
                    logging.info('No other regions to analyze for this chain.')
                    pass
        logging.info('Analysis for the chain {0} of the protein {1} ended'.format(chains[i], targetname))
        if os.path.isfile('{}/stdout_mustang_{}.log'.format(mustang_align, chains[i])):
            copyfile('{}/stdout_mustang_{}.log'.format(mustang_align, chains[i]),
                     '{}/stdout_mustang_{}.log'.format(outputDir, chains[i]))
        else:
            pass
        unify_db_all_regions(outputDir, chains[i], targetname)
        os.system(f'rm {outputDir}/{chains[i]}/{targetname}_{chains[i]}.db')
        # os.system(f'mv {outputDir}/{chains[i]}/{targetname}{chains[i]}.db {outputDir}/{chains[i]}/{targetname}_{chains[i]}.db')


def enrichoutput_and_repeatsdboutput(chains, standard, targetname, outputDir, repeatsDB):
    for i in range(len(chains)):
        if standard:
            logging.info('It seems you set -s, --standard option; the only output you will obtain will be the .db file')
            for f in os.listdir('{0}/{1}'.format(outputDir, chains[i])):
                if f == '{}{}.db'.format(targetname, chains[i]):
                    os.system('cp {0}/{1}/{2}{1}.db {0}/{2}{1}.db'.format(outputDir, chains[i], targetname))
                else:
                    pass

                for g in os.listdir('{}'.format(outputDir)):
                    if not g.endswith('.db'):
                        if not g.endswith('.log'):
                            try:
                                os.remove('{}/{}'.format(outputDir, g))
                            except IsADirectoryError:
                                pass

            os.system('rm -r {}/{}'.format(outputDir, chains[i]))

        elif repeatsDB:
            if os.path.exists('{0}/{1}/{2}{1}.db'.format(outputDir, chains[i], targetname)):
                reg_and_unit, residues_chain, model = built_matrix(outputDir, chains[i], targetname)
                write_info_file(targetname, outputDir, chains[i], reg_and_unit)
                dssp = mapp_file(targetname, chains[i], outputDir, residues_chain, model)
                header_pdb(outputDir, targetname)
                make_region_one(outputDir, targetname, chains[i])
                os.system(f'mv {outputDir}/{chains[i]} {outputDir}/_{chains[i]}')
            else:
                os.system('rm -r {}/{}'.format(outputDir, chains[i]))
        else:
            pass
    pass


def main():

    ########################################################################
    '''cannot put the SRUL in the config file, don't know why...'''
    SRUL = '/home/sara/Documents/SRUL_version/NEWVERSION/SRUL.0.4_class_sub'
    # SRUL = '/home/sara/Documents/SRUL_version/provaSRUL.0.4'
    ########################################################################

    args = parse_arguments()
    print(args)
    if args.standard:
        standard = True
    else:
        standard = False
    if args.repeatsDB:
        repeatsDB = True
    else:
        repeatsDB = False

    outputDir = '{0}/{1}'.format(args.workingdir, args.output)
    if os.path.exists(outputDir):
        pass
    else:
        os.makedirs(outputDir)
    initial_time = time.time()

    LOG = '{0}/reupred.log'.format(outputDir)

    setup_logging(args, LOG)
    logging.info("Starting the prediction.....")
    logging.info(
        "Running predictor: {0} \n\t\t\t\t\t\t "
        "The directory where it is: {1} \n\t\t\t\t\t\t "
        "The chain is: {2} \n\t\t\t\t\t\t "
        "The output directory is: {3}".format(
            args.inputf, args.workingdir, args.chain, outputDir))

    # dowload the .pdb file if it's not in the database
    download_pdb(outputDir, args.inputf, args.database)
    temp_dir = '{0}/Temporary'.format(outputDir)
    align = '{0}/TMalign_alignment'.format(temp_dir)
    mustang_align = '{0}/mustang_alignment'.format(temp_dir)

    # check if the library (SRUL) is in the working directory. If not, set a symbolic link and remove it after analyzing all the chains
    if os.path.isdir("{0}/{1}".format(args.workingdir, os.path.basename(os.path.normpath(SRUL)))):
        logging.info('The library RUNE is in the directory {}.'.format(args.workingdir))
        pass
    try:
        os.symlink(SRUL, '{0}/{1}'.format(args.workingdir, os.path.basename(os.path.normpath(SRUL))))
        logging.info(
            'The library RUNE is not in the directory {0}; a symbolic link will be created in the directory {0}'.format(
                args.workingdir))
    except FileExistsError:
        logging.info('There is a symbolic link to the library; nothing else will be done.')

    os.chdir('{0}'.format(args.workingdir))
    SRUL = './{0}'.format(os.path.basename(os.path.normpath(SRUL)))

    # count the number of chains in .pdb file and create .pdb files for every chain or just the chian you want to analyze
    logging.info('Counting the number of the chains in the PDB ({}) target...'.format(args.inputf))

    if args.chain == '-':
        chains_pdb = count_n_chains(outputDir, args.inputf, args.chain, chain_flag = False)
        chain_pdb = is_chain_protein_or_no(chains_pdb, args.inputf, outputDir)
        logging.info('Will be analyzed the chains {0}. The others are not proteins.'.format(chain_pdb))
        if not chain_pdb:
            logging.warning('The program will exit because none of the chains of this protein could be analyzed')
            print('The program will exit because none of the chains of this protein could be analyzed')
            print('END')
            logging.info('END')
            sys.exit()
        if len(chain_pdb) == 1:
            chains = chain_pdb
        else:
            if args.a == 'TRUE':
                cha = align_sequence_cdhit(args.inputf, outputDir)
                logging.info('The cdhit analysis will be done.')
                chains = [value for value in chain_pdb if value in cha]
            else:
                logging.info('No cdhit analysis will be done.')
                chains = chain_pdb
    else:
        chains_pdb = count_n_chains(outputDir, args.inputf, args.chain, chain_flag = True)
        if args.chain in chains_pdb:
            specific_chain = is_chain_protein_or_no(args.chain, args.inputf, outputDir)
        else:
            logging.info('The chain provided "{0}" is not in the PDB file.')
            sys.exit()
        chains = specific_chain

    repeatsdb_for_each_chain(outputDir, args.inputf, chains, align, mustang_align, SRUL, args.gap,
                             args.xcent,
                             args.tmscore, args.resins, args.thresholdunit, args.trashunit,
                             args.insthreshold, repeatsDB, args.otherregion, standard)

    enrichoutput_and_repeatsdboutput(chains, standard, args.inputf, outputDir, repeatsDB)
    shutil.rmtree(f'{outputDir}/Temporary')

    return 0


def parse_arguments(initargs=None):
    if initargs is None:
        initargs = []
    parser = argparse.ArgumentParser(description="RepeatsDB-lite 2.0: a new algorithm to predict repeated proteins "
                                                 "from structure.")
    parser.add_argument("-w", "--workingdir", required=True, help="Set a working directory for the project")
    parser.add_argument("-i", "--inputf", help="Write in input the name of the protein")
    parser.add_argument("-c", "--chain", help="Indicate the Chain. PUT THE OPTION IN UPPER CHARACTERS")
    parser.add_argument("-d", "--database", required=True,
                        help="Indicate the folder in which there is the protein.pdb file")
    parser.add_argument("-o", "--output", required=True,
                        help="Set an output directory in which you can find all the results")
    parser.add_argument('-m',
                        help="Set the master you want; '.pdb' file or in the format "
                             "'pdbnamechain_start_end_classsubclass'")
    parser.add_argument("-b", help="Set this option if you want to perform a sequence analysis with BLAST")
    parser.add_argument('-a',
                        help="Set this option if you want to analyze all the chains but if the sequences are equal "
                             "100% only one chain will be analyze")
    # parser.add_argument('-F', help="Align using Fr-TMalign instead of TM-align", action='store_true')
    # parser.add_argument('-f', "--full",
    #                     help="Keep the files of TMalignment to visualize the alignment with rasmol or pymol",
    #                     action='store_true')
    parser.add_argument('-s', "--standard", help="Keep only the .db file", action='store_true')
    parser.add_argument('-r', "--repeatsDB", help="Generate the same output of RepeatsDBlite", action='store_true')


    parser.add_argument('-g', "--gap", required=True,
                        help='Maximum number of gaps (-) in the chain, after the first alignment, to start the analysis')
    parser.add_argument('-x', "--xcent", required=True,
                        help='Pencent length of the first unit of N- and C-terminal segments after last alignment in '
                             'order to be processed')
    parser.add_argument('-R', "--resins", required=True,
                        help='Miinimum number of residues between one unit and another for the segment to be annotated '
                             'as insertion')
    parser.add_argument('-t', "--insthreshold", required=True,
                        help='For each position in the line, if there are gaps in more than this percentual of units, '
                             'this residue will be considered as I')
    parser.add_argument('-T', "--trashunit", required=True,
                        help='FIRST MUSTANG: after remove the insertions, if a unit has length less than this percentual '
                             'value of the entire unit, remove the entire unit')
    parser.add_argument('-U', "--thresholdunit", required=True,
                        help='tmscore x coverage: if a unit has this value under the threshold is removed')
    parser.add_argument('-M', "--tmscore", required=True,
                        help='After obtaining all the units of a region, the average of the tmscores of all these units '
                             'should be more than this value')
    parser.add_argument('-O', "--otherregion", required=True,
                        help='After removing the units belonging to a region, if the remaining chain has a length more '
                             'or equal to this value, it will be analyzed')

    if not initargs or len(sys.argv) > 1:
        args = parser.parse_args()
    else:
        args = parser.parse_args(initargs)
    return args


def setup_logging(args, LOG):
    logging.basicConfig(filename=LOG, level=logging.INFO,
                        format='%(asctime)s | %(levelname)-5.5s | %(message)s',
                        datefmt='%d/%m/%Y %H:%M:%S')
    logging.info(f'{os.path.basename(__file__)} started')
    logging.debug(f'Arguments: {vars(args)}')
    return


if __name__ == '__main__':
    sys.exit(main())