#!/usr/bin/env python3

import argparse
import getopt
import warnings
import urllib.request
from Bio import BiopythonWarning
from repeatsDB_output import *

from configurationFile import *
from insertionFunction import *
from core_code import *
from mainFunction import *
from cube_matrix import *
from manage_unit_from_mustang import *
from second_region import *

warnings.simplefilter('ignore', BiopythonWarning)
import subprocess_function as sp

#print('sys.version')
# print Bio.__version__
argv = sys.argv[1:]
parser = argparse.ArgumentParser(description="New version of reupred.py.")
parser.add_argument("-w", "--workingdir", required=True, help="Set a working directory for the project")
parser.add_argument("-i", "--inputf", help="Write in input the name of the protein")
parser.add_argument("-c", "--chain", help="Indicate the Chain. PUT THE OPTION IN UPPER CHARACTERS")
parser.add_argument("-d", "--database", required=True, help="Indicate the folder in which there is the protein.pdb file")
parser.add_argument("-o", "--output", required=True, help="Set an output directory in which you can find all the results")
parser.add_argument('-m', help="Set the master you want; '.pdb' file or in the format 'pdbnamechain_start_end_classsubclass'")
parser.add_argument("-b", help="Set this option if you want to perform a sequence analysis with BLAST")
parser.add_argument('-a', help="Set this option if you want to analyze all the chains but if the sequences are equal 100% only one chain will be analyze")
#parser.add_argument('-F', help="Align using Fr-TMalign instead of TM-align", action='store_true')
parser.add_argument('-f', "--full", help="Keep the files of TMalignment to visualize the alignment with rasmol or pymol", action='store_true')
parser.add_argument('-s', "--standard", help="Keep only the .db file", action='store_true')
parser.add_argument('-r', "--repeatsDB", help="Generate the same output of RepeatsDBlite", action='store_true')

parser.add_argument('-g', "--gap", required=True, help='Maximum number of gaps (-) in the chain, after the first alignment, to start the analysis')
parser.add_argument('-x', "--xcent", required=True, help='Pencent length of the first unit of N- and C-terminal segments after last alignment in order to be processed')
parser.add_argument('-R', "--resins", required=True, help='Miinimum number of residues between one unit and another for the segment to be annotated as insertion')
parser.add_argument('-t', "--insthreshold", required=True, help='For each position in the line, if there are gaps in more than this percentual of units, this residue will be considered as I')
parser.add_argument('-T', "--trashunit", required=True, help='FIRST MUSTANG: after remove the insertions, if a unit has length less than this percentual value of the entire unit, remove the entire unit')
parser.add_argument('-U', "--thresholdunit", required=True, help='tmscore x coverage: if a unit has this value under the threshold is removed')
parser.add_argument('-M', "--tmscore", required=True, help='After obtaining all the units of a region, the average of the tmscores of all these units should be more than this value')
parser.add_argument('-O', "--otherregion", required=True, help='After removing the units belonging to a region, if the remaining chain has a length more or equal to this value, it will be analyzed')


args = parser.parse_args()

opts, args = getopt.getopt(argv, "hi:w:c:d:o:rm:b:a:g:x:t:R:T:M:O:U:fs", ["inputf=", "workingdir=", "chain=", "database=", "output=", "blast="])

for opt, arg in opts:
    if opt == "-h":
        print('parse_args')
        sys.exit()
    elif opt in ("-w", "--workingdir"):
        wdirOriginal = arg
    elif opt in ("-i", "--inputf"):
        targetname = arg
    elif opt in ("-c", "--chain"):
        chain = arg
    elif opt in ("-d", "--database"):
        database = arg
    elif opt in ("-o", "--output"):
        outputDir = arg
        odir = arg + 'Temporary/'
    elif opt == "-a":
        aliseq = arg
    elif opt in "-b":
        sequence = arg
    elif opt == "-m":
        masterCode = arg
    elif opt in ('-g', "--gap"):
        gap=arg
    elif opt in ('-x', "--xcent"):
        xcent=arg
    elif opt in ('-t', "--insthreshold"):
        ins_threshold = arg
    elif opt in ('-R', "--resins"):
        res_ins = arg
    elif opt in ('-T', "--trashunit"):
        trash_unit = arg
    elif opt in ('-M', "--tmscore"):
        tm_score = arg
    elif opt in ('-O', "--otherregion"):
        len_new_region = arg
    elif opt in ('-U', "--thresholdunit"):
        threshold_unit = arg


for i in opts:
    a, b = i
    if a == '-f':
        enrichout = True
    else:
        enrichout = False
    if a == '-s':
        standard = True
    else:
        standard = False
    if a == '-r':
        repeatsDB = True
    else:
        repeatsDB = False

#create_second_config_file(gap, xcent, ins_threshold, res_ins, tm_score, threshold_unit, len_new_region)

outputDir = '{0}/{1}'.format(wdirOriginal, outputDir)
# print(repeatsDB)

if os.path.exists(outputDir):
    pass
else:
    os.makedirs(outputDir)

initial_time = time.time()
if repeatsDB:
    LOG = '{0}/reupred.log'.format(outputDir)  # , num_of_chain)
    logging.basicConfig(filename=LOG, level=logging.INFO, format='%(asctime)s | %(name)s | %(threadName)s | %(levelname)s | %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
    logging.info("Starting the prediction.....")
    logging.info("Running predictor: {0} \n\t\t\t\t\t\t The directory where it is: {1} \n\t\t\t\t\t\t The chain is: {2} \n\t\t\t\t\t\t The output directory is: {3}".format(targetname, wdirOriginal, chain, outputDir))
else:
    try:
        sequence
        targetname = os.path.basename(os.path.normpath(outputDir))
        LOG = '{0}/reupred_{1}.log'.format(outputDir, targetname)#, num_of_chain)
        logging.basicConfig(filename=LOG, level=logging.INFO, format='%(asctime)s | %(name)s | %(threadName)s | %(levelname)s | %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
        logging.info("Starting the prediction.....")
        logging.info("Running predictor: {0} \n\t\t\t\t\t\t The directory where it is: {1} \n\t\t\t\t\t\t The output directory is: {2}".format(targetname, wdirOriginal, outputDir))
    except NameError:
        LOG = '{0}/reupred_{1}.log'.format(outputDir, targetname)#, num_of_chain)
        logging.basicConfig(filename=LOG, level=logging.INFO, format='%(asctime)s | %(name)s | %(threadName)s | %(levelname)s | %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
        logging.info("Starting the prediction.....")
        logging.info("Running predictor: {0} \n\t\t\t\t\t\t The directory where it is: {1} \n\t\t\t\t\t\t The chain is: {2} \n\t\t\t\t\t\t The output directory is: {3}".format(targetname, wdirOriginal, chain, outputDir))

try:
    sequence
    targetname = os.path.basename(os.path.normpath(outputDir))
    print('Align the sequence provided with the PDB database')
    logging.info('Align the sequence provided with the PDB database...')
    if os.path.exists('{0}/databasePDB.fasta'.format(outputDir)):
        print("The PDB database already exist")
        logging.info('The PDB database already exists in the folder {}'.format(outputDir))
    else:
        print('Download the PDB database...')
        logging.info('The PDB database doesn"t exist in the folder {}...\nDownload the PDB database'.format(outputDir))
        url = 'ftp://ftp.wwpdb.org/pub/pdb/derived_data/pdb_seqres.txt' #download a FASTA file containing all PDB sequences
        urllib.request.urlretrieve(url, '{0}/databasePDB.fasta'.format(outputDir))
    print('Starting analysis with BLAST...')
    logging.info('Starting analysis with BLAST...')
    bl = sp.sub_blast(sequence, outputDir)
    print(bl)
    alignment = pd.DataFrame([line.split('\t') for line in bl])
    sub_align = alignment[alignment.ix[:, 2] == alignment.ix[:, 2][0]]

    targetname = sub_align.ix[:, 1][0].split('_')[0]
    chain = sub_align.ix[:, 1][0].split('_')[1]

    ###########The option for the output format is -outfmt
    # 0 = pairwise
    # 1 = query-anchored showing identities
    # 2 = query-anchored no identities
    # 3 = flat query-anchored, show identities
    # 4 = flat query-anchored, no identities
    # 5 = XML Blast output
    # 6 = tabular
    # 7 = tabular with comment lines
    # 8 = Text ASN.1
    # 9 = Binary ASN.1
    # 10 = Comma-separated values
    #os.system('blastp -query {0}.fasta -subject ./{1}/databasePDB.fasta -out ./{1}/{0}_blast_result.txt -outfmt 6'.format(sequence, outputDir))
except NameError:
    print('No BLAST analysis will be done.')
    logging.info('No BLAST analysis will be done.')

# dowload the .pdb file if it's not in the database
download_pdb(outputDir, targetname, database)

temp_dir = '{0}/Temporary'.format(outputDir)
align = '{0}/TMalign_alignment'.format(temp_dir)
mustang_align = '{0}/mustang_alignment'.format(temp_dir)
############ DON'T REMOVE ################
#remove_temp_folder(temp_dir)
create_folder(outputDir)

# check if the library (SRUL) is in the working directory. If not, set a symbolic link and remove it after analyzing all the chains
if os.path.isdir("{0}/{1}".format(wdirOriginal, os.path.basename(os.path.normpath(SRUL)))):
    logging.info('The library RUNE is in the directory {}.'.format(wdirOriginal))
    pass
try:
    os.symlink(SRUL, '{0}/{1}'.format(wdirOriginal, os.path.basename(os.path.normpath(SRUL))))
    logging.info('The library RUNE is not in the directory {0}; a symbolic link will be created in the directory {0}'.format(wdirOriginal))
except FileExistsError:
    logging.info('There is a symbolic link to the library; nothing else will be done.')

os.chdir('{0}'.format(wdirOriginal))
SRUL = './{0}'.format(os.path.basename(os.path.normpath(SRUL)))

# count the number of chians in .pdb file and create .pdb files for every chain
logging.info('Counting the number of the chains in the PDB ({}) target...'.format(targetname))
chains_pdb = count_n_chains(outputDir, targetname, chain=True)

# check if analyze all chains or only one chain
if chain == '-':
    chain_pdb = is_chain_protein_or_no(chains_pdb, targetname, outputDir)
    logging.info('Will be analyzed the chains {0}. The others are not proteins.'.format(chain_pdb))
    if not chain_pdb:
        logging.warning('The program will exit because none of the chains of this protein could be analyzed')
        print('The program will exit because none of the chains of this protein could be analyzed')
        if standard == True:
            for f in os.listdir('{}'.format(outputDir)):
                if not f.endswith('.log'):
                    os.system('rm -r {}/{}'.format(outputDir, f))
        print('END')
        logging.info('END')
        sys.exit()
    if len(chain_pdb) == 1:
       chains=chain_pdb
    else:
        if aliseq == 'TRUE':
            cha = align_sequence_cdhit(targetname, outputDir)
            logging.info('The cdhit analysis will be done.')
            chains = [value for value in chain_pdb if value in cha]
        else:
            logging.info('No cdhit analysis will be done.')
            chains = chain_pdb
else:
    if chain in chains_pdb:
        specific_chain = is_chain_protein_or_no(chain, targetname, outputDir)
    else:
        logging.info('The chain provided "{0}" is not in the PDB file.')
        sys.exit()
    chains = specific_chain

print(chains)
# Align every chain with SRUL
region_ids=[]
for i in range(len(chains)):
    print(chains[i])
   # print(repeatsDB)
    logging.info("{}: Starting alignment with TMalign...".format(chains[i]))
    pl = open('{0}/{2}/{1}_{2}.pdb'.format(outputDir, targetname, chains[i])).readlines()
    pdb_chain = PDBParser().get_structure('{0}_{1}'.format(targetname, chains[i]), '{0}/{2}/{1}_{2}.pdb'.format(outputDir, targetname, chains[i]))
    ress = ress_chain(outputDir, targetname, chains[i])
    print(ress)
    # exit()
    if len(ress) < int(length_of_chain):
        logging.info("The chain {0} will not be analyzed because its length ({1}) is less than the threshold ({2}).".format(chains[i], len(ress), length_of_chain))
        continue
    logging.info("The chain {0} will be analyzed; it has a right length ({1} > {2}).".format(chains[i], len(ress), length_of_chain))
########################### change after  #############################
    try:
        os.makedirs('{0}/{1}'.format(align, chains[i]))
        os.makedirs('{0}/{1}'.format(mustang_align, chains[i]))
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        pass

    # in case you have provided a master
    try:
        masterCode
        logging.info('You have provided a master in the option: {}'.format(masterCode))
        master_unit = master_given(masterCode, outputDir, database)
        if os.path.isfile('{0}/{1}'.format(SRUL, master_unit)):
            logging.info('The master you provided ({0}) exist in the folder: {1}'.format(master_unit, SRUL))

            fl = sp.sub_tmalign(TMalignexe, master_unit, SRUL, outputDir, chains[i], targetname, enrichout, library=True)
            pass
        else:
            logging.info('The master you provided ({0}) doesn t exist in the folder {1}. The .pdb file will be dowloaded and the master will be obtained.'.format(master_unit, SRUL))
            master_name = master_unit.split('_')[0][:4]
            urlretrieve('http://files.rcsb.org/download/{0}.pdb'.format(master_name), '{0}/{1}.pdb'.format(outputDir, master_name))
            ress_start = master_unit.split('_')[1]
            ress_end = master_unit.split('_')[2]
            master_chain = master_unit.split('_')[0][4:]
            save_pdb_master_chain(master_name, outputDir, master_chain)
            mf = open('{0}/{1}_{2}.pdb'.format(outputDir, master_name, master_chain)).readlines()
            master_file = open('{0}/{1}'.format(outputDir, master_unit), 'w+')
            master_pdb = PDBParser().get_structure('{0}_{1}'.format(master_name, master_chain), '{0}/{1}_{2}.pdb'.format(outputDir, master_name, master_chain))
            resseqs_master = [residue.id[1] for residue in master_pdb.get_residues()]
            prova = master_gaiden(mf, master_file, resseqs_master, 0, ress_start, ress_end, no_master=False)
            fl = sp.sub_tmalign(TMalignexe, master_unit, 0, outputDir, chains[i], targetname, enrichout, library=False)
            SRUL = outputDir
        ######################### change 'database' option depending on where you have the master ############################
    except NameError:
        fl = []
        logging.info('You have not provided any master, therefore align chain {0} against the SRUL...'.format(chains[i]))
        for j in os.listdir(SRUL):
            fl = fl + sp.sub_tmalign(TMalignexe, j, SRUL, outputDir, chains[i], targetname, enrichout, library=True)

    # Pick the higher value of tm_score for master choice
    ###################################### CORE CODE ##########################################################################
    max_score = master_choice(fl)
    df = pd.DataFrame({'unit': max_score})
    df = pd.DataFrame(df.unit.str.split('\n', 1).tolist(), columns=['unit', 'tm_score'])

    df.unit = df.unit.apply(lambda x: x.strip())
    df['tm_score'] = df['tm_score'].astype(float)
    df.to_csv('{0}/{1}/prova_df_max_tmscore.txt'.format(outputDir, chains[i]), sep='\t', index=False)
    tm_score_max = df['tm_score'].max()
    master = df[df['tm_score'] == tm_score_max]
    logging.info("The master for chain {0} is: {1} \n\t\t\t\t\t\t with a score of: {2}".format(chains[i], master.iloc[0]['unit'], master.iloc[0]['tm_score']))
    master_unit = master.iloc[0]['unit']

    pre = open('{0}/{1}/{2}_{1}_pre_0.pdb'.format(align, chains[i], targetname), 'w+')
    post = open('{0}/{1}/{2}_{1}_post_0.pdb'.format(align, chains[i], targetname), 'w+')
    master_pdb = open('{0}/{1}/{2}_{1}_temp.pdb'.format(align, chains[i], targetname), 'w+')
    os.system('{0} {1}/{2} {3}/{4}/{5}_{4}.pdb > {6}/{4}/tm_output_master.txt'.format(TMalignexe, SRUL, master_unit, outputDir, chains[i], targetname, align))
    # pdb_chain = PDBParser().get_structure('{0}_{1}'.format(targetname, chains[i]), '{0}/{2}/{1}_{2}.pdb'.format(outputDir, targetname, chains[i]))

    file_tm_master = open('{1}/{0}/tm_output_master.txt'.format(chains[i], align)).readlines()
    # resseqs = [residue.id[1] for residue in pdb_chain.get_residues()]
    resseqs = ress
    # print(resseqs)
    #exit()

    master_seq = master_on_pdb(file_tm_master)
    start_seq = num_gap_pre_seq(master_seq, file_tm_master)
    in_seq = num_gap_in_seq(master_seq, file_tm_master)
    tot_gap = start_seq + in_seq

    pre_master, residue_pre = pre_master_gaiden(pl, pre, resseqs, master_seq, start_seq)
    post_master, residue_post = post_master_gaiden(pl, post, resseqs, master_seq, tot_gap)
    protein_master, master_residue = master_gaiden(pl, master_pdb, resseqs, master_seq, start_seq, tot_gap, no_master=True)
    # print(pre_master, post_master, protein_master)
    # exit()

    coverage_master = find_coverage(file_tm_master)
    RMSD_master = extract_RMSD(file_tm_master)
    in_master = gap_master(file_tm_master, master_seq)
    if len(master_seq) < length_of_master:
        logging.info("The master on the chain is too short: ({0})".format(len(master_seq)))
        continue
    if int(in_master) > int(gap):
        logging.info("The chain {0} will not be analyzed because the number of the gap in the master ({2}) is higher than the default number of gap ({1}).".format(chains[i], gap, int(in_master)))
        continue

    percentage = (int(xcent) * len(master_seq)) / 100
    intermediate_file = []
    # pdb_pre_m = PDBParser().get_structure('{0}_{1}_pre'.format(targetname, chains[i]), '{0}/{1}/{2}_{1}_pre_0.pdb'.format(align, chains[i], targetname))
    # pdb_post_m = PDBParser().get_structure('{0}_{1}_post'.format(targetname, chains[i]), '{0}/{1}/{2}_{1}_post_0.pdb'.format(align, chains[i], targetname))
    # resseqs_pre = [residue.id[1] for residue in pdb_pre_m.get_residues()]
    # resseqs_post = [residue.id[1] for residue in pdb_post_m.get_residues()]
    # resseqs_pre = pre_master
    # resseqs_post = post_master
    # print(post_master)
    # print(resseqs_pre)
    # print(resseqs_post)
    # exit()

    if not residue_pre:
        if len(residue_post) >= percentage:
            logging.info('Once aligned the master against the whole chain, enough length remains AFTER the "first unit".')
            logging.info('Realign on the chain remained...')
            intermediate_file.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(int(resseqs[master_seq[0] - start_seq]), int(resseqs[master_seq[len(master_seq) - 2] - tot_gap]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]) - int(resseqs[master_seq[0] - start_seq]), float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))
            iterative_function('{0}/{1}/{2}_{1}_temp.pdb'.format(align, chains[i], targetname), '{0}/{1}/{2}_{1}_post_0.pdb'.format(align, chains[i], targetname), residue_post, align, mustang_align, chains[i], targetname, intermediate_file, percentage, other_region=False)
        else:
            logging.info('There is NOT enough chain to align more than "first unit".')
            continue

    elif not residue_post:
        if len(residue_pre) >= percentage:
            logging.info('Once aligned the master against the whole chain, enough length remains BEFORE the "first unit".')
            logging.info('Realign on the chain remained...')
            iterative_function('{0}/{1}/{2}_{1}_temp.pdb'.format(align, chains[i], targetname), '{0}/{1}/{2}_{1}_pre_0.pdb'.format(align, chains[i], targetname), residue_pre, align,  mustang_align, chains[i], targetname, intermediate_file, percentage, other_region=False)
            intermediate_file.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(int(resseqs[master_seq[0] - start_seq]), int(resseqs[master_seq[len(master_seq) - 2] - tot_gap]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]) - int(resseqs[master_seq[0] - start_seq]), float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))
        else:
            logging.info('There is NOT enough chain to align more than "first unit".')
            continue

    else:
        if len(residue_pre) >= percentage and len(residue_post) >= percentage:
            logging.info('Once aligned the master against the whole chain, enough length remains BEFORE and AFTER the "first unit".')
            logging.info('Realign on the chain remained...')

            iterative_function('{0}/{1}/{2}_{1}_temp.pdb'.format(align, chains[i], targetname), '{0}/{1}/{2}_{1}_pre_0.pdb'.format(align, chains[i], targetname), residue_pre, align, mustang_align, chains[i], targetname, intermediate_file, percentage, other_region=False)
            intermediate_file.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(int(resseqs[master_seq[0] - start_seq]), int(resseqs[master_seq[len(master_seq) - 2] - tot_gap]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]) - int(resseqs[master_seq[0] - start_seq]), float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))
            iterative_function('{0}/{1}/{2}_{1}_temp.pdb'.format(align, chains[i], targetname), '{0}/{1}/{2}_{1}_post_0.pdb'.format(align, chains[i], targetname), residue_post, align, mustang_align, chains[i], targetname, intermediate_file, percentage, other_region=False)

        if len(residue_pre) >= percentage and len(residue_post) <= percentage:
            iterative_function('{0}/{1}/{2}_{1}_temp.pdb'.format(align, chains[i], targetname), '{0}/{1}/{2}_{1}_pre_0.pdb'.format(align, chains[i], targetname), residue_pre, align, mustang_align, chains[i], targetname, intermediate_file, percentage, other_region=False)
            intermediate_file.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(int(resseqs[master_seq[0] - start_seq]), int(resseqs[master_seq[len(master_seq) - 2] - tot_gap]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]) - int(resseqs[master_seq[0] - start_seq]), float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))

        if len(residue_pre) <= percentage and len(residue_post) >= percentage:
            intermediate_file.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(int(resseqs[master_seq[0] - start_seq]), int(resseqs[master_seq[len(master_seq) - 2] - tot_gap]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]) - int(resseqs[master_seq[0] - start_seq]), float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))
            iterative_function('{0}/{1}/{2}_{1}_temp.pdb'.format(align, chains[i], targetname), '{0}/{1}/{2}_{1}_post_0.pdb'.format(align, chains[i], targetname), residue_post, align, mustang_align, chains[i], targetname, intermediate_file, percentage, other_region=False)

        if len(residue_pre) < percentage and len(residue_post) < percentage:
            logging.info('There is NOT enough chain to align more than "first unit".')
            continue

    # print(data)
    # exit()
    data, repeat_data = repeat_file(intermediate_file, outputDir, chains[i], tm_score, other_region=False)#, resseqs, targetname)#, manage_ins)
    print(data)
    exit()

    logging.info('Clean the units if there is any under the threshold (tm_score x coverage) {}'.format(threshold_unit))
    data, junk_names = remove_unit_under_threshold(data, targetname, chains[i], threshold_unit)
    logging.info('Find insertion between units...\n\t\t\t\t\t\t If the insertion is shorter than {} unify the unit, else annotate it as an insertion.'.format(res_ins))
    ins, data = find_insertion_between_units(data, resseqs, res_ins)
    create_file_for_mustang(data, mustang_align, resseqs, pl, targetname, chains[i])
    # print(type(data))
    #exit()
    if isinstance(data, list):
        if not data:
            logging.info('There is no unit therefore no other things will be done.')
            pass
    elif isinstance(data, pd.DataFrame):
        if data.empty:
            logging.info('There is no unit therefore no other things will be done.')
            pass
        else:
            logging.info('Create the mustang description file for the first alignment to find insertion in the unit and remove units that have a length of insertion more than {}% of the legth of the unit itself.'.format(100 - int(trash_unit)))
            len_must_des_file = mustang_description_file(mustang_align, chains[i], targetname)
            if len_must_des_file == 1:
                pass
            ######### MUSTANG REALIGNMENT ##########
            data, id_ins = first_mustang_to_remove_fake_unit(mustang_align, chains[i], targetname, data, repeatsDB, ins_threshold, res_ins, trash_unit)
            # print(data)
            #exit()
            data.END = data.END.astype(int)
            ins_2, data_1 = find_insertion_between_units(data, resseqs, res_ins)
            if isinstance(data_1, list):
                if not data_1:
                    logging.info('There is no unit therefore no other things will be done.')
                    ins = pd.DataFrame(columns=['START', 'END', 'LENGTH'])
                    data_2=data
                    pass
            elif isinstance(data_1, pd.DataFrame):
                if data_1.empty:
                    logging.info('There is no unit therefore no other things will be done.')
                    ins = pd.DataFrame(columns=['START', 'END', 'LENGTH'])
                    data_2 = data
                    pass
                else:
                    logging.info('Remove the unit that are too far apart: more than {} residues.'.format(unit_far_apart))
                    remove_too_far_apart_unit(data, data_1, mustang_align, targetname, chains[i])
                    data_2, ins = insertion_between_unit_manage_data(data_1, resseqs, ins_2, res_ins)
                    if isinstance(data_2, list):
                        if not data_2:
                            logging.info('There is no unit therefore no other things will be done.')
                    elif isinstance(data_2, pd.DataFrame):
                        if data_2.empty:
                            logging.info('There is no unit therefore no other things will be done.')

            all_insertion = order_all_insertion(ins, id_ins)
            ###################################
            ave_tmscore = average(data_2)
            # print(ave_tmscore)
            # print(data_2)
            # exit()
            if round(ave_tmscore, 2) < float(tm_score):
                print('This region will not be analyzed cause the mean of the tm_score of the units is less than {1}: {0}'.format(round(ave_tmscore, 2), tm_score))
                logging.info('This region will not be analyzed cause the mean of the tm_score of the units is less than {1}: {0}'.format(round(ave_tmscore, 2), tm_score))
                pass
            else:
                logging.info('This region will be analyzed. Mean of tm_score:{}'.format(round(ave_tmscore, 2)))
                # region_ids.append(residue)
                region_ids.append(f'{data_2["START"][0]}\t{data_2["END"][len(data_2)-1]}')
                # print(data_2['START'][0], data_2['END'][len(data_2)-1])
                # exit()
                # if repeatsDB:
                #     header = assemble_repeat_file(master_unit, data_2, '{0}/_{1}/Region_{2}_{3}'.format(outputDir, chains, ress[0], ress[len(ress)-1]), chains, all_insertion, targetname, repeatsDB)
                # else:
                #############################################
                header = assemble_repeat_file(master_unit, data_2, outputDir, chains[i], all_insertion, targetname)

                # if enrichout or repeatsDB:
                #     TMscore = built_matrix(mustang_align, chains[i], targetname, repeatsDB)
                #     draw_matrix(TMscore, outputDir, chains[i], targetname, threshold_unit)
                # else:
                #     pass

                final_ins = last_management_ins(data_2, all_insertion)

                ############### WRITE .DB FILE #################
                logging.info('Create the .db file for the chain {0} of the protein {1}'.format(chains[i], targetname))
                make_db_file(targetname, chains[i], final_ins, data_2, master_seq, resseqs, outputDir, master_unit)

            ress_pre, ress_post = possibly_other_region(data_2, targetname, outputDir, chains[i], pl, resseqs, align, mustang_align)  # , 0)

            if len(ress_pre) < int(len_new_region) or not ress_pre:
                if len(ress_post) > int(len_new_region):
                    create_second_region_folder(outputDir, chains[i], align, ress_post, mustang_align)
                    logging.info('Seems that there is another region to analyze...')
                    # region_ids.append(ress_post[0])
                    # print(ress_post[0], ress_post[len(ress_post)-1])
                    # counter +=1
                    # print(ress_post)
                    iterative_core(outputDir, targetname, enrichout, standard, align, mustang_align, ress_post, chains[i], SRUL, repeatsDB, xcent, tm_score, len_new_region, res_ins, threshold_unit, ins_threshold, trash_unit, region_ids)
                if len(ress_post) < int(len_new_region) or not ress_post:
                    pass
                    # print('non c"è una seconda regione')
            elif len(ress_pre) > int(len_new_region):
                if len(ress_post) < int(len_new_region) or not ress_post:
                    create_second_region_folder(outputDir, chains[i], align, ress_pre, mustang_align)
                    logging.info('Seems that there is another region to analyze...')
                    # print(ress_pre[0], ress_pre[len(ress_pre)-1])
                    # region_ids.append(f'{ress_pre[0]}\t{ress_pre[len(ress_pre)-1]}')
                    # counter += 1
                    # print(ress_pre)
                    iterative_core(outputDir, targetname, enrichout, standard, align, mustang_align, ress_pre, chains[i], SRUL, repeatsDB, xcent, tm_score, len_new_region, res_ins, threshold_unit, ins_threshold, trash_unit, region_ids)
                else:
                    logging.info('Seems that there is another region to analyze...')
                    # counter += 1
                    # print(ress_pre, ress_post)
                    create_second_region_folder(outputDir, chains[i], align, ress_pre, mustang_align)
                    create_second_region_folder(outputDir, chains[i], align, ress_post, mustang_align)
                    iterative_core(outputDir, targetname, enrichout, standard, align, mustang_align, ress_pre, chains[i], SRUL, repeatsDB, xcent, tm_score, len_new_region, res_ins, threshold_unit, ins_threshold, trash_unit, region_ids)
                    iterative_core(outputDir, targetname, enrichout, standard, align, mustang_align, ress_post, chains[i], SRUL, repeatsDB, xcent, tm_score, len_new_region, res_ins, threshold_unit, ins_threshold, trash_unit, region_ids)
            else:
                logging.info('No other regions to analyze for this chain.')
                pass
    logging.info('Analysis for the chain {0} of the protein {1} ended'. format(chains[i], targetname))
    if os.path.isfile('{}/stdout_mustang_{}.log'.format(mustang_align, chains[i])):
        copyfile('{}/stdout_mustang_{}.log'.format(mustang_align, chains[i]), '{}/stdout_mustang_{}.log'.format(outputDir, chains[i]))
    else:
        pass
    unify_db_all_regions(outputDir, chains[i], targetname)

for i in range(len(chains)):
    if standard:
        logging.info('It seems you set -s, --standard option; the only output you will obtain will be the .db file')
        for f in os.listdir('{0}/{1}'.format(outputDir, chains[i])):
            if f == '{}{}.db'.format(targetname, chains[i]):
                os.system('cp {0}/{1}/{2}{1}.db {0}/{2}{1}.db'.format(outputDir, chains[i], targetname))
            else:
                pass

            for g in os.listdir('{}'.format(outputDir)):
                if not g.endswith('.db'):
                    if not g.endswith('.log'):
                        try:
                            os.remove('{}/{}'.format(outputDir, g))
                        except IsADirectoryError:
                            pass

        os.system('rm -r {}/{}'.format(outputDir, chains[i]))

    elif repeatsDB or enrichout:
        if os.path.exists('{0}/{1}/{2}{1}.db'.format(outputDir, chains[i], targetname)):
            reg_and_unit, residues_chain, model = built_matrix(outputDir, chains[i], targetname)
            write_info_file(targetname, outputDir, chains[i], reg_and_unit)
            dssp = mapp_file(targetname, chains[i], outputDir, residues_chain, model)
            header_pdb(outputDir, targetname)
            make_region_one(outputDir, targetname, chains[i], reg_and_unit)
            exit()

            obtain_zipped_folder_region_one(outputDir, chains[i], targetname)
            for f in os.listdir('{}'.format(outputDir)):
                if f.endswith('.fa') or f.endswith('.clstr') or f.endswith('.txt'):
                    os.remove('{}/{}'.format(outputDir, f))
            exit()
        else:
            os.system('rm -r {}/{}'.format(outputDir, chains[i]))
        os.system('rm -r {}/{}'.format(outputDir, chains[i]))

    else:
        pass
#exit()
os.system('rm -r {}/Temporary'.format(outputDir))

print('END')
logging.info('END')
