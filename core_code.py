from mainFunction import *
from second_region import *
from configurationFile import *


def iterator_core_function(outputDir, SRUL, chains, align, targetname, x, xcent):
    fl = []
    for j in os.listdir(SRUL):
        fl = fl + sp.sub_tmalign(j, SRUL, outputDir, chains, targetname, repeatsDB=False, regionx=False, library=True)
    max_score = master_choice(fl)
    df = pd.DataFrame({'unit': max_score})
    df = pd.DataFrame(df.unit.str.split('\n', 1).tolist(), columns=['unit', 'tm_score'])
    df.unit = df.unit.apply(lambda x: x.strip())
    df['tm_score'] = df['tm_score'].astype(float)
    df.to_csv('{0}/{1}/prova_df_max_tmscore.txt'.format(outputDir, chains), sep='\t', index=False)
    tm_score_max = df['tm_score'].max()
    master = df[df['tm_score'] == tm_score_max]
    master_unit = master.iloc[0]['unit']

    os.system('{0} {1}/{2} {3}/{4}/Region{7}/{5}_{4}_reg{7}.pdb > {6}/{4}/Region{7}/tm_output_master.txt'.format(TMalignexe, SRUL, master_unit, outputDir, chains, targetname, align, x))

    pl = open('{0}/{2}/{1}_{2}.pdb'.format(outputDir, targetname, chains)).readlines()
    pre = open('{0}/{1}/{2}_{1}_pre_0.pdb'.format(align, chains, targetname), 'w+')
    post = open('{0}/{1}/{2}_{1}_post_0.pdb'.format(align, chains, targetname), 'w+')
    master_pdb = open('{0}/{1}/{2}_{1}_temp.pdb'.format(align, chains, targetname), 'w+')
    pdb_chain = PDBParser().get_structure('{0}_{1}'.format(targetname, chains), '{0}/{2}/{1}_{2}.pdb'.format(outputDir, targetname, chains))
    resseqs = [residue.id[1] for residue in pdb_chain.get_residues()]

    file_tm_master = open('{1}/{0}/tm_output_master.txt'.format(chains, align)).readlines()

    master_seq = master_on_pdb(file_tm_master)
    start_seq = num_gap_pre_seq(master_seq, file_tm_master)
    in_seq = num_gap_in_seq(master_seq, file_tm_master)
    tot_gap = start_seq + in_seq

    pre_master = pre_master_gaiden(pl, pre, resseqs, master_seq, start_seq)
    post_master = post_master_gaiden(pl, post, resseqs, master_seq, tot_gap)
    protein_master = master_gaiden(pl, master_pdb, resseqs, master_seq, start_seq, tot_gap, no_master=True)
    copyfile('{0}/{1}/{2}_{1}_temp.pdb'.format(align, chains[i], targetname), '{0}/{1}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chains[i], targetname, resseqs[master_seq[0] - start_seq], resseqs[master_seq[len(master_seq) - 1] - tot_gap]))
    coverage_master = find_coverage(file_tm_master)
    RMSD_master = extract_RMSD(file_tm_master)
    in_master = gap_master(file_tm_master, master_seq)

    percentage = (xcent * len(master_seq)) / 100
    intermediate_file = []
    pdb_pre_m = PDBParser().get_structure('{0}_{1}_pre'.format(targetname, chains[i]), '{0}/{1}/{2}_{1}_pre_0.pdb'.format(align, chains[i], targetname))
    pdb_post_m = PDBParser().get_structure('{0}_{1}_post'.format(targetname, chains[i]), '{0}/{1}/{2}_{1}_post_0.pdb'.format(align, chains[i], targetname))
    resseqs_pre = [residue.id[1] for residue in pdb_pre_m.get_residues()]
    resseqs_post = [residue.id[1] for residue in pdb_post_m.get_residues()]

