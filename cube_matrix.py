from tables import table

from mainFunction import *
from insertionFunction import *
import re
from Bio.PDB import PDBIO, Select
import configurationFile


def unify_unit(insertion, data, s, t, resseqs):
    idx_START = data['START'][data['START'] == s].index.tolist()
    idx_END = data['END'][data['END'] == t].index.tolist()
    range=resseqs[resseqs.index(insertion[0]):resseqs.index(s)]
    if len(range) % 2 == 0:
        half = len(range)//2
        data.START[idx_START] = data.START[idx_START].replace(data.START[idx_START], range[half:][0])
        data.END[idx_END] = data.END[idx_END].replace(data.END[idx_END], range[:half][len(range[:half]) - 1])
    else:
        if len(range) == 1:
            if (int(data.LENGTH[idx_END].values[0]) > int(data.LENGTH[idx_START].values[0])) or (int(data.LENGTH[idx_END].values[0]) == int(data.LENGTH[idx_START].values[0])):
                data.START[idx_START]=data.START[idx_START].replace(data.START[idx_START], range[-1])#lunghezza dell'end maggiore dai metà +1
            elif int(data.LENGTH[idx_END].values[0]) < int(data.LENGTH[idx_START].values[0]):
                data.END[idx_END]=data.END[idx_END].replace(data.END[idx_END], range[0])
        else:
            if (int(data.LENGTH[idx_END].values[0]) > int(data.LENGTH[idx_START].values[0])) or (int(data.LENGTH[idx_END].values[0]) == int(data.LENGTH[idx_START].values[0])):
                data.START[idx_START]=data.START[idx_START].replace(data.START[idx_START], range[-1])#lunghezza dell'end maggiore dai metà +1
                range_1 = resseqs[resseqs.index(range[0]):resseqs.index(data.START[idx_START].values[0])]
                half = len(range_1)//2
                data.START[idx_START] = data.START[idx_START].replace(data.START[idx_START], range_1[half:][0])
                data.END[idx_END] = data.END[idx_END].replace(data.END[idx_END], range_1[:half][len(range_1[:half]) - 1])
            elif int(data.LENGTH[idx_END].values[0]) < int(data.LENGTH[idx_START].values[0]):
                data.END[idx_END]=data.END[idx_END].replace(data.END[idx_END], range[0])
                range_1 = resseqs[resseqs.index(range[1]): resseqs.index(data.START[idx_START].values[0])]
                half = len(range_1) // 2
                data.START[idx_START] = data.START[idx_START].replace(data.START[idx_START], range_1[half:][0])
                data.END[idx_END] = data.END[idx_END].replace(data.END[idx_END], range_1[:half][len(range_1[:half]) - 1])

    len_new_unit_END = resseqs[resseqs.index(data.START[idx_END].values[0]): resseqs.index(data.END[idx_END].values[0])]
    len_new_unit_START = resseqs[resseqs.index(data.START[idx_START].values[0]): resseqs.index(data.END[idx_START].values[0])]
    data.LENGTH[idx_END] = data.LENGTH[idx_END].replace(data.LENGTH[idx_END], len(len_new_unit_END) + 2)
    data.LENGTH[idx_START] = data.LENGTH[idx_START].replace(data.LENGTH[idx_START], len(len_new_unit_START) + 2)

    return data


def replace_with_right_file_for_mustang(mustang_align, chain, data, outputDir, targetname, bef_aft_start, bef_aft_end):
    if not bef_aft_start and not bef_aft_end:
        pass
    else:
        if os.path.basename(os.path.normpath(mustang_align)) != 'mustang_alignment':
            pl = open('{0}/{2}/{1}_{2}.pdb'.format(outputDir, targetname, chain)).readlines()
            for i in os.listdir('{0}/'.format(mustang_align)):
                start_name = i.split('_')[1].rstrip()
                end_name = i.split('_')[2].split('.')[0].rstrip()
                for s, t in zip(data.START, data.END):
                    if int(start_name) != s:
                        if int(end_name) == t:
                            os.remove('{0}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, start_name, end_name))
                            new_unit_pdb = open('{0}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, s, t),'w+')
                            for line in pl:
                                if line[22:26] != '':
                                    if s <= int(line[22:26]) <= t:  # add a = in the first <=
                                        if line[0:6] == "ATOM  ":
                                            new_unit_pdb.write(line)
                            new_unit_pdb.close()
                    if int(start_name) == s:
                        if int(end_name) != t:
                            os.remove('{0}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, start_name, end_name))
                            new_unit_pdb=open('{0}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, s, t),'w+')
                            for line in pl:
                                if line[22:26] != '':
                                    if int(s) <= int(line[22:26]) <= int(t):  # add a = in the first <=
                                        if line[0:6] == "ATOM  ":
                                            new_unit_pdb.write(line)
                            new_unit_pdb.close()
                for j in bef_aft_start:
                    for k in bef_aft_end:
                        if j.split('\t')[0] == start_name:
                            if k.split('\t')[0] == end_name:
                                os.remove('{0}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, start_name, end_name))
                                # print('{0}/{1}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, j.split('\t')[1], k.split('\t')[1]))
                                new_unit_pdb = open('{0}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, j.split('\t')[1], k.split('\t')[1]), 'w+')
                                for line in pl:
                                    if line[22:26] != '':
                                        if int(j.split('\t')[1]) <= int(line[22:26]) <= int(k.split('\t')[1]):  # add a = in the first <=
                                            if line[0:6] == "ATOM  ":
                                                new_unit_pdb.write(line)
                                new_unit_pdb.close()
        else:
            pl = open('{0}/{2}/{1}_{2}.pdb'.format(outputDir, targetname, chain)).readlines()
            for i in os.listdir('{0}/{1}/'.format(mustang_align, chain)):
                if i.startswith('{0}{1}_'.format(targetname, chains)):
                    start_name = i.split('_')[1].rstrip()
                    end_name = i.split('_')[2].split('.')[0].rstrip()
                    for s, t in zip(data.START, data.END):
                        if int(start_name) != s:
                            if int(end_name) == t:
                                os.remove('{0}/{1}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, start_name, end_name))
                                new_unit_pdb = open('{0}/{1}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, s, t), 'w+')
                                for line in pl:
                                    if line[22:26] != '':
                                        if s <= int(line[22:26]) <= t:  # add a = in the first <=
                                            if line[0:6] == "ATOM  ":
                                                new_unit_pdb.write(line)
                                new_unit_pdb.close()
                        if int(start_name) == s:
                            if int(end_name) != t:
                                os.remove('{0}/{1}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, start_name, end_name))
                                new_unit_pdb = open('{0}/{1}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, s, t), 'w+')
                                for line in pl:
                                    if line[22:26] != '':
                                        if int(s) <= int(line[22:26]) <= int(t):  # add a = in the first <=
                                            if line[0:6] == "ATOM  ":
                                                new_unit_pdb.write(line)
                                new_unit_pdb.close()
                    for j in bef_aft_start:
                        for k in bef_aft_end:
                            if j.split('\t')[0] == start_name:
                                if k.split('\t')[0] == end_name:
                                    os.remove('{0}/{1}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, start_name, end_name))
                                    new_unit_pdb = open('{0}/{1}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chain, targetname, j.split('\t')[1], k.split('\t')[1]), 'w+')
                                    for line in pl:
                                        if line[22:26] != '':
                                            if int(j.split('\t')[1]) <= int(line[22:26]) <= int(k.split('\t')[1]):  # add a = in the first <=
                                                if line[0:6] == "ATOM  ":
                                                    new_unit_pdb.write(line)
                                    new_unit_pdb.close()
