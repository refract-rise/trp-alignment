import os, sys, re, logging, urllib, errno, shutil
import time
#from string import upper
from operator import itemgetter
from itertools import groupby
import itertools
#from Bio.PDB import PDBParser, PDBIO
from shutil import copyfile
from configurationFile import *
from subprocess_function import *
from manage_unit_from_mustang import *
from cube_matrix import *
import pandas as pd
import numpy as np

pd.set_option('mode.chained_assignment', None)


def repeat_file(intermediate_file, outputDir, chains, tm_score, other_region=False):#, resseqs, targetname):#, manage_ins):
    if len(intermediate_file) < 2:
        data = pd.DataFrame({'unit': intermediate_file})
        data = data.replace('\n', ' ', regex=True)
        data = pd.DataFrame(data.unit.str.split('\t', 8).tolist(), columns=['START', 'END', 'LENGTH', 'TM_SCORE', 'RMSD', 'GAP_UNIT', 'GAP_MASTER', 'COVERAGE', 'PDB_FILE'])
        df=data
    else:
        df = pd.DataFrame({'unit': intermediate_file})
        df = df.replace('\n', ' ', regex=True)
        df = pd.DataFrame(df.unit.str.split('\t', 8).tolist(), columns=['START', 'END', 'LENGTH', 'TM_SCORE', 'RMSD', 'GAP_UNIT', 'GAP_MASTER', 'COVERAGE', 'PDB_FILE'])
        df['sort'] = df['START'].str.extract('(\d+)', expand=False).astype(int)
        df.sort_values('sort', inplace=True, ascending=True)
        df = df.drop('sort', axis=1) #delete column "sort"
        df.TM_SCORE = df.TM_SCORE.astype(float)
        df.START = df.START.astype(str)
        # if other_region==True:
        #     df.to_csv('{0}/before_filter_units_{1}.txt'.format(outputDir, chains), sep='\t', index=False)
        # else:
        #     df.to_csv('{0}/{1}/before_filter_units_{1}.txt'.format(outputDir, chains), sep='\t', index=False)
        data = df[df.TM_SCORE > float(tm_score)].reset_index(drop=True)
        if data.empty:
            data = pd.DataFrame(columns=['START', 'END', 'LENGTH', 'TM_SCORE', 'RMSD', 'GAP_UNIT', 'GAP_MASTER', 'COVERAGE', 'PDB_FILE'])
            data = data.append(pd.Series(['-', '-', '-', '-', '-', '-', '-', '-', '-'], ['START', 'END', 'LENGTH', 'TM_SCORE', 'RMSD', 'GAP_UNIT', 'GAP_MASTER', 'COVERAGE', 'PDB_FILE']), ignore_index=True)
        # changed 'data' with 'df' that is the not-filtered dataframe based on tm-score
    return df, data


def find_insertion_between_units(data, resseqs, res_ins):
    ins, data_new = [], []
    resseqs = list(map(str, resseqs))
    for s, t in zip(data.START[1:], data.END):
        insertion = resseqs[resseqs.index(str(t))+1:resseqs.index(str(s))]
        if not insertion:
            ins.append('-\t-\t-')
            data_new = data
        else:
            if len(insertion) >= int(res_ins):
                ins.append('{0}\t{1}\t{2}\n'.format(insertion[0], insertion[len(insertion)-1], len(insertion)))
                data_new = data
            else:
                data_new = unify_unit(insertion, data, s, t, resseqs)#,  targetname, manage_ins)
    ins = pd.DataFrame({'unit': ins})
    ins = ins.replace('\n', ' ', regex=True)
    if ins.empty:
        ins = pd.DataFrame(columns=['START', 'END', 'LENGTH'])
        ins = ins.append(pd.Series(['-', '-', '-'], ['START', 'END', 'LENGTH']), ignore_index=True)
    else:
        ins = pd.DataFrame(ins.unit.str.split('\t', 2).tolist(), columns=['START', 'END', 'LENGTH'])
    return ins, data_new


def first_mustang_to_remove_fake_unit(mustang_align, chains, targetname, data, repeatsDB, ins_threshold, res_ins, trash_unit):
    insertion_df = define_insertion_mustang(mustang_align, chains, targetname, repeatsDB, ins_threshold)
    id_ins = identify_insertion(insertion_df, res_ins)
    id_ins = pd.DataFrame({'Name': id_ins})
    id_ins = pd.DataFrame(id_ins.Name.str.split('\t', 4).tolist(), columns=['Name', 'start', 'end', 'length', 'mark_ins'])
    junk_name, data = manage_unit_from_mustang(id_ins, data, trash_unit)
    # print('aisjdbkajb', data)
    remove_useless_mustang_file(mustang_align, junk_name, chains)
    if not junk_name:
        data.reset_index(drop=True, inplace=True)
        data['insertion_mark'] = '0'
        for k in range(len(id_ins)):
            if id_ins.Name[k] == '-':
                pass
            else:
                for j in range(len(data)):
                    if int(id_ins.iloc[k].Name.split('_', 2)[1]) == int(data.START[j]):
                        data.insertion_mark[j] = '1'
    else:
        if len(data) <= 1:
            return data, id_ins
        else:
            data, id_ins = second_mustang_alignment(data, chains, mustang_align, targetname, repeatsDB, ins_threshold, res_ins)
    return data, id_ins


def second_mustang_alignment(data, chains, mustang_align, targetname, repeatsDB, ins_threshold, res_ins):
    len_mustang_desc_file = mustang_description_file(mustang_align, chains, targetname)
    if len_mustang_desc_file == 1:
        id_ins = []
        return data, id_ins
    else:
        insertion_df = define_insertion_mustang(mustang_align, chains, targetname, repeatsDB, ins_threshold)
        id_ins = identify_insertion(insertion_df, res_ins)
        id_ins = pd.DataFrame({'Name': id_ins})
        id_ins = pd.DataFrame(id_ins.Name.str.split('\t', 4).tolist(), columns=['Name', 'start', 'end', 'length', 'mark_ins'])
        data.reset_index(drop=True, inplace=True)
        data['insertion_mark'] = '0'
        for k in range(len(id_ins)):
            if id_ins.Name[k] == '-':
                pass
            else:
                for j in range(len(data)):
                    if int(id_ins.iloc[k].Name.split('_', 2)[1]) == int(data.START[j]):
                        data.insertion_mark[j] = '1'
        return data, id_ins


def order_all_insertion(insertion, id_ins):
    if len(id_ins) <1 :
        all_insertion=insertion
    else:
        id_ins = id_ins.drop('Name', axis=1)
        id_ins = id_ins.drop('mark_ins', axis=1)
        id_ins.columns = ['START', 'END', 'LENGTH']
        all_insertion = pd.concat([insertion, id_ins], axis=0, ignore_index=True)
        all_insertion = all_insertion[all_insertion.LENGTH != 0]
        all_insertion.reset_index(drop=True, inplace=True)
    if (len(all_insertion) <= 1) and (all_insertion['START'][0] == '-'):
        return all_insertion
    else:
        all_insertion['sort'] = all_insertion['START'].str.extract('(\d+)', expand=False).astype(float)
        all_insertion.sort_values('sort', inplace=True, ascending=True)
        all_insertion = all_insertion.drop('sort', axis=1)
        return all_insertion


def assemble_repeat_file(master_unit, data, outputDir, chains, all_insertion, targetname, repeatsDB, standard):
    header=[]
    header.append('*********************** SUMMARY ***********************\nSRUL:\t{0}\nMASTER:\t{1}\nDATE:\t{2}\n'.format(SRUL, master_unit, time.strftime(r"%d.%m.%Y %H:%M:%S", time.localtime())))
    #ave_tmscore = average(data)
    #w_average = weigthed_average(data)
    if len(data) >= n_unit_rep:# and w_average > tm_score:
    # if len(data) >= n_unit_rep and ave_tmscore > tm_score:
        header.append('REPEAT:\tYes\n')
        logging.info('The chain {0} of the protein {1} is a REPEAT.'.format(chains, targetname))
    else:
        header.append('REPEAT:\tNo\n')
        logging.info('The chain {0} of the protein {1} is NOT a REPEAT'.format(chains, targetname))
    header.append('*******************************************************\nUNITS:\n{0}\n*******************************************************\nINSERTION: \n{1}'.format(data, all_insertion))

    if standard or repeatsDB:
        pass
    else:
        if os.path.basename(os.path.normpath(outputDir)) != targetname:
            file_prova = open('{0}/units_and_correlation_final_{1}.txt'.format(outputDir, chains), 'w+')
        else:
            file_prova=open('{0}/{1}/units_and_correlation_final_{1}.txt'.format(outputDir, chains), 'w+')
        for ln in header:
            file_prova.write(ln)
        file_prova.close()
    return header


def mustang_description_file(mustang_align, chains, targetname):
    description_file_array=[]
    if os.path.basename(os.path.normpath(mustang_align)) != 'mustang_alignment':
        if os.path.isfile('{0}/{2}{1}.pdb'.format(mustang_align, chains, targetname)):
            os.remove('{0}/{2}{1}.pdb'.format(mustang_align, chains, targetname))
            description_file = open('{0}/mustang_description_file'.format(mustang_align), 'w+')
            description_file.write('>' + '{0}'.format(mustang_align) + '\n')
            for file in os.listdir('{0}/'.format(mustang_align)):
                if file.endswith('.pdb'):
                    name = file
                    description_file.write('+ ' + name + '\n')
            description_file.close()
        else:
            description_file = open('{0}/mustang_description_file'.format(mustang_align), 'w+')
            description_file.write('>' + '{0}'.format(mustang_align) + '\n')
            for file in os.listdir('{0}/'.format(mustang_align)):
                if file.endswith('.pdb'):
                    name = file
                    description_file.write('+ ' + name + '\n')
                    description_file_array.append('+ {0}\n'.format(name))
            description_file.close()
    else:
        if os.path.isfile('{0}/{1}/{2}{1}.pdb'.format(mustang_align, chains, targetname)):
            os.remove('{0}/{1}/{2}{1}.pdb'.format(mustang_align, chains, targetname))
            description_file = open('{0}/{1}/mustang_description_file'.format(mustang_align, chains), 'w+')
            description_file.write('>' + '{0}/{1}'.format(mustang_align, chains) + '\n')
            for file in os.listdir('{0}/{1}/'.format(mustang_align, chains)):
                if file.endswith('.pdb'):
                    name = file
                    description_file.write('+ ' + name + '\n')
            description_file.close()
        else:
            description_file = open('{0}/{1}/mustang_description_file'.format(mustang_align, chains), 'w+')
            description_file.write('>' + '{0}/{1}'.format(mustang_align, chains) + '\n')
            for file in os.listdir('{0}/{1}/'.format(mustang_align, chains)):
                if file.endswith('.pdb'):
                    name = file
                    description_file.write('+ ' + name + '\n')
                    description_file_array.append('+ {0}\n'.format(name))
            description_file.close()
    return len(description_file_array)


def define_insertion_mustang(mustang_align, chains, targetname, repeatsDB, ins_threshold):
    mustang_fil, gap_num, name_df = [], [], []
    try:
        if os.path.basename(os.path.normpath(mustang_align)) != 'mustang_alignment':
            error = sub_mustang(mustang_align, targetname, chains, 0, repeatsDB=False)
            manage_mustang_error(error, mustang_align, chains)
            fl = open('{0}/{2}{1}.afasta'.format(mustang_align, chains, targetname)).readlines()
        else:
            error = sub_mustang(mustang_align, targetname, chains, 0, repeatsDB=False)
            manage_mustang_error(error, mustang_align, chains)
            fl = open('{0}/{1}/{2}{1}.afasta'.format(mustang_align, chains, targetname)).readlines()

        for ln in fl:
            if ln.startswith('>'):
                name = ln.split('>')[1].split('.pdb')[0]
                name_df.append(name)
            else:
                mustang_fil.append(ln)
        idx_list = [idx for idx, val in enumerate(mustang_fil) if val == '\n']
        if not idx_list:
            z = []
            idx_list = [idx for idx, val in enumerate(mustang_fil) if len(val) < len(mustang_fil[0])]
            for i in range(1, len(idx_list)+1):
                z.append(i)
            idx_list = [x + y for x, y in zip(idx_list, z)]
            for j in range(len(idx_list)):
                mustang_fil[idx_list[j]:idx_list[j]] = ['\n']
            idx_list = [idx for idx, val in enumerate(mustang_fil) if val == '\n']
        size = len(mustang_fil)
        res = [mustang_fil[i: j] for i, j in zip([0] + idx_list, idx_list + ([size] if idx_list[-1] != size else []))]
        for line in range(len(res)):
                res[line] = [i.replace("\n", "") for i in res[line]]
        for j in range(len(res)):
            res[j] = ''.join(res[j])
        df_name = pd.DataFrame({'Name': name_df})
        df = pd.DataFrame({'sequence': res})
        df['sequence'].replace('', np.nan, inplace=True)
        df.dropna(subset=['sequence'], inplace=True)
        df.reset_index(drop=True, inplace=True)
        for i in range(len(df.sequence[0])):
            counter = 0
            for j in range(len(df)):
                if df.sequence[j][i] == '-':
                    counter = counter + 1
            gap_num.append(counter)
        threshold = (int(ins_threshold) * len(df))/100
        threshold = int(round(threshold)) #ho arrotondato per eccesso o difetto a seconda del decimale altrimenti la soglia era un po' ambigua (5.1 e l'85% era 5...)
        l, r = [], []
        for i in range(len(df.sequence)):
            for j in range(len(df.sequence[i])):
                for f in df.sequence[i][j]:
                    if gap_num[j] >= threshold:
                        f = re.sub('[a-zA-Z]+', 'I', f)
                    else:
                        f = re.sub('[a-zA-Z]+', 'U', f)
                    l.append(f)
            l.append('\n')
        m = ','.join(l)
        n = m.replace(',', '').split('\n')
        df_n = pd.DataFrame({'seq': n})
        df_n = df_n.replace('', np.NaN).dropna(how='all')
        insertion_df = df_name.join(df_n)

    except FileNotFoundError:
        logging.warning('Mustang Error: fasta file not finding. Pass detect insertions in unit.')
        insertion_df = []

    return insertion_df


def identify_insertion(insertion_df, res_ins):
    index_I, num_of_I, insertion, ins_four = [], [], [], []
    if isinstance(insertion_df, list):
        if not insertion_df:
            insertion.append('-\t-\t-\t-\t-')
    elif isinstance(insertion_df, pd.DataFrame):
        if insertion_df.empty:
            insertion.append('-\t-\t-\t-\t-')
        else:
          #  print('insertion_df', insertion_df)
            insertion_df = insertion_df[insertion_df['seq'].notna()]
            for j in range(len(insertion_df)):
                for ind, val in enumerate(insertion_df.seq[j]):
                    if val == 'I':
                        index_I.append('{0}\t{1}'.format(insertion_df.Name[j], ind))
            prova_df = pd.DataFrame({'Name': index_I})
            if prova_df.empty:
                insertion.append('-\t-\t-\t-\t-')
            else:
                prova_df = pd.DataFrame(prova_df.Name.str.split('\t', 1).tolist(), columns=['Name', 'index_I'])
                prova_df["index_I"] = prova_df["index_I"].astype(int)
                prova_df = prova_df.groupby('Name')['index_I'].apply(list).reset_index()
                for i in range(len(prova_df)):
                    for k, g in groupby(enumerate(prova_df.index_I[i]), lambda ix: ix[0] - ix[1]):
                        ins_four.append('{0}\t{1}'.format(prova_df.Name[i], list(map(itemgetter(1), g))))
                df_ins = pd.DataFrame({'Name': ins_four})
                df_ins = pd.DataFrame(df_ins.Name.str.split('\t', 1).tolist(), columns=['Name', 'index_I'])
                for i in range(len(df_ins)):
                    df_ins['index_I'][i] = pd.eval(df_ins['index_I'][i])
                df_ins['Length'] = df_ins['index_I'].str.len().astype(int)
                df_ins['insertion_mark'] = '0'
                for k in range(len(df_ins)):
                    gap_before_ins = 0
                    if df_ins.iloc[k].Length > int(res_ins):
                        df_ins['insertion_mark'][k] = 1
                        for l in range(0, int(df_ins.iloc[k].index_I[0])):
                            for j in range(len(insertion_df)):
                                if df_ins.iloc[k].Name == insertion_df.Name[j]:
                                    if insertion_df.seq[j][l] == '-':
                                        gap_before_ins += 1
                        insertion.append('{0}\t{1}\t{2}\t{3}\t{4}'.format(df_ins.iloc[k].Name, int(df_ins.iloc[k].Name.split('_', 2)[1]) + int(df_ins.iloc[k].index_I[0] - gap_before_ins), int(df_ins.iloc[k].Name.split('_', 2)[1]) + int(df_ins.iloc[k].index_I[int(len(df_ins.iloc[k].index_I)-1)] - gap_before_ins), df_ins.iloc[k].Length, df_ins.iloc[k].insertion_mark))
                if not insertion:
                    insertion.append('-\t-\t-\t-\t-')
    return insertion


def average(data):
    table = pd.DataFrame(data)
    # print('TABLEEEEEE', table)
    if table.empty:
        ave_tmscore=0
    else:
        ave_tmscore = table.TM_SCORE.mean()
    return ave_tmscore


def weigthed_average(data):
    vector_sum, prova = [], []
    table = pd.DataFrame(data)
    for i in range(len(table.TM_SCORE)):
        summ = table.TM_SCORE[i]*float(table.LENGTH[i])
        vector_sum.append(summ)
        prova.append(float(table.LENGTH[i]))
    vector = sum(vector_sum)/sum(prova)
    return vector


def remove_unit_under_threshold(data, targetname, chains, threshold_unit):#, ins, mustang_align):
    junk_name=[]
    data['THRESHOLD'] = float('0')
    for i in range(len(data)):
        data.THRESHOLD[i] = float(float(data.TM_SCORE[i]) * float(data.COVERAGE[i].split(' ')[0]))

        ######################### changed here ###################
        if data.THRESHOLD[i] < float(threshold_unit):
            junk_name.append('{0}{1}_{2}_{3}'.format(targetname, chains, data.START[i], data.END[i]))
        ########################################################
        # if int(data['GAP_MASTER'][i]) > int(gap_in_master):
        #     junk_name.append('{0}{1}_{2}_{3}'.format(targetname, chains, data.START[i], data.END[i]))

    data['GAP_MASTER']=data['GAP_MASTER'].astype(int)
    ####################### and here ##########################
    data = data.loc[data['THRESHOLD'] > float(threshold_unit)]
    #######################################################
    ## data = data.loc[data['GAP_MASTER'] < int(gap_in_master)]

    data.reset_index(inplace=True)
    data = data.drop(['THRESHOLD', 'index'], axis=1)
    return data, junk_name


def insertion_between_unit_manage_data(data, resseqs, ins, res_ins):#, mustang_align, targetname, chains, ins):
    ins['LENGTH'] = pd.to_numeric(ins['LENGTH'], errors='coerce')
    ins = ins.replace(np.nan, 0, regex=True)
    ins['LENGTH'] = ins['LENGTH'].astype(int)
    for i in range(len(ins)):
        if ins.LENGTH[i] != '-':
            if int(ins.LENGTH[i]) > int(unit_far_apart):
                if i is ins.index[-1]:
                    data = data.drop(data.index[[len(data)-1]])
                else:
                    data = data.drop(data.index[[i]])
                data.reset_index(drop=True, inplace=True)

    if isinstance(data, pd.DataFrame):
        if data.empty:
            return data, ins
        else:
            if len(data.START) == 1:
                return data, ins

            ins_2, data_1= find_insertion_between_units(data, resseqs, res_ins)
            ins_2['LENGTH'] = pd.to_numeric(ins_2['LENGTH'], errors='coerce')
            ins_2 = ins_2.replace(np.nan, 0, regex=True)
            ins_2['LENGTH'] = ins_2['LENGTH'].astype(int)
            ins_new = ins_2.loc[ins_2['LENGTH'] > int(unit_far_apart)]
            if ins_new.empty:
                return data_1, ins_2
            else:
                return insertion_between_unit_manage_data(data_1, resseqs, ins_2)
    else:
        return data, ins


def detect_too_far_apart_units(data, ins):#, mustang_align, targetname, chains):
    junk_info = []
    for i in range(len(ins.LENGTH)):
        if ins.LENGTH[i] != '-':
            if int(ins.LENGTH[i]) > int(unit_far_apart):
                data_1 = data
                data = data[data.END != int(ins.START[i]) - 1]
                junk = data_1[data_1.END == int(ins.START[i]) - 1]
                junk_info.append('{0}\t{1}'.format(junk.START.iloc[0], junk.END.iloc[0]))
                data.reset_index(drop=True, inplace=True)
    return data, junk_info


def remove_too_far_apart_unit(df1, df2, mustang_align, targetname, chains):
    #print('df1', type(df1), 'df2', type(df2))
    if isinstance(df1, pd.DataFrame) and isinstance(df2, pd.DataFrame):
        if df1.empty:
            junk_info = df2
        elif df2.empty:
            junk_info = df1
        else:
            junk_info = pd.concat([df1, df2]).drop_duplicates(keep=False)
    else:
        if not df2:
            junk_info = df1
        elif not df1:
            junk_info = df2
        else:
            junk_info = df1 + list(set(df2) - set(df1))
    #print(type(junk_info))
    if junk_info.empty:
        pass
    else:
        for i, j in zip(junk_info.START, junk_info.END):
            if os.path.basename(os.path.normpath(mustang_align)) != 'mustang_alignment':
                if os.path.isfile('{0}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chains, targetname, i, j)):
                    os.remove('{0}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chains, targetname, i, j))
                else:
                    print('Seems you already removed the file...')
                    pass
            else:
                if os.path.isfile('{0}/{1}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chains, targetname, i, j)):
                    os.remove('{0}/{1}/{2}{1}_{3}_{4}.pdb'.format(mustang_align, chains, targetname, i, j))
                else:
                    print('Seems you already removed the file...')
                    pass


def last_management_ins(data, all_insertion):
    if (all_insertion.START[0] == '-') and (len(all_insertion) == 1):
        return all_insertion
    else:
        insertion=[]
        i=data.START[0]
        j=data.END.iloc[-1]
        for k in range(len(all_insertion.START)):
            if all_insertion.START[k] != '-':
                if int(i) < int(all_insertion.START[k]) < int(j):
                    insertion.append(all_insertion.loc[k])
        insert = pd.DataFrame(insertion, index=range(len(insertion)), columns=['START', 'END', 'LENGTH'])
        return insert
