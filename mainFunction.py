import errno
import time
import logging
import os, sys
import re
import shutil
import gzip, inspect
from shutil import copyfile
from urllib.request import urlretrieve
from Bio.PDB.Polypeptide import PPBuilder
from string import whitespace
from Bio.PDB import parse_pdb_header
from urllib.error import HTTPError

import pandas as pd
from Bio.PDB import PDBParser, PDBIO
from Bio.PDB import *

from configurationFile import *
import fileinput


def download_pdb(outputDir, targetname, database):
    if os.path.exists('{0}/{1}.pdb'.format(database, targetname)):
        copyfile('{0}/{1}.pdb'.format(database, targetname), '{0}/{1}.pdb'.format(outputDir, targetname))
        logging.info("The protein {0} in in the database.".format(targetname))
    elif os.path.exists('{0}/pdb{1}.ent.gz'.format(database, targetname)):
        logging.info("The protein {0} in in the database.".format(targetname))
        with gzip.open('{0}/pdb{1}.ent.gz'.format(database, targetname), 'rb') as s_file, \
                open('{0}/{1}.pdb'.format(outputDir, targetname), 'wb') as d_file:
            shutil.copyfileobj(s_file, d_file)
    elif os.path.exists('{0}/{1}.pdb'.format(outputDir, targetname)):
        pass
    else:
        logging.info("The protein {0} is not in the database... \n\t\t\t\t\t\t "
                     "Download {0} from Protein Data Bank (PDB)...".format(targetname))
        try:
            url = urlretrieve(f'http://files.rcsb.org/download/{targetname}.pdb',
                              f'{outputDir}/{targetname}.pdb')
            # logging.info("The protein {0} is not in the database... \n\t\t\t\t\t\t Download {0} from Protein Data Bank (PDB)...".format(targetname))
        except HTTPError as err:
            if err.code == 404:
                logging.error('404 Not foud')
                sys.exit()
            else:
                logging.error('Try to download file...not knowing error found...')


def remove_temp_folder(temp_dir):
    try:
        temp_dir
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
    pass

    #################### This command is useful when the folder 'Temporary' is useless ##################
    # if os.path.exists(temp_dir):
    #     shutil.rmtree(temp_dir, ignore_errors=True)


def create_folder(outputDir, chain):
    try:
        os.makedirs(f'{outputDir}/Temporary/TMalign_alignment/{chain}')
        os.makedirs(f'{outputDir}/Temporary/mustang_alignment/{chain}')
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        pass


def save_pdb_master_chain(master_name, outputDir, chain_master):
    pdb_file = open('{}/{}.pdb'.format(outputDir, master_name)).readlines()
    master_chain = open('{}/{}_{}.pdb'.format(outputDir, master_name, chain_master), 'w+')
    for ln in pdb_file:
        if ln.startswith('ATOM '):
            if ln[21:23].translate(dict.fromkeys(map(ord, whitespace))) == chain_master:
                master_chain.write(ln)
    master_chain.close()


def remove_dis_atoms(targetname, outputDir):
    parser = PDBParser()
    pdb = parser.get_structure(targetname, '{0}/{1}.pdb'.format(outputDir, targetname))
    io = PDBIO()
    ppb = PPBuilder()

    class NMROutputSelector2(Select):  # Inherit methods from Select class
        def accept_atom(self, atom):
            if (not atom.is_disordered()) or atom.get_altloc() == 'A':
                atom.set_altloc(' ')  # Eliminate alt location ID before output.
                return True
            else:  # Alt location was not one to be output.
                return False

    # class NotDisordered(Select):
    #     def accept_atom(self, atom):
    #         return not atom.is_disordered() or atom.get_altloc() == 'A'

    io = PDBIO()
    io.set_structure(pdb)
    io.save('{0}/{1}_temp.pdb'.format(outputDir, targetname), select=NMROutputSelector2())


def count_n_chains(outputDir, targetname, pdb_chain, chain_flag=True):
    remove_dis_atoms(targetname, outputDir)
    io = PDBIO()

    pdb_temp = PDBParser().get_structure(targetname, '{0}/{1}_temp.pdb'.format(outputDir, targetname))
    ppb = PPBuilder()
    io.set_structure(pdb_temp)
    if chain_flag is True:
        for chain in pdb_temp.get_chains():
            io.set_structure(chain)
            chains.append(chain.get_id())
            if chain.get_id() == pdb_chain:
                ########################### this will be remove after the end of the code #####################
                # try:
                #     os.mkdir('{0}/{1}'.format(outputDir, chain.get_id()))
                # except OSError as exc:
                #     if exc.errno != errno.EEXIST:
                #         raise
                #     pass
                ###############################################################################################
                if os.path.exists('{0}/{2}_{1}.pdb'.format(outputDir, chain.get_id(), pdb_temp.get_id())):
                    pass
                else:
                    io.save('{0}/{2}_{1}.pdb'.format(outputDir, chain.get_id(), pdb_temp.get_id()))
            else:
                pass
    else:
        for chain in pdb_temp.get_chains():
            io.set_structure(chain)
            chains.append(chain.get_id())
            if os.path.exists('{0}/{1}/{2}_{3}.pdb'.format(outputDir, targetname, pdb_temp.get_id(), chain.get_id())):
                pass
            else:
                io.save('{0}/{2}_{1}.pdb'.format(outputDir, chain.get_id(), pdb_temp.get_id()))
    logging.info("There are {0} chains in {1}.pdb: {2}".format(len(chains), targetname, chains))
    return chains


def is_chain_protein_or_no(chains_pdb, targetname, outputDir):
    chas = []
    for i in range(len(chains_pdb)):
        pdb_chain = PDBParser().get_structure(f'{targetname}_{chains_pdb[i]}',
                                              f'{outputDir}/{targetname}_{chains_pdb[i]}.pdb')
        ppb = PPBuilder()
        for pp in ppb.build_peptides(pdb_chain):
            chas.append(chains_pdb[i])
    chains = list(set(chas))
    for i in range(len(chains)):
        try:
            os.mkdir('{0}/{1}'.format(outputDir, chains[i]))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
            pass
        os.system('mv {0}/{1}_{2}.pdb {0}/{2}/ 2>/dev/null'.format(outputDir, targetname, chains[i]))
    for item in os.listdir('{0}/'.format(outputDir)):
        if item.startswith('{0}_'.format(targetname)):
            os.remove(os.path.join('{0}/'.format(outputDir, targetname), item))
    return chains


def align_sequence_cdhit(targetname, outputDir):
    chain = []
    url = urlretrieve(
        'https://www.rcsb.org/pdb/download/viewFastaFiles.do?structureIdList={0}&compressionType=uncompressed'.format(
            targetname), '{0}/{1}.fa'.format(outputDir, targetname))
    os.system('{2} -i {0}/{1}.fa -o {0}/{1}_100.fa -c 1.00'.format(outputDir, targetname, cdhit))
    protein_fa = open('{0}/{1}_100.fa'.format(outputDir, targetname)).readlines()
    for ln in protein_fa:
        if ln.startswith('>'):
            first_chain = ln.split('>')[1].split(':')[1].split('|')[0]
            chain.append(first_chain)
    return chain


def ress_chain(outputDir, targetname, chains):
    chain_file_temp = open('{0}/{1}/{2}_{1}_temp.pdb'.format(outputDir, chains, targetname), 'w+')
    chain_file = open('{0}/{1}/{2}_{1}.pdb'.format(outputDir, chains, targetname)).readlines()
    for ln in chain_file:
        if ln[0:6] == "ATOM  ":
            chain_file_temp.write(ln)
    chain_file_temp.close()
    os.rename('{0}/{1}/{2}_{1}_temp.pdb'.format(outputDir, chains, targetname),
              '{0}/{1}/{2}_{1}.pdb'.format(outputDir, chains, targetname))
    chain_pdb = PDBParser().get_structure('{0}_{1}'.format(targetname, chains),
                                          '{0}/{2}/{1}_{2}.pdb'.format(outputDir, targetname, chains))
    ress = [residue.id[1] for residue in chain_pdb.get_residues()]
    return ress


def master_given(masterCode, outputDir, database):
    r_pdb = re.compile('.*.pdb')
    r_piece = re.compile('.*_.*_.*_.*_.*')
    if r_pdb.match('{0}'.format(masterCode)) is not None:
        print('You choose a pdb file for the master')
        download_pdb(outputDir, masterCode.split('.')[0], database)
        structure = PDBParser().get_structure('{0}'.format(masterCode.split('.')[0]),
                                              '{0}/{1}.pdb'.format(outputDir, masterCode.split('.')[0]))
        if structure:
            print('OK, master confirmed!')
            logging.info('The master provided is in the right format: {0}'.format(masterCode))
        else:
            print('...are you sure that the master is correct? Check and try again...')
            logging.info('...are you sure that the master is correct? Check and try again...'.format(masterCode))
            sys.exit()
    elif r_piece.match('{0}'.format(masterCode)) is not None:
        print('Yes, there is a match: {0} \n'.format(masterCode))
    return masterCode


# def master_choice(fl):
#     max_score = []
#     for ln in fl:
#         if ln.startswith('Name of Chain_1'):
#             name = ln.split(':')[1].split('/')[-1].split('(')[0]
#         if ln.startswith('TM-score'):
#             if ln.split(',')[0].endswith('_1'):
#             # if ln.endswith('_1)\n'):
#                 tm_score = ln.split('(')[0]
#                 value = tm_score.split(' ')[1]
#                 max_score.append(name + value)
#     return max_score

################ DON'T REMOVEEEEEE #################
def master_choice(fl):
    max_score = []
    for ln in fl:
        if ln.startswith('Name of Chain_1'):
            name = ln.split(':')[1].split('/')[-1]
        if ln.startswith('TM-score'):
            if ln.endswith('_1)\n'):
                tm_score = ln.split('(')[0]
                value = tm_score.split(' ')[1]
                max_score.append(name + value)
    return max_score


def master_on_pdb(fl):
    master_seq = []
    for ln in fl:
        if '::' in ln:
            for idx, val in enumerate(ln):
                if val == ':':
                    master_seq.append(idx)
    return master_seq


def num_gap_pre_seq(new_seq, ml):
    start_seq = 0
    for l in range(0, new_seq[0]):
        for ind, line in enumerate(ml):
            if ind == 24:
                for idx, val in enumerate(line):
                    if idx == l:
                        if val == '-':
                            start_seq = start_seq + 1
    return start_seq


def num_gap_in_seq(new_seq, ml):
    in_seq = 0
    print(new_seq[0])
    print(new_seq[len(new_seq) - 1])
    for l in range(new_seq[0], new_seq[len(new_seq) - 1]):
        for ind, line in enumerate(ml):
            if ind == 24:
                for idx, val in enumerate(line):
                    if idx == l:
                        if val == '-':
                            in_seq = in_seq + 1
    return in_seq


def pre_master_gaiden(pl, pre, resseqs, master_seq, start_seq):
    pre_master = []
    resseqs_1 = list(map(str, resseqs))
    result = resseqs_1[:resseqs_1.index(resseqs_1[master_seq[0]]) - start_seq]
    for i in range(len(result)):
        for line in pl:
            if line[22:26] != '':
                if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == result[
                    i]:  # if int(line[22:26]) < int(resseqs[master_seq[0]-start_seq]):
                    if line[0:6] == "ATOM  ":
                        pre_master.append(line)
                        pre.write(line)
    pre.close()
    # return pre_master, result
    return result


def post_master_gaiden(pl, post, resseqs, master_seq, tot_gap):
    post_master = []
    resseqs_1 = list(map(str, resseqs))
    result = resseqs_1[resseqs_1.index(
        resseqs_1[master_seq[len(master_seq) - 1] - tot_gap]):]  # resseqs_1.index(resseqs_1[len(resseqs_1) - 1])]
    for i in range(len(result)):
        for line in pl:
            if line[22:26] != '':
                if line[0:6] == "ATOM  ":
                    if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == result[i]:
                        post_master.append(line)
                        post.write(line)
    post.close()
    # return post_master, result
    return result


def master_gaiden(pl, master_pdb, resseqs, master_seq, start_seq, tot_gap, no_master=True):
    if no_master is True:
        start = resseqs[master_seq[0] - start_seq]
        end = resseqs[master_seq[len(master_seq) - 1] - tot_gap]
    else:
        start = start_seq
        end = tot_gap
    protein_master = []
    resseqs_1 = list(map(str, resseqs))
    result = resseqs_1[resseqs_1.index(str(start)): resseqs_1.index(str(end)) + 1]
    for i in range(len(result)):
        for line in pl:
            if line[22:26] != '':
                if line[0:6] == "ATOM  ":
                    if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == result[
                        i]:  # == int(start) <= int(line[22:26]) <= int(end): #add a = in the first <=
                        protein_master.append(line)
                        master_pdb.write(line)
    master_pdb.close()
    # return protein_master, result
    return result


def find_coverage(ml):  # new_seq, start_seq, tot_gap):
    len_master, len_align = 0, 0
    for ind, line in enumerate(ml):
        if ind == 22:
            for id, val in enumerate(line):
                if val != '-':
                    len_master += 1
        if ind == 23:
            for idx, point in enumerate(line):
                if point == ':':
                    len_align += 1
    coverage = round(((float(len_align)) * 100) / (float(len_master - 1)))
    return coverage


def extract_RMSD(ml):
    rmsd = []
    for ln in ml:
        line = ln.strip()
        if line.startswith('Aligned length='):
            rmsd = line.split(',')[1].split('=')[1]
    return rmsd


def gap_master(ml, new_seq):
    in_master = 0
    for l in range(new_seq[0], new_seq[len(new_seq) - 1]):
        for ind, line in enumerate(ml):
            if ind == 22:
                for idx, val in enumerate(line):
                    if idx == l:
                        if val == '-':
                            in_master = in_master + 1
    return in_master


def iterative_function(master, pdb_file, resseqs, align, mustang_alignment, chains, targetname, intermediate_file,
                       percentage, other_region=False):
    # if align.rsplit('/',1)[1] == chains and align.rsplit('/',2)[1].startswith('Region_'):
    #     align = align.rsplit('/',1)[0]
    # elif align.rsplit('/',1)[1] == chains and align.rsplit('/',2)[1].startswith('TMalign_alignment'):
    #     pass
    # elif align.rsplit('/',1)[1].startswith('Region_'):
    #     pass
    # else:
    #     align = f'{align}/{chains}'

    # print('align', align)
    # print(resseqs[0], resseqs[len(resseqs)-1])
    if other_region is True:
        os.system('{4} {0} {1} > {2}/tm_output_{3}_{5}.txt'.format(master, pdb_file, align, resseqs[0], TMalignexe,
                                                                   resseqs[len(resseqs) - 1]))
        ml = open('{0}/tm_output_{1}_{2}.txt'.format(align, resseqs[0], resseqs[len(resseqs) - 1])).readlines()
        pre_x = open('{0}/{2}_{1}_{3}pre.pdb'.format(align, resseqs[0], targetname, resseqs[len(resseqs) - 1]), 'w+')
        post_x = open('{0}/{2}_{1}_{3}post.pdb'.format(align, resseqs[0], targetname, resseqs[len(resseqs) - 1]), 'w+')
    else:
        os.system('{5} {0} {1} > {2}/{4}/tm_output_{3}_{6}.txt'.format(master, pdb_file, align, resseqs[0], chains,
                                                                       TMalignexe, resseqs[len(resseqs) - 1]))
        ml = open(
            '{0}/{1}/tm_output_{2}_{3}.txt'.format(align, chains, resseqs[0], resseqs[len(resseqs) - 1])).readlines()
        pre_x = open(
            '{0}/{1}/{3}_{2}_{4}pre.pdb'.format(align, chains, resseqs[0], targetname, resseqs[len(resseqs) - 1]), 'w+')
        post_x = open(
            '{0}/{1}/{3}_{2}_{4}post.pdb'.format(align, chains, resseqs[0], targetname, resseqs[len(resseqs) - 1]),
            'w+')

    new_seq = indentify_seq(ml)
    if not new_seq:
        pass
    else:
        start_seq = num_gap_pre_seq(new_seq, ml)
        in_seq = num_gap_in_seq(new_seq, ml)
        tot_gap = start_seq + in_seq
        coverage = find_coverage(ml)
        pl_x = open(pdb_file).readlines()
        # pdb_chain_x = PDBParser().get_structure('pre_name_{0}'.format(chains), pdb_file)
        # resseqs_x = [residue.id[1] for residue in pdb_chain_x.get_residues()]
        ress_pre = pre_master_x(pl_x, resseqs, new_seq, start_seq, pre_x)  # è il file .pdb
        ress_post = post_master_x(pl_x, resseqs, new_seq, tot_gap, post_x)  # è il file .pdb
        name_chain = little_chain(ml)
        # print(ress_pre)
        # print(ress_post)

        tmscore = extract_tmscore(ml, name_chain)
        RMSD = extract_RMSD(ml)
        count_length = length_unit(resseqs, new_seq, start_seq, tot_gap)
        in_master = gap_master(ml, new_seq)
        intermediate_file.append(f'{resseqs[new_seq[0] - start_seq]}\t'
                                 f'{resseqs[resseqs.index(resseqs[new_seq[len(new_seq) - 1] - tot_gap]) - 1]}\t'
                                 f'{count_length}\t{float(tmscore)}\t{float(RMSD)}\t{int(in_seq)}\t{int(in_master)}\t'
                                 f'{coverage} %\ttm_output_{resseqs[0]}_{resseqs[len(resseqs) - 1]}\n')
        # percentage = (int(xcent) * len(new_seq)) / 100

        # intermediate_file.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\t{8}\n'.format(resseqs[new_seq[0]-start_seq], resseqs[resseqs.index(resseqs[new_seq[len(new_seq) - 1]-tot_gap])-1], count_length, float(tmscore), float(RMSD), int(in_seq), int(in_master), coverage, 'tm_output_{0}_{1}'.format(resseqs[0], resseqs[len(resseqs)-1])))
        if not ress_pre:
            if len(ress_post) > int(percentage):

                if other_region is True:
                    iterative_function(master, post_x.name, ress_post, align, mustang_alignment, chains, targetname,
                                       intermediate_file, percentage, other_region=True)
                else:
                    iterative_function(master, post_x.name, ress_post, align, mustang_alignment, chains, targetname,
                                       intermediate_file, percentage, other_region=False)
            else:
                return
        elif not ress_post:
            if len(ress_pre) > int(percentage):
                if other_region is True:
                    iterative_function(master, pre_x.name, ress_pre, align, mustang_alignment, chains, targetname,
                                       intermediate_file, percentage, other_region=True)
                else:
                    iterative_function(master, pre_x.name, ress_pre, align, mustang_alignment, chains, targetname,
                                       intermediate_file, percentage, other_region=False)
            else:
                return
        elif ress_pre and ress_post:
            if len(ress_pre) > int(percentage):
                if len(ress_post) > int(percentage):
                    if other_region is True:
                        iterative_function(master, post_x.name, ress_post, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=True)
                        iterative_function(master, pre_x.name, ress_pre, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=True)
                    else:
                        iterative_function(master, post_x.name, ress_post, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=False)
                        iterative_function(master, pre_x.name, ress_pre, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=False)
                else:
                    if other_region is True:
                        iterative_function(master, pre_x.name, ress_pre, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=True)
                    else:
                        iterative_function(master, pre_x.name, ress_pre, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=False)

            elif len(ress_post) > int(percentage):
                if len(ress_pre) > int(percentage):
                    if other_region is True:
                        iterative_function(master, post_x.name, ress_post, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=True)
                        iterative_function(master, pre_x.name, ress_pre, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=True)
                    else:
                        iterative_function(master, post_x.name, ress_post, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=False)
                        iterative_function(master, pre_x.name, ress_pre, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=False)
                else:
                    if other_region is True:
                        iterative_function(master, post_x.name, ress_post, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=True)
                    else:
                        iterative_function(master, post_x.name, ress_post, align, mustang_alignment, chains, targetname,
                                           intermediate_file, percentage, other_region=False)
            else:
                return
        else:
            return


def indentify_seq(ml):
    new_seq = []
    for ln in ml:
        if '::' in ln:
            for ind, val in enumerate(ln):
                if val == ':':
                    new_seq.append(ind)
    return new_seq


def pre_master_x(file, resseqs, new_seq, start_seq, pre_file):
    if resseqs == 0:
        resseqs_1, pre_line, result = [], [], []
        for line in file:
            if line[22:26] != '':
                if line[0:6] == "ATOM  ":
                    resseqs_1.append(line[22:26].translate(dict.fromkeys(map(ord, whitespace))))
        seen = set()
        for item in resseqs_1:
            if item not in seen:
                seen.add(item)
                result.append(item)
        pre = result[:result.index(result[new_seq[0]]) - start_seq]
        for i in range(len(pre)):
            for line in file:
                if line[22:26] != '':
                    if line[0:6] == "ATOM  ":
                        if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == pre[
                            i]:  # remove whitespace from a sting
                            pre_line.append(line)
                            pre_file.write(line)
        pre_file.close()
    else:
        if len(resseqs) < round((85 / 100) * length_of_master):
            pre = []
            pre_line = []
            # pass
        else:
            # resseqs_1 = list(map(str, resseqs))
            pre = resseqs[:resseqs.index(resseqs[new_seq[0] - start_seq])]
            pre_line = []
            for i in range(len(pre)):
                for line in file:
                    if line[22:26] != '':
                        if line[0:6] == "ATOM  ":
                            if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == pre[
                                i]:  # remove whitespace from a sting
                                pre_line.append(line)
                                pre_file.write(line)
            pre_file.close()
    # return pre_line, pre
    return pre


def post_master_x(file, resseqs, new_seq, tot_gap, post_file):
    if resseqs == 0:
        resseqs_1, post_line, result = [], [], []
        for line in file:
            if line[22:26] != '':
                if line[0:6] == "ATOM  ":
                    resseqs_1.append(line[22:26].translate(dict.fromkeys(map(ord, whitespace))))
        seen = set()
        for item in resseqs_1:
            if item not in seen:
                seen.add(item)
                result.append(item)
        post = result[result.index(tot_gap) + 1:]  # result.index(result[len(result)-1])]
        for i in range(len(post)):
            for line in file:
                if line[22:26] != '':
                    if line[0:6] == "ATOM  ":
                        if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == post[
                            i]:  # remove whitespace from a sting
                            post_line.append(line)
                            post_file.write(line)
        post_file.close()
        # return post_line, post
    else:
        if len(resseqs) < round((85 / 100) * length_of_master):
            post = []
            post_line = []
            # pass
        else:
            # resseqs_1 = list(map(str, resseqs))
            # print('racasc', resseqs_1)
            post = resseqs[resseqs.index(
                resseqs[new_seq[len(new_seq) - 1] - tot_gap]):]  # resseqs_1.index(resseqs_1[len(resseqs_1) - 1])]
            del post[0]
            post_line = []
            for i in range(len(post)):
                for line in file:
                    if line[22:26] != '':
                        if line[0:6] == "ATOM  ":
                            if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == post[
                                i]:  # remove whitespace from a sting
                                post_line.append(line)
                                post_file.write(line)
            post_file.close()
    # return post_line, post
    return post


def little_chain(ml):
    res_chain = []
    for ln in ml:
        if ln.startswith('Length'):
            name = ln.split(':')[0].split('of')[1]
            n_res = ln.split(':')[1].split('residues')[0]
            res_chain.append((name, n_res))
    df = pd.DataFrame(res_chain)
    min = df[1].min()
    chain = df[df[1] == min]
    chain_name = chain.iloc[0][0]
    return chain_name


def extract_tmscore(ml, chain_name):
    tmscore = []
    for ln in ml:
        line = ln.strip()
        if line.startswith('TM-score='):
            if line.endswith(chain_name + ')'):
                tmscore = line.split('=')[1].split('(')[0]
    return tmscore


def length_unit(resseqs_x, new_seq, start_seq, tot_gap):
    resseqs = resseqs_x[resseqs_x.index(resseqs_x[new_seq[0] - start_seq]): resseqs_x.index(
        resseqs_x[new_seq[len(new_seq) - 1] - tot_gap])]
    # resseqs=resseqs_x[resseqs_x.index(resseqs_x[ns[0]]-start_seq): resseqs_x.index(resseqs_x[ns[len(ns) - 1] ]- tot_gap)]
    return len(resseqs)


def make_db_file(targetname, chains, insertion, data, protein_master, resseqs, outputDir, master_unit):
    # if os.path.basename(os.path.normpath(outputDir)) != targetname:
    # if os.path.basename(os.path.normpath(outputDir)).startswith('Region_'):
    #     db_file_ann = open('{0}/{3}_{4}_{1}_{2}.db'.format(outputDir, os.path.basename(os.path.normpath(outputDir)).split('_')[1], os.path.basename(os.path.normpath(outputDir)).split('_')[2], targetname, chains), 'w+')
    # else:
    print(outputDir.rsplit('/', 2))
    if outputDir.rsplit('/', 2)[2].startswith('Region_'):
        db_file_ann = open(
            f'{outputDir}/{targetname}_{chains}_{outputDir.rsplit("/", 2)[2].split("_")[1]}_{outputDir.rsplit("/", 2)[2].split("_")[2]}.db',
            'w+')
    else:
        db_file_ann = open(f'{outputDir}/{chains}/{targetname}_{chains}.db', 'w+')
    db_file = []
    unit = data[['START', 'END']]
    print(unit)
    # print(len(unit))
    if len(unit) < 3:
        pass
    else:
        ins = insertion[['START', 'END']]
        db_file.append(f'SOURCE\tRepeatsDB-lite 2.0\nDATE\t{time.strftime(r"%d.%m.%Y")}\n'
                       f'PDB\t{targetname}\nCHAIN\t{chains}\nRAPHAEL\t*\n')
        if len(unit) >= n_unit_rep:
            db_file.append('REPEATED\tYes\n')
        else:
            db_file.append('REPEATED\tNo\n')
        if master_unit.split('_')[3] == 'III':
            class_master = '3'
        elif master_unit.split('_')[3] == 'IV':
            class_master = '4'
        elif master_unit.split('_')[3] == 'V':
            class_master = '5'
        else:
            class_master = '0'
            logging.warning('class not recognized...it is not 3,4 or 5.')
        db_file.append(
            f'REG\t{unit.START.iloc[0]} {unit.END.iloc[len(unit) - 1]} {class_master} {master_unit.split("_")[4]}\n'
            f'MASTER\t{unit.START.iloc[0]} {unit.END.iloc[len(unit) - 1]} {master_unit.split("_")[0]} {master_unit.split("_")[1]} {master_unit.split("_")[2]}\n')
        for i, j in zip(unit.START, unit.END):
            db_file.append(f'UNIT\t{i} {j}\n')
        for k, l in zip(ins.START, ins.END):
            if k == '-':
                pass
            else:
                db_file.append(f'INS\t{k} {l}\n')
        for i in db_file:
            db_file_ann.write(i)


def unify_db_all_regions(outputDir, chains, targetname):
    db = []
    region_unit = []
    new_db_file = open(f'{outputDir}/{chains}/{targetname}{chains}.db', 'w+')
    db.append(
        f'SOURCE\tRepeatsDB-lite 2.0\nDATE\t{time.strftime(r"%d.%m.%Y")}\nPDB\t{targetname}\nCHAIN\t{chains}\nRAPHAEL\t*\n')
    for i in os.listdir('{0}/{1}'.format(outputDir, chains)):
        if i.startswith('Region_'):
            start = i.split('_')[1]
            end = i.split('_')[2]
            if not os.path.isfile(
                    '{0}/{1}/{5}/{2}_{1}_{3}_{4}.db'.format(outputDir, chains, targetname, start, end, i)):
                shutil.rmtree('{0}/{1}/{2}'.format(outputDir, chains, i))
            else:
                db_file = open(
                    '{0}/{1}/{5}/{2}_{1}_{3}_{4}.db'.format(outputDir, chains, targetname, start, end, i)).readlines()
                for line in db_file:
                    if line.startswith(('REG')):
                        region_unit.append(line)
        elif i == '{0}_{1}.db'.format(targetname, chains):
            db_file = open('{0}/{1}/{2}'.format(outputDir, chains, i)).readlines()
            for line in db_file:
                if line.startswith(('REG')):
                    region_unit.append(line)

    df = pd.DataFrame([ln.rstrip().split('\t')[1].split(' ') for ln in region_unit])
    if df.empty:
        os.remove(f'{outputDir}/{chains}/{targetname}{chains}.db')
        # print('il dataframe è vuoto...')
        pass
    else:
        print('df', df)
        df.columns = ['start', 'end', 'class', 'subclass']
        df['sort'] = df['start'].str.extract('(\d+)', expand=False).astype(int)
        df.sort_values('sort', inplace=True, ascending=True)
        df = df.drop('sort', axis=1)  # delete column "sort"
        for a, b in zip(df['start'], df['end']):
            for i in os.listdir('{0}/{1}'.format(outputDir, chains)):
                if i.startswith('Region_'):
                    start = i.split('_')[1]
                    end = i.split('_')[2]
                    db_file = open('{0}/{1}/{5}/{2}_{1}_{3}_{4}.db'.format(outputDir, chains, targetname, start, end,
                                                                           i)).readlines()
                    for line in db_file:
                        if line.startswith(('REG\t{0} {1}'.format(a, b))):
                            db.append(line)
                            for line in db_file:
                                if line.startswith('MASTER'):
                                    db.append(line)
                                if line.startswith('UNIT'):
                                    db.append(line)
                elif i == '{0}_{1}.db'.format(targetname, chains):
                    db_file = open('{0}/{1}/{2}'.format(outputDir, chains, i)).readlines()
                    for line in db_file:
                        if line.startswith(('REG\t{0} {1}'.format(a, b))):
                            db.append(line)
                            for line in db_file:
                                if line.startswith('MASTER'):
                                    db.append(line)
                                if line.startswith('UNIT'):
                                    db.append(line)
            for i in os.listdir('{0}/{1}'.format(outputDir, chains)):
                if i.startswith('Region_'):
                    start = i.split('_')[1]
                    end = i.split('_')[2]
                    db_file = open('{0}/{1}/{5}/{2}_{1}_{3}_{4}.db'.format(outputDir, chains, targetname, start, end,
                                                                           i)).readlines()
                    for line in db_file:
                        if line.startswith(('REG\t{0} {1}'.format(a, b))):
                            for line in db_file:
                                if line.startswith('INS'):
                                    db.append(line)
                elif i == '{0}_{1}.db'.format(targetname, chains):
                    db_file = open('{0}/{1}/{2}'.format(outputDir, chains, i)).readlines()
                    for line in db_file:
                        if line.startswith(('REG\t{0} {1}'.format(a, b))):
                            for line in db_file:
                                if line.startswith('INS'):
                                    db.append(line)
        for r in db:
            new_db_file.write(r)
        new_db_file.close()
