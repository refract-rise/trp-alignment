from insertionFunction import *
from mainFunction import *
import os
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt


def manage_unit_from_mustang(id_ins, data, trash_unit):
    junk_name=[]
    if len(id_ins) == 1 and id_ins.Name[0] == '-':
        junk_name = []
        data = data
    else:
        id_ins.length = id_ins.length.astype(int)
        junk_pieces = id_ins.groupby(id_ins.Name, as_index=False).sum()
        for i in range(len(junk_pieces.Name)):
            for j in range(len(data.START)):
                if int(junk_pieces.Name[i].split('_')[1]) == int(data.START[j]):
                    len_ins = int(junk_pieces.length[i])
                    len_unit = int(data.LENGTH[j])
                    true_len = len_unit - len_ins
                    len_threshold = round((int(trash_unit) * int(len_unit))/100)
                    if true_len >= len_threshold:
                        data = data
                    else:
                        data.END = data.END.replace(data.END[j], np.nan)
                        junk_name.append(junk_pieces.Name[i])

    data = data[pd.notnull(data['END'])]
    return junk_name, data


def create_file_for_mustang(data, mustang_align, resseqs, pl, targetname, chains):
    resseqs_1 = list(map(str, resseqs))
    if isinstance(data, pd.DataFrame):
        if data.empty:
            pass
        else:
            for i, j in zip(data.START, data.END):
                if os.path.basename(os.path.normpath(mustang_align)) != 'mustang_alignment':
                    file_mus = open('{0}/{1}{2}_{3}_{4}.pdb'.format(mustang_align, targetname, chains, i, j), '+w')
                else:
                    file_mus = open('{0}/{2}/{1}{2}_{3}_{4}.pdb'.format(mustang_align, targetname, chains, i, j), '+w')
                result = resseqs_1[resseqs_1.index(str(i)):resseqs_1.index(str(j)) + 1]
                for h in range(len(result)):
                    for line in pl:
                        if line[22:26] != '':
                            if line[0:6] == "ATOM  ":
                                if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == result[h]:
                                    file_mus.write(line)
                file_mus.close()

    elif not data:
        pass
    else:
        for i, j in zip(data.START, data.END):
            if os.path.basename(os.path.normpath(mustang_align)) != 'mustang_alignment':
                file_mus=open('{0}/{1}{2}_{3}_{4}.pdb'.format(mustang_align, targetname, chains, i, j), '+w')
            else:
                file_mus = open('{0}/{2}/{1}{2}_{3}_{4}.pdb'.format(mustang_align, targetname, chains, i, j), '+w')
            result = resseqs_1[resseqs_1.index(str(i)):resseqs_1.index(str(j))+1]
            for h in range(len(result)):
                for line in pl:
                    if line[22:26] != '':
                        if line[0:6] == "ATOM  ":
                            if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == result[h]:
                                file_mus.write(line)
            file_mus.close()


def remove_useless_mustang_file(mustang_align, junk_name, chains):#, data):
    if not junk_name:
        pass
    else:
        if os.path.basename(os.path.normpath(mustang_align)) != 'mustang_alignment':
            for i in range(len(junk_name)):
                for k in os.listdir(f'{mustang_align}/'):
                    if k.startswith(f'{junk_name[i].split("_")[0]}_'):
                        if k.split('.')[0].rstrip() == junk_name[i]:
                            os.remove('{0}/{2}.pdb'.format(mustang_align, chains, junk_name[i]))
        else:
            for i in range(len(junk_name)):
                for k in os.listdir('{0}/{1}/'.format(mustang_align, chains)):
                    if k.startswith('{0}_'.format(junk_name[i].split('_')[0])):
                        if k.split('.')[0].rstrip() == junk_name[i]:
                            os.remove('{0}/{1}/{2}.pdb'.format(mustang_align, chains, junk_name[i]))


def built_matrix(outputDir, chain, targetname):
    with open(f'{outputDir}/{chain}/{targetname}{chain}.db') as file_db:
        chain_pdb = PDBParser().get_structure(f'{targetname}_{chain}', f'{outputDir}/{chain}/{targetname}_{chain}.pdb')
        chain_pdb_file = open(f'{outputDir}/{chain}/{targetname}_{chain}.pdb').readlines()
        model=chain_pdb[0]
        residues_chain = [residue.id[1] for residue in chain_pdb.get_residues()]
        resseqs = list(map(str, residues_chain))
        residue_length, regions, units, insertion = [], [], [], []
        for j in file_db:
            line_data = j.rstrip().split('\t')[1]
            line_split = line_data.split(' ')
            if j.startswith('REG'):
                region = (f'{line_split[0]}', f'{line_split[1]}')
                regions.append(region)
            elif j.startswith('UNIT'):
                unit_one = line_split
                units.append(unit_one)
            elif j.startswith('INS'):
                if line_split[0] == '-':
                    pass
                else:
                    ins_one = line_split
                    insertion.append(ins_one)
            else:
                pass
        reg_and_unit=[]
        for k in range(len(regions)):
            try:
                os.mkdir(f'{outputDir}/{chain}/{targetname}_temp_{regions[k][0]}_{regions[k][1]}')
            except FileExistsError:
                pass
            residue_region=resseqs[resseqs.index(str(regions[k][0])): resseqs.index(str(regions[k][1]))+1]
            units_in_region, ins_in_region, highlighted_unit = [], [], []
            for i in range(len(units)):
                if units[i][0] in residue_region:
                    units_in_region.append(units[i])
            reg_and_unit.append(f'{regions[k]}\t{len(units_in_region)}')
            if not insertion:
                pass
            else:
                for r in range(len(insertion)):
                    for u in range(len(units_in_region)):
                        if int(units_in_region[u][0]) <= int(insertion[r][0]) < int(units_in_region[u][1]):
                            ins_in_region.append(insertion[r])
            for l in range(len(units_in_region)):
                unit_db=open(f'{outputDir}/{chain}/{targetname}_temp_{regions[k][0]}_{regions[k][1]}/{targetname}_{units_in_region[l][0]}_{units_in_region[l][1]}.pdb', 'w+')
                if (residue_region[residue_region.index(str(units_in_region[l][0]))]== residue_region[residue_region.index(str(units_in_region[l][0]))]) and (residue_region[residue_region.index(str(units_in_region[l][1]))] == residue_region[residue_region.index(str(units_in_region[l][1]))]):
                    result = residue_region[residue_region.index(str(units_in_region[l][0])): residue_region.index(str(units_in_region[l][1]))+1]
                elif (residue_region[residue_region.index(str(units_in_region[l][0]))]== residue_region[residue_region.index(str(units_in_region[l][0]) +'A')]) and (residue_region[residue_region.index(str(units_in_region[l][0]))]== residue_region[residue_region.index(str(units_in_region[l][0])+'A')]):
                    result = residue_region[residue_region.index(str(units_in_region[l][0]) + 'A'): residue_region.index(str(units_in_region[l][0]) + 'A') + 1]
                elif (residue_region[residue_region.index(str(units_in_region[l][0]))]== residue_region[residue_region.index(str(units_in_region[l][0])+'A')]) and (residue_region[residue_region.index(str(units_in_region[l][0]))]== residue_region[residue_region.index(str(units_in_region[l][0]))]):
                    result = residue_region[residue_region.index(str(units_in_region[l][0]) + 'A'): residue_region.index(str(units_in_region[l][0])) + 1]
                elif (residue_region[residue_region.index(str(units_in_region[l][0]))] == residue_region[residue_region.index(str(units_in_region[l][0]))]) and (residue_region[residue_region.index(str(units_in_region[l][0]))] == residue_region[residue_region.index(str(units_in_region[l][0]) + 'A')]):
                    result = residue_region[residue_region.index(str(units_in_region[l][0])): residue_region.index(str(units_in_region[l][0]) + 'A') + 1]
                else:
                    logging.error(f'The protein {targetname}{chain} has something wrong with residues...{residue_region}')
                    sys.exit()

                for g in range(len(result)):
                    for line in chain_pdb_file:
                        if line[22:26] != '':
                            if line[0:6] == "ATOM  ":
                                if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == result[g]:
                                    unit_db.write(line)
                unit_db.close()
                if not ins_in_region:
                    pass
                else:
                    for t in range(len(ins_in_region)):
                        if ins_in_region[t][0] in result:
                            if units_in_region[l] in highlighted_unit:
                                pass
                            else:
                                highlighted_unit.append(units_in_region[l])
                        else:
                            pass
            tm_score = []
            for u1 in os.listdir(f'{outputDir}/{chain}/{targetname}_temp_{regions[k][0]}_{regions[k][1]}'):
                for u2 in os.listdir(f'{outputDir}/{chain}/{targetname}_temp_{regions[k][0]}_{regions[k][1]}'):
                    cmd = 'TMalign {0}/{1}/{2}_temp_{3}_{4}/{5} {0}/{1}/{2}_temp_{3}_{4}/{6}'.format(outputDir, chain, targetname, regions[k][0], regions[k][1], u1, u2).split()
                    with subprocess.Popen(cmd, stdout=subprocess.PIPE, encoding="utf-8") as proc:
                        out = proc.stdout.readlines()
                    for line in out:
                        if line.endswith('Chain_1)\n'):
                            tm_score.append('{0}\t{1}\t{2}'.format(u1, u2, float(line.split('=')[1].split('(')[0])))

            build_matrix(tm_score, regions[k], outputDir, chain, targetname, highlighted_unit)
    return reg_and_unit, residues_chain, model


def build_matrix(tm_score, regions, outputDir, chain, targetname, highlighted_unit):
    tm_matrix = pd.DataFrame([ln.split('\t') for ln in tm_score])
    tm_matrix.columns = ['unitA', 'unitB', 'TMscore']
    tm_matrix.to_csv(f'{outputDir}/{chain}/{targetname}_{regions[0]}_{regions[1]}_matrix.tsv', sep='\t', index=False)

    tm_matrix.ix[:, 0] = tm_matrix.ix[:, 0].map(
        lambda x: x.lstrip('{}'.format(targetname.split('.')[0])).rstrip('.pdb'))
    tm_matrix.ix[:, 1] = tm_matrix.ix[:, 1].map(
        lambda x: x.lstrip('{}'.format(targetname.split('.')[0])).rstrip('.pdb'))
    tm_matrix.ix[:, 0] = tm_matrix.ix[:, 0].str.replace('^_', ' ').str.replace('_', '-')
    tm_matrix.ix[:, 1] = tm_matrix.ix[:, 1].str.replace('^_', ' ').str.replace('_', '-')

    prova = tm_matrix.pivot(index='unitA', columns='unitB', values='TMscore').reset_index().fillna(0)
    prova['sort'] = '0'
    for p in range(len(prova.unitA)):
        start_unit = prova.unitA[p].split('-')[0]
        if start_unit == ' ':
            start_unit = prova.unitA[p].split('-')[1]
        prova.sort[p] = prova.sort[p].replace(prova.sort[p], start_unit)
    prova['sort'] = prova['sort'].astype(int)
    prova.sort_values('sort', inplace=True, ascending=True)
    prova.reset_index(inplace=True)
    prova = prova.drop(['sort', 'index'], axis=1)
    prova.loc[-1] = prova.columns.values
    prova.index = prova.index + 1
    for s in range(1, len(prova.unitA)):
        if prova.loc[0][s].split('-')[0] == ' ':
            prova.loc[0][s] = prova.loc[0][s].replace(prova.loc[0][s], prova.loc[0][s].split('-')[1])
        else:
            prova.loc[0][s] = prova.loc[0][s].replace(prova.loc[0][s], prova.loc[0][s].split('-')[0])
    prova.loc[0]['unitA'] = prova.loc[0]['unitA'].replace(prova.loc[0]['unitA'], '0')

    prova.loc[0] = prova.loc[0].astype(float)
    prova = prova.sort_values(0, axis=1)
    prova = prova.drop(prova[prova.unitA == 0].index)
    prova.set_index('unitA', inplace=True)
    prova = prova.astype(float)

    f, ax = plt.subplots(figsize=(16, 7))
    for xtick in ax.xaxis.get_major_ticks():
        xtick.label.set_fontsize(14)
    for ytick in ax.yaxis.get_major_ticks():
        ytick.label.set_fontsize(14)

    if len(tm_matrix) > 10:
        font_size_cell = 10
    else:
        font_size_cell = 17
    sns.heatmap(prova, annot=True, linewidths=.5, cmap="BuPu", vmin=0, vmax=1, cbar=False,
                annot_kws={'fontsize': font_size_cell}, square=True)

    for h in highlighted_unit:
        label = f'{int(h[0])}-{int(h[1])}'
        wanted_index = prova.columns.get_loc(f' {label}')
        ax.get_xticklabels()[wanted_index].set_color("#f9a516")
        ax.get_yticklabels()[wanted_index].set_color("#f9a516")

    #################### OTHER MODE TO RAPRESENT THE INSERTION IN UNIT ###################
    # plt.gca().get_xticklabels()[wanted_index].set_color("red")
    # plt.gca().get_yticklabels()[wanted_index].set_color("red")
    # x, y, w, h = 0, wanted_index, N, 1
    # for _ in range(2):
    #     ax.add_patch(Rectangle((x, y), w, h, fill=False, edgecolor='purple', lw=3, clip_on=False))
    #     x, y = y, x  # exchange the roles of x and y
    #     w, h = h, w  # exchange the roles of w and h
    # ax.tick_params(length=0)
    #######################################################################################

    plt.yticks(rotation='horizontal')
    plt.xticks(rotation='vertical')

    plt.xlabel('')
    plt.ylabel('')
    # plt.show()
    ########################### SVG #############################
    # plt.savefig(f'{outputDir}/{chain}/{targetname}_{regions[0]}_{regions[1]}_matrix.svg',
    #             format='svg',
    #             dpi=1200,
    #             bbox_inches='tight',
    #             transparent=True)
    ########################### PNG ############################
    plt.savefig(f'{outputDir}/{chain}/{targetname}_{regions[0]}_{regions[1]}_matrix.png',
                format='png',
                dpi=1200,
                bbox_inches='tight',
                transparent=True)
    plt.close()
    logging.info(f'Created matrix for the region {regions[0]}_{regions[1]} of {targetname}{chain}.')
    pass


################################################ DO NOT REMOVE ###################################################
# def built_matrix(mustang_align, chains, targetname, repeatsDB):#, outputDir):#, resseqs, len_new_region=False):
#     TMscore, repeats=[], []
#     repeats.append('unitA unitB sequence_A sequence_B TMscore SeqId rmsd\n')
#     if os.path.basename(os.path.normpath(mustang_align)) != 'mustang_alignment':
#         pairwise_rep = open('{0}/{2}_{1}region1_PairwiseReport'.format(mustang_align, chains, targetname), 'w+')
#         try:
#             os.mkdir('{0}/all_vs_all'.format(mustang_align))
#         except OSError as exc:
#             if exc.errno != errno.EEXIST:
#                 raise
#             pass
#         for i in os.listdir('{0}/'.format(mustang_align)):
#             for j in os.listdir('{0}/'.format(mustang_align)):
#                 if i.startswith('{0}{1}_'.format(targetname, chains)):
#                     if not i.endswith('.png'):
#                         if j.startswith('{0}{1}_'.format(targetname, chains)):
#                             if not j.endswith('.png'):
#                                 os.system('{0} {1}/{2} {1}/{3} > {1}/all_vs_all/tm_score_all_vs_all.txt'.format(TMalignexe, mustang_align, i, j))
#                                 ml = open('{0}/all_vs_all/tm_score_all_vs_all.txt'.format(mustang_align)).readlines()
#                                 start_i = i.split('_')[1]
#                                 end_i = i.split('_')[2].split('.')[0]
#                                 start_j = j.split('_')[1]
#                                 end_j = j.split('_')[2].split('.')[0]
#                                 if (int(end_i)-int(start_i)) <= (int(end_j) - int(start_j)):
#                                     for line in ml:
#                                         if line.endswith('Chain_1)\n'):
#                                             TMscore.append('{0}\t{1}\t{2}'.format(i, j, float(line.split('=')[1].split('(')[0])))
#                                             tm_score=float(line.split('=')[1].split('(')[0])
#                                         if line.startswith('Aligned length'):
#                                             rmsd=float(line.split('=')[2].split(',')[0])
#                                             seq_id=float(line.split('=')[4].rstrip())
#                                     repeats.append('{0} {1} {2} {3} {4} {5} {6}\n'.format(i, j, ml[22], ml[24], tm_score, seq_id, rmsd))
#                                 else:
#                                     for line in ml:
#                                         if line.endswith('Chain_2)\n'):
#                                             TMscore.append('{0}\t{1}\t{2}'.format(i, j, float(line.split('=')[1].split('(')[0])))
#                                             tm_score = float(line.split('=')[1].split('(')[0])
#                                         if line.startswith('Aligned length'):
#                                             rmsd=float(line.split('=')[2].split(',')[0])
#                                             seq_id=float(line.split('=')[4].rstrip())
#                                     repeats.append('{0} {1} {2} {3} {4} {5} {6}\n'.format(i, j, ml[22], ml[24], tm_score, seq_id, rmsd))
#         #print(repeats)
#         for r in repeats:
#             pairwise_rep.write(r)
#         pairwise_rep.close()
#         #exit()
#         ################# RICOMINCIARE DA QUI ############################
#         return TMscore
#     else:
#         pairwise_rep = open('{0}/{1}/{2}_{1}region1_PairwiseReport'.format(mustang_align, chains, targetname), 'w+')
#         try:
#             os.mkdir('{0}/{1}/all_vs_all'.format(mustang_align, chains))
#         except OSError as exc:
#             if exc.errno != errno.EEXIST:
#                 raise
#             pass
#         for i in os.listdir('{0}/{1}/'.format(mustang_align, chains)):
#             for j in os.listdir('{0}/{1}/'.format(mustang_align, chains)):
#                 if i.startswith('{0}{1}_'.format(targetname, chains)):
#                     if not i.endswith('.png'):
#                         if j.startswith('{0}{1}_'.format(targetname, chains)):
#                             if not j.endswith('.png'):
#                                 os.system('{0} {1}/{2}/{3} {1}/{2}/{4} > {1}/{2}/all_vs_all/tm_score_all_vs_all.txt'.format(TMalignexe, mustang_align, chains, i, j))
#                                 ml = open('{0}/{1}/all_vs_all/tm_score_all_vs_all.txt'.format(mustang_align, chains)).readlines()
#                                 start_i=i.split('_')[1]
#                                 end_i=i.split('_')[2].split('.')[0]
#                                 start_j=j.split('_')[1]
#                                 end_j=j.split('_')[2].split('.')[0]
#                                 if (int(end_i)-int(start_i)) <= (int(end_j) - int(start_j)):
#                                     for line in ml:
#                                         if line.endswith('Chain_1)\n'):
#                                             TMscore.append('{0}\t{1}\t{2}'.format(i, j, float(line.split('=')[1].split('(')[0])))
#                                             tm_score = float(line.split('=')[1].split('(')[0])
#                                         if line.startswith('Aligned length'):
#                                             rmsd = float(line.split('=')[2].split(',')[0])
#                                             seq_id = float(line.split('=')[4].rstrip())
#                                     repeats.append('{0} {1} {2} {3} {4} {5} {6}\n'.format(i, j, ml[22], ml[24], tm_score, seq_id, rmsd))
#                                     '''i è la catena più piccola quindi è chain1'''
#                                 else:
#                                     for line in ml:
#                                         if line.endswith('Chain_2)\n'):
#                                             TMscore.append('{0}\t{1}\t{2}'.format(i, j, float(line.split('=')[1].split('(')[0])))
#                                             tm_score = float(line.split('=')[1].split('(')[0])
#                                         if line.startswith('Aligned length'):
#                                             rmsd = float(line.split('=')[2].split(',')[0])
#                                             seq_id = float(line.split('=')[4].rstrip())
#                                     repeats.append('{0} {1} {2} {3} {4} {5} {6}\n'.format(i, j, ml[22], ml[24], tm_score, seq_id, rmsd))
#                                     '''j è la catena più piccola'''
#         for r in repeats:
#             pairwise_rep.write(r)
#         pairwise_rep.close()
#         return TMscore
#
#
#
# def draw_matrix(TMscore, outputDir, chains, targetname, threshold_unit):
#     if not TMscore:
#         # logging.info('All the unit did not pass the threshold (tmscore x coverage) > {0}'.format(threshold_unit))
#         print('All the unit did not pass the threshold (tmscore x coverage) > {0}'.format(threshold_unit))
#         pass
#     else:
#         tm_matrix = pd.DataFrame([ln.split('\t') for ln in TMscore])
#         tm_matrix.columns = ['chain_1', 'chain_2', 'score']
#         prova = tm_matrix.pivot(index='chain_1', columns = 'chain_2', values = 'score').reset_index().fillna(0)
#
#         prova['sort'] = '0'
#         for i in range(len(prova.chain_1)):
#             start_unit=prova.chain_1[i].split('_')[1]
#             prova.sort[i] = prova.sort[i].replace(prova.sort[i], start_unit)
#         prova['sort'] = prova['sort'].astype(float)
#         prova.sort_values('sort', inplace=True, ascending=True)
#         prova.reset_index(inplace=True)
#         prova=prova.drop(['sort', 'index'], axis=1)
#         prova.loc[-1] = prova.columns.values
#         prova.index = prova.index + 1
#         for j in range(1, len(prova.chain_1)):
#             prova.loc[0][j] = prova.loc[0][j].replace(prova.loc[0][j], prova.loc[0][j].split('_')[1])
#         prova.loc[0]['chain_1'] = prova.loc[0]['chain_1'].replace(prova.loc[0]['chain_1'], '0')
#         prova.loc[0] = prova.loc[0].astype(float)
#         prova = prova.sort_values(0, axis=1)
#         prova = prova.drop(prova[prova.chain_1 == 0].index)
#         prova.set_index('chain_1', inplace=True)
#         prova = prova.astype(float)
#         f, ax = plt.subplots(figsize=(8, 5))
#         sns.heatmap(prova, annot=True, linewidths=.5, ax=ax, cmap="Blues", vmin=0, vmax=1)
#         sns.set()
#         if os.path.basename(os.path.normpath(outputDir)) != targetname:
#             plt.savefig('{0}/{2}{1}_unit_matrix.png'.format(outputDir,chains, targetname), dpi=300) #######change the directory in wich you want to save the matrix if there is a second region (os.path.basename(os.path.normpath(mustang_align)) != chains)
#         else:
#             plt.savefig('{0}/{1}/{2}{1}_unit_matrix.png'.format(outputDir, chains, targetname), dpi=300) #######change the directory in wich you want to save the matrix if there is a second region (os.path.basename(os.path.normpath(mustang_align)) != chains)
#         plt.close()

