from zipfile import ZipFile

from Bio.PDB import *
from Bio.PDB.DSSP import DSSP
import os
from configurationFile import *
import subprocess as sp
from subprocess_function import *

import pandas as pd
import numpy as np
from string import whitespace
import fileinput
import shutil


def write_info_file(targetname, outputDir, chains, reg_and_unit):
    pdb = PDBParser().get_structure(targetname, f'{outputDir}/{targetname}.pdb')
    with open(f'{outputDir}/{targetname}.info', 'w+') as info_file:
        head = pdb.header['head']
        journal = pdb.header['journal']
        structure_method = pdb.header['structure_method']
        journal_reference = pdb.header['journal_reference']
        compound = pdb.header['compound']
        keywords = pdb.header['keywords']
        name = pdb.header['name']
        author = pdb.header['author']
        deposition_date = pdb.header['deposition_date']
        release_date = pdb.header['release_date']
        source = pdb.header['source']
        resolution = pdb.header['resolution']
        structure_reference = pdb.header['structure_reference']

        info_file.write(f'structure_method {structure_method}\nhead {head}\njournal {journal}\n'
                        f'journal_reference {journal_reference}\ncompound {compound}\nkeywords {keywords}\n'
                        f'name {name}\nauthor {author}\ndeposition_date {deposition_date}\n'
                        f'release_date {release_date}\nsource {source}\nresolution {resolution}\n'
                        f'structure_reference {structure_reference}\n')

        if int(reg_and_unit[0].split('\t')[1].rstrip()) >= n_unit_rep:
            info_file.write('chain {} Predicted as Repeated'.format(chains))
        else:
            info_file.write(f'chain {chains} Predicted as Non Repeated')


def mapp_file(targetname, chains, outputDir, resseqs, model):
    with open(f'{outputDir}/{chains}/{targetname}_{chains}.mapping', 'w+') as mapping_file:
        dssp = DSSP(model, f'{outputDir}/{chains}/{targetname}_{chains}.pdb', dssp='mkdssp')
        for j, k in zip(range(len(dssp.keys())), range(len(resseqs))):
            a_key = list(dssp.keys())[j]
            mapping_file.write(f'{dssp[a_key][0]}\t{resseqs[k]}\t{dssp[a_key][1]}\t{dssp[a_key][2]}\n')
    return dssp


def header_pdb(outputDir, targetname):
    pdb_file = open(f'{outputDir}/{targetname}.pdb').readlines()
    with open(f'{outputDir}/header', 'w+') as header_file:
        for i in pdb_file:
            if not i.startswith('ATOM'):
                # or (not i.startswith('TER')) or (not i.startswith('HETATM')) or (not i.startswith('CONECT')) or (
                    # not i.startswith('MASTER')) or (not i.startswith('END'))
                header_file.write(i)
            else:
                break


def make_region_one(outputDir, targetname, chains):
    '''
        NetworkReport = unitA unitB TMscore
        PairwiseReport = unitA unitB sequence_A sequence_B TMscore SeqId rmsd
        .afasta file = fasta file coming from mustang
        aligned_dssp = multi sequence secondary structure alignment
        mus = mustang description file
        mustang.pdb = mustang superimposition alignment
        matrix.png = multialignment matrix
        rms_rot = rmsd rotation matrix
        units.zip = zipped archive with all unit's PDBs file
    '''

    dirlist = [item for item in os.listdir(f'{outputDir}/{chains}')
               if os.path.isdir(os.path.join(f'{outputDir}/{chains}', item))]
    for d in range(len(dirlist)):
        if dirlist[d].startswith('Region_'):
            pass
        else:
            '''Create a folder for each region'''
            os.mkdir(f'{outputDir}/{chains}/Region{d+1}')

            '''mustang_description_file'''
            with open(f'{outputDir}/{chains}/Region{d+1}/{targetname}_{chains}region{d+1}.mus', 'w+') as mustang_desc_file:
                mustang_desc_file.write(f'>{outputDir}/{chains}/{dirlist[d]}\n')
                for file in os.listdir(f'{outputDir}/{chains}/{dirlist[d]}'):
                    if file.endswith('.pdb') and file.startswith(targetname):
                        mustang_desc_file.write(f'+ {file}\n')

            '''mustang or mTM-align'''
            '''These methods include the insertions in the units'''
            return_code_mustang = sub_mustang(f'{outputDir}/{chains}/Region{d+1}', targetname, chains, (d+1), repeatsDB=True)
            if return_code_mustang == 0:
                logging.info(f'The exit code of Mustang is 0. The algorithm will go on...')
                pass
            else:
                logging.info(f'The exit code of Mustang is {return_code_mustang}. Obtain the superimposition informations with mTMalign...')
                # return_code_tmalign = sub_mtmalign(f'{outputDir}/{chains}/{dirlist[d]}', targetname, chains, (d+1), dirlist[d])
                # if return_code_tmalign == 0:
                #
                #     pass
                # else:
                #     logging.info(f'The exit code of mTMalign is is different than 0 too: {return_code_mustang}. Continue without superimposition .pdb...')


            '''create PairwiseReport'''
            with open(f'{outputDir}/{chains}/Region{d+1}/{targetname}_{chains}region{d+1}_PairwiseReport', '+w') as pairwise_report:
                pairwise_report.write('unitA unitB sequence_A sequence_B TMscore SeqId rmsd\n')
                for i in os.listdir(f'{outputDir}/{chains}/{dirlist[d]}'):
                    for j in os.listdir(f'{outputDir}/{chains}/{dirlist[d]}'):
                        output_tmalign = sub_tmalign(i, j, f'{outputDir}/{chains}/{dirlist[d]}',
                                                     chains, targetname, repeatsDB=True, library=False)
                        seq_i = output_tmalign[22].rstrip()
                        seq_j = output_tmalign[24].rstrip()
                        rmsd = float(output_tmalign[16].split('RMSD=')[1].split(',')[0])
                        seq_id = float(output_tmalign[16].split('Seq_ID=')[1].split('=')[1].rstrip())
                        tm_score = float(output_tmalign[17].split('(')[0].split('=')[1])
                        pairwise_report.write(f'{i.split(".")[0]}_reg{d+1} {j.split(".")[0]}_reg{d+1} {seq_i} {seq_j} {tm_score} {seq_id} {rmsd}\n')

            '''create aligned-dssp'''
            p = PDBParser()
            prova, df_name = [], []
            ss_file=open(f'{outputDir}/{chains}/Region{d+1}/{targetname}_{chains}region{d+1}.aligned_dssp', 'w+')
            afasta_file = open(f'{outputDir}/{chains}/Region{d+1}/{targetname}_{chains}region{d+1}.mustang.afasta').readlines()
            mapping_file = open(f'{outputDir}/{chains}/{targetname}_{chains}.mapping').readlines()
            structure = p.get_structure(f"{targetname}_{chains}", f"{outputDir}/{chains}/{targetname}_{chains}.pdb")
            model = structure[0]
            resseqs = [residue.id[1] for residue in structure.get_residues()]
            resseqs_1 = list(map(str, resseqs))
            dssp = DSSP(model, f"{outputDir}/{chains}/{targetname}_{chains}.pdb", dssp='mkdssp')
            for l in afasta_file:
                prova.append('{}'.format(l))
            prova.append('\n')
            idx_list = [idx for idx, val in enumerate(prova) if val == '\n']
            new_file, line, ss = [], [], []
            for ind, v in enumerate(prova):
                if v.startswith('>'):
                    start = v.split('>')[1].split('_')[1]
                    end = v.split('>')[1].split('_')[2].split('.')[0].rstrip()
                    result = resseqs_1[resseqs_1.index(start):resseqs_1.index(end) + 1]
                    new_file.append(v)
                for res in range(len(result)):
                    for m in mapping_file:
                        if result[res] == m.split('\t')[1]:
                            ss.append(m.split('\t')[3].rstrip())
                for k in idx_list:
                    if k-1 == ind:
                        line = []
                        counter = 0
                        for val in v:
                            if val == '-':
                                line.append(val)
                            else:
                                if counter == len(ss):
                                    break
                                else:
                                    line.append(ss[counter])
                                    counter += 1
                        new_file.append('{}\n'.format(''.join(map(str, line))))
            for b in new_file:
                ss_file.write(b)
            ss_file.close()

            '''make zipped archive for units'''
            with ZipFile(f'{outputDir}/{chains}/{targetname}_{chains}region{d+1}_units.zip', 'w') as zipObj2:
                for file in os.listdir(f'{outputDir}/{chains}/{dirlist[d]}'):
                    zipObj2.write(f'{outputDir}/{chains}/{dirlist[d]}/{file}',
                                  os.path.basename(f'{outputDir}/{chains}/{dirlist[d]}/{file}'))

            os.system(f'mv {outputDir}/{chains}/{targetname}_{chains}region{d+1}_units.zip '
                      f'{outputDir}/{chains}/Region{d+1}/  2>/dev/null')
            os.system(f'mv {outputDir}/{chains}/{targetname}_{dirlist[d].split("_")[2]}_{dirlist[d].split("_")[3]}_matrix.png '
                      f'{outputDir}/{chains}/Region{d + 1}/{targetname}_{chains}region{d+1}_matrix.png 2>/dev/null')
            os.system(f'mv {outputDir}/{chains}/{targetname}_{dirlist[d].split("_")[2]}_{dirlist[d].split("_")[3]}_matrix.tsv '
                      f'{outputDir}/{chains}/Region{d + 1}/{targetname}_{chains}region{d+1}_NetworkReport 2>/dev/null')

        shutil.rmtree(f'{outputDir}/{chains}/{dirlist[d]}')