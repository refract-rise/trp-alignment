import math, os, sys
import argparse, errno, getopt, logging, shutil, urllib.request, warnings
from shutil import copyfile
from time import time
from Bio import BiopythonWarning
from Bio.PDB import PDBParser, PDBIO
import pandas as pd

from configurationFile import *
from mainFunction import *

warnings.simplefilter('ignore', BiopythonWarning)
#python repeats_01_08.py -w . -i 1aom -c - -o 1aom -d /home/sara/Downloads

print(sys.version)
#print Bio.__version__
argv = sys.argv[1:]
parser = argparse.ArgumentParser(description="New version of reupred.py.")
parser.add_argument("-w", "--workingdir", required=True, help="Set a working directory for the project")
parser.add_argument("-i", "--inputf", required=True, help="Write in input the name of the protein")
parser.add_argument("-c", "--chain", required=True, help="Indicate the Chain. PUT THE OPTION IN UPPER CHARACTERS")
parser.add_argument("-d", "--database", required=True, help="Indicate the folder in which there is the protein.pdb file")
parser.add_argument("-o", "--output", required=True, help="Set an output directory in which you can find all the results")
args = parser.parse_args()

opts, args = getopt.getopt(argv, "hi:w:c:d:o:", ["inputf=", "workingdir=", "chain=", "database=", "output="])

for opt, arg in opts:
    if opt == "-h":
        print(parse_args)
        sys.exit()
    elif opt in ("-w", "--workingdir"):
        wdirOriginal = arg
    elif opt in ("-i", "--inputf"):
        targetname = arg
    elif opt in ("-c", "--chain"):
        chain = arg
    elif opt in ("-d", "--database"):
        database = arg
    elif opt in ("-o", "--output"):
        outputDir = arg
        odir = arg+'Temporary/'

if os.path.exists(outputDir):
    print("Directory", targetname,  "already exists")
else:
    os.mkdir(outputDir)
    print("Directory", targetname,  "created ")

#initialize log_file
initial_time = time()
LOG = '{0}/reupred_{1}.log'.format(outputDir, targetname)
if os.path.isfile(LOG):
    os.remove(LOG)
print("Start creating .log file ...")
logging.basicConfig(filename=LOG, level=logging.INFO, format='%(asctime)s | %(name)s | %(threadName)s | %(levelname)s | %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
logging.info("Starting .....")
logging.info("Running predictor: {0} \n\t\t\t\t\t\t The directory where it is: {1} \n\t\t\t\t\t\t The chain is: {2} \n\t\t\t\t\t\t The output directory is: {3}".format(targetname, wdirOriginal, chain, outputDir))

#dowload the .pdb file if it's not in the database
if os.path.exists('{0}/{1}.pdb'.format(database, targetname)):
    copyfile('{0}/{1}.pdb'.format(database, targetname), '{0}/{1}.pdb'.format(outputDir, targetname))
    logging.info("The protein {0} in in the database.".format(targetname))
else:
    url = urllib.request.urlretrieve('http://files.rcsb.org/download/{0}.pdb'.format(targetname), '{0}/{1}.pdb'.format(outputDir, targetname))
    logging.info("The protein {0} is not in the database... \n\t\t\t\t\t\t Download {0} from Protein Data Bank (PDB)...".format(targetname))

temp_dir = '{0}/Temporary'.format(outputDir)
align = '{0}/TMalign_alignment'.format(temp_dir)
if os.path.exists(temp_dir):
    shutil.rmtree(temp_dir, ignore_errors=True)
create_folder(outputDir)

#count the number of chians in .pdb file and create .pdb files for every chain
io = PDBIO()
pdb = PDBParser().get_structure(targetname, '{0}/{1}.pdb'.format(outputDir, targetname))
#chains = []
for chain in pdb.get_chains():
    io.set_structure(chain)
    chains.append(chain.get_id())
    #resseqs = [residue.id[1] for residue in pdb.get_chains()]
    ########################### this will be remove after the end of the code #####################
    try:
        os.mkdir('{0}/{1}'.format(outputDir, chain.get_id()))
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        pass
    ###############################################################################################
    io.save('{0}/{1}/{2}_{1}.pdb'.format(outputDir, chain.get_id(), pdb.get_id()))
logging.info("There are {0} chains in {1}.pdb: {2}".format(len(chains), targetname, chains))

#Align every chain with SRUL
logging.info("Starting alignment with TMalign...")
for i in range(len(chains)):
    print('This is the number of chain:', len(chains))
    print('Chain: {0}'.format(chains[i]))
    max_score, chain2, master_seq = [], [], []
    logging.info("Align chain {0} against the SRUL...".format(chains[i]))
    os.mkdir('{0}/{1}'.format(align, chains[i]))
    for j in os.listdir(SRUL):
        os.system('{3} {0}/{1}/{0}_{1}.pdb {2}/{4} >> {5}/{1}/tm_output.txt 2>{5}/{1}/tm_output.log'.format(outputDir, chains[i], SRUL, TMalignexe, j, align)) #2>{5}/{1}/tm_output.log

    #Pick the higher value of tm_score for master choice
    fl = open('{0}/{1}/tm_output.txt'.format(align, chains[i])).readlines()
    for ln in fl:
        if ln.startswith('Name of Chain_2'):
            name = ln.split(':')[1].split('/')[3]
        if ln.startswith('TM-score'):
            if ln.endswith('_2)\n'):
                tm_score = ln.split('(')[0]
                value = tm_score.split(' ')[1]
                max_score.append(name + value)
    df = pd.DataFrame({'unit': max_score})
    df = pd.DataFrame(df.unit.str.split(' ', 1).tolist(), columns=['unit', 'tm_score'])
    df = df.replace('\n', ' ', regex=True)
    tm_score_max = df['tm_score'].max()
    master = df[df['tm_score'] == tm_score_max]
    logging.info("The master for chain {0} is: {1} \n\t\t\t\t\t\t with a score of: {2}".format(chains[i], master.iloc[0]['unit'], master.iloc[0]['tm_score']))

    #Define the master unit on the target
    for ln in fl:
        if '::' in ln:
            chain2.append(ln)
    df2 = pd.DataFrame({'unit': chain2})
    master_df = df2.iloc[master.index.tolist()].iloc[0]['unit'].rstrip()
    for idx, val in enumerate(master_df):
        if val == ':':
            master_seq.append(idx)
            #print master_seq
            #print chains[i], idx
    logging.info("prova for master_seq {0}".format(master_seq))

    #find the master on the protein file, save it in a .pdb file and save the pre and the post master. Align the "protein master" against the protein without the master region
    pl = open('{0}/{2}/{1}_{2}.pdb'.format(outputDir, targetname, chains[i])).readlines()
    pre = open('{0}/{1}/{2}_{1}_pre_0.pdb'.format(align, chains[i], targetname), 'w+')
    post = open('{0}/{1}/{2}_{1}_post_0.pdb'.format(align, chains[i], targetname), 'w+')
    master_pdb = open('{0}/{1}/{2}_{1}_temp.pdb'.format(align, chains[i], targetname), 'w+')
    pdb_chain = PDBParser().get_structure('{0}_{1}'.format(targetname, chains[i]), '{0}/{2}/{1}_{2}.pdb'.format(outputDir, targetname, chains[i]))
    resseqs = [residue.id[1] for residue in pdb_chain.get_residues()]

    #save pre_and post master .pdb
    pre_master, post_master, protein_master = [], [], [] #no_master=[]
    for line in pl:
        if line[22:26] != '':
            if int(line[22:26]) < int(resseqs[master_seq[0]]):
                pre_master.append(line)
                pre.write(line)
                #sum(1 for _ in pre_master) #count number of lines in a file/array
            if int(line[22:26]) > int(resseqs[master_seq[len(master_seq)-1]]):
                if line[0:6] == "ATOM  ":
                    post_master.append(line)
                    post.write(line)
            for k in master_seq:
                if resseqs[k] == int(line[22:26]):
                    protein_master.append(line)
                    len_master = len(protein_master)
                    master_pdb.write(line)

    ######## STEP 3 ############ VERSION 2
#    master_chain = PDBParser().get_structure('{0}_{1}_master'.format(targetname, chains[i]), '{0}/{1}/{2}_{1}_temp.pdb'.format(align, chains[i], targetname))
#    resseqs_master = [residue.id[1] for residue in master_chain.get_residues()]
#    pre_chain = PDBParser().get_structure('{0}_{1}_pre'.format(targetname, chains[i]), '{0}/{1}/{2}_{1}_pre.pdb'.format(align, chains[i], targetname))
#    resseqs_pre = [residue.id[1] for residue in pre_chain.get_residues()]
#    post_chain = PDBParser().get_structure('{0}_{1}_post'.format(targetname, chains[i]), '{0}/{1}/{2}_{1}_post.pdb'.format(align, chains[i], targetname))
#    resseqs_post = [residue.id[1] for residue in post_chain.get_residues()]
#    n_part_pre = int(math.ceil(float(len(resseqs_pre))/float(len(resseqs_master))))
#     n_part_post = int(math.ceil(float(len(resseqs_post))/float(len(resseqs_master))))
#     #n_part_pre = len(resseqs_pre)/len(resseqs_master)
#     #n_part_post = len(resseqs_post)/len(resseqs_master)
#     percentage = (xcent * len(resseqs_master)) / 100
#
#     pre_part = [resseqs_pre[x:x+len(resseqs_master)] for x in xrange(len(resseqs_pre), 0, -len(resseqs_master))] #this is for split  from the bottom
#     post_part = [resseqs_post[x:x+len(resseqs_master)] for x in xrange(0, len(resseqs_post), len(resseqs_master))] #this is for normal splitting

    #save every single part in an array and align everytime with the master
    #POST
#     for n in range(n_part_post-1):
#         post_n = open('{0}/{1}/{2}_{1}_post_n{3}.pdb'.format(align, chains[i], targetname, n), 'w+')
#         part_n = []
#         for l in post_part[n]:
#             for g in pl:
#                 if g[22:26] != '':
#                     if l == int(g[22:26]):
#                         part_n.append(g)
#                         post_n.write(g)
#
#     #allinea tutte la divisioni con la master#
#         os.system('{3} {4}/{1}/{0}_{1}_temp.pdb {4}/{1}/{0}_{1}_post_n{5}.pdb > {4}/{1}/tm_post_output_n{5}.txt'.format(outputDir, chains[i], SRUL, TMalignexe, align, n))
#     #    if n_part_post[len(n_part_post)-1]:

   ######## STEP 3 ############ VERSION 1
    ##PRE
    #copyfile('{2}/{1}/{0}_{1}_pre.pdb'.format(outputDir, chains[i], align), '{2}/{1}/{0}_{1}_pre_0.pdb'.format(outputDir, chains[i], align))
    #os.system('{3} {4}/{1}/{0}_{1}_temp.pdb {4}/{1}/{0}_{1}_pre_0.pdb > {4}/{1}/tm_pre_output_0.txt'.format(outputDir, chains[i], SRUL, TMalignexe, align))
    new_post = post_master
    new_pre = pre_master
    num_ress_protein_master = int(protein_master[len(protein_master) - 1][23:29]) - int(protein_master[0][23:29])  # lenght of master
    percentage = (xcent * num_ress_protein_master) / 100  # xcent is a variable for the percetange of the length to pick. The default value is 10
    new_seq_new, resseq_tmscore=[], []

    for x in range(1, number_of_repetition):#int(math.ceil(residue_chain/num_ress_protein_master)+1)):
        if not new_pre:
            break
        #print new_pre
        #print 'This is new_pre[0]', new_pre[0]
        num_ress_pre_master = int(new_pre[len(new_pre)-1][23:29])-int(new_pre[0][23:29]) #length of pre_master
        #num_ress_post_master = int(new_post[len(new_post)-1][23:29])-int(new_post[0][23:29])
        print('x is:', x)
        pre_master_x, post_master_x, master_first = [], [], []
        if num_ress_pre_master > int(percentage):
            count = 0
            index, new_seq, master_i = [], [], []
            #pre_master_x, post_master_x, master_first = [], [], []
            print('percentage is:', percentage)
            print('num_ress_pre_master is:', num_ress_pre_master)
            #os.system('{3} {4}/{1}/{0}_{1}_temp.pdb {4}/{1}/{0}_{1}_pre_0.pdb > {4}/{1}/tm_pre_output_1.txt'.format(outputDir, chains[0], SRUL, TMalignexe, align))
            os.system('{3} {4}/{1}/{0}_{1}_temp.pdb {4}/{1}/{0}_{1}_pre_{5}.pdb > {4}/{1}/tm_pre_output_{6}.txt'.format(outputDir, chains[i], SRUL, TMalignexe, align, (x-1), x))
            #ml = open('{1}/{0}/tm_pre_output_1.txt'.format(chains[0], align)).readlines()
            ml = open('{1}/{0}/tm_pre_output_{2}.txt'.format(chains[i], align, x)).readlines()
            #parse_tmalign(ml)

            for ln in ml:
                if '::' in ln:
                    for indx, value in enumerate(ln):
                        if value == ':':
                            master_i.append(indx)

            for l in range(master_i[0], master_i[len(master_i)-1]):
                for ind, line in enumerate(ml):
                    if ind == 24:
                        line = line.rstrip()
                        for idx, val in enumerate(line):
                            if idx == l:
                                if val == '-':
                                    count = count+1
            print('count is:', count)
#            exit()
            for ind, line in enumerate(ml):
                if ind == 24:
                    line = line.rstrip()
                    if line.startswith('-'):
                        for idx, val in enumerate(line):
                            if val != '-':
                                index.append(idx)
                        for ln in ml:
                            if '::' in ln:
                                for indx, value in enumerate(ln):
                                    if value == ':':
                                        new_seq.append(indx-int(index[0]))
                    else:
                        #chain_2=[]
                        for ln in ml:
                            if '::' in ln:
#                                chain_2.append(ln)
                         #       for g in chain_2:
                                for indx, value in enumerate(ln):
                                    if value == ':':
                                        new_seq.append(indx)

            # #se la linea che individua il pre nel tmoutput inizia con '-' eliminali prima di trovare gli indici di dove cade la master
            # for ind, line in enumerate(ml):
            #     if ind == 24:
            #         if line.startswith('-'):
            #             for idx, val in enumerate(line):
            #                 if val != '-':
            #                     index.append(idx)
            #             #chain_2 = []
            #             for ln in ml:
            #                 if '::' in ln:
            #             #         chain_2.append(ln)
            #             # for g in chain_2:
            #                     for indx, value in enumerate(ln):
            #                         if value == ':':
            #                             new_seq.append(indx-int(index[0]))
            #         else:
            #             chain_2=[]
            #             for ln in ml:
            #                 if '::' in ln:
            #                     chain_2.append(ln)
            #             for g in chain_2:
            #                 for indx, value in enumerate(g):
            #                     if value == ':':
            #                         new_seq.append(indx)
            print('new_seq is:', new_seq)
            #if not new_seq:
            #   break

            #print new_seq
            if new_seq == new_seq_new:
                break

            #pre_x = open('{0}/{1}/{2}_{1}_pre_1.pdb'.format(align, chains[0], targetname), 'w+')
            pre_x = open('{0}/{1}/{2}_{1}_pre_{3}.pdb'.format(align, chains[i], targetname, x), 'w+')
            #post_x = open('{0}/{1}/{2}_{1}_post_1.pdb'.format(align, chains[0], targetname), 'w+')
            post_x = open('{0}/{1}/{2}_{1}_post_{3}.pdb'.format(align, chains[i], targetname, x), 'w+')
            #pl_x = open('{2}/{1}/{0}_{1}_pre_0.pdb'.format(outputDir, chains[0], align)).readlines()  # maybe original pdb directly...
            pl_x = open('{0}/{1}/{2}_{1}_pre_{3}.pdb'.format(align, chains[i], targetname, (x-1))).readlines()
            #pdb_chain_x = PDBParser().get_structure('{0}_{1}_pre'.format(targetname, chains[0]), '{0}/{2}/{1}_{2}_pre_0.pdb'.format(align, targetname, chains[0]))
            pdb_chain_x = PDBParser().get_structure('{0}_{1}_pre'.format(targetname, chains[i]), '{0}/{2}/{1}_{2}_pre_{3}.pdb'.format(align, targetname, chains[i], (x-1)))
            resseqs_x = [residue.id[1] for residue in pdb_chain_x.get_residues()]
            #print resseqs_x

            #pre_master_x, post_master_x, master_first = [], [], []
            for line in pl_x:
                if line[22:26] != '':
                    #print line[22:26]
                    if int(line[22:26]) < int(resseqs_x[new_seq[0]]):
                        pre_master_x.append(line)
                        pre_x.write(line)
                    #print new_seq
                    #print resseqs_x
                    if int(line[22:26]) > int(resseqs_x[new_seq[len(new_seq)-1-count]]):
                        post_master_x.append(line)
                        post_x.write(line)
            print('resseqs_x is:', resseqs_x)

# #qui c'era la heatmap
            if pre_master_x == new_pre:
                break

        # elif num_ress_post_master > percentage:
        #     pl_x = open('{0}/{1}/{2}_{1}_post_{3}.pdb'.format(align, chains[i], targetname, (x-1))).readlines()
        #     post_x = open('{0}/{1}/{2}_{1}_post_{3}.pdb'.format(align, chains[i], targetname, x), 'w+')
        #     for line in pl_x:
        #         if line[22:26] != '':
        #             if int(line[22:26]) > int(resseqs[new_seq[len(new_seq) - 1]]):
        #                 post_master_x.append(line)
        #                 post_x.write(line)

        ######################## HEATMAP FOR VISUALIZATION ####################
        #    prova=[]
        #    heatmap_tmscore_on_chain(ml, pl_x, resseqs_x, new_seq, chains[i])
        #######################################################################

        else:
            break
        #new_post = post_master_x
        new_pre = pre_master_x
        new_seq_new = new_seq



#########################################
#prove per eliminare '-' da dentro la sequenza
#index=[]
#for ind, line in enumerate(ml):
#   if ind==24:
#       line=line.rstrip()
#       if line.startswith('-'):
#           for idx, val in enumerate(line):
#               if val != '-':
#                   index.append((idx, val))

