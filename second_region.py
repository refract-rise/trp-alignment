from mainFunction import *
from insertionFunction import *
import subprocess_function as sp
from manage_unit_from_mustang import *

######################### reiguardare questa funzione ##############################
def possibly_other_region(data, targetname, outputDir, chains, pl, resseqs, align, mustang_align):#, name_not_aligned):#, resseqs):
    resseqs_1 = list(map(str, resseqs))
    start_aligned_region = str(data.START[0])
    end_aligned_region = str(data.END[len(data) - 1])
    ress_pre = resseqs_1[: resseqs_1.index(start_aligned_region)]
    ress_post = resseqs_1[resseqs_1.index(end_aligned_region)+1: ]#resseqs_1.index(resseqs_1[len(resseqs_1)-1])]

    if not ress_pre:
        if not ress_post:
            logging.info('There is not another region for the chain {}'.format(chains))
            pass
        else:
            logging.info('The other region is located AFTER the previous region')
            chain_not_aligned_POST = open('{0}/{1}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress_post[0], ress_post[len(ress_post) - 1]), 'w+')

            for i in range(len(ress_post)):
                for line in pl:
                    if line[22:26] != '':
                        if line[0:6] == "ATOM  ":
                            if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == ress_post[i]:  # remove whitespace from a sting
                                chain_not_aligned_POST.write(line)
            chain_not_aligned_POST.close()
            create_second_region_folder(outputDir, chains, align, ress_post, mustang_align)
            os.system('mv {0}/{1}/{2}_{1}_{3}_{4}.pdb {0}/{1}/Region_{3}_{4}/{2}_{1}_{3}_{4}.pdb 2>/dev/null'.format(outputDir, chains, targetname, ress_post[0], ress_post[len(ress_post) - 1]))  # copyfile('{0}/{2}_{1}_{3}_PRE.pdb'.format(outputDir, chains, targetname, resseqs), '{0}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress_pre[0], ress_pre[len(ress_pre)-1]))
    elif not ress_post:
        if not ress_pre:
            logging.info('There is not another region for the chain {}'.format(chains))
            pass
        else:
            logging.info('The other region is located BEFORE the prevous region')
            chain_not_aligned_PRE = open('{0}/{1}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress_pre[0], ress_pre[len(ress_pre)-1]), 'w+')
            for i in range(len(ress_pre)):
                for line in pl:
                    if line[22:26] != '':
                        if line[0:6] == "ATOM  ":
                            if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == ress_pre[i]:  # remove whitespace from a sting
                                chain_not_aligned_PRE.write(line)
            chain_not_aligned_PRE.close()
            create_second_region_folder(outputDir, chains, align, ress_pre, mustang_align)
            os.system('mv {0}/{1}/{2}_{1}_{3}_{4}.pdb {0}/{1}/Region_{3}_{4}/{2}_{1}_{3}_{4}.pdb 2>/dev/null'.format(outputDir, chains, targetname, ress_pre[0], ress_pre[len(ress_pre)-1]))# copyfile('{0}/{2}_{1}_{3}_PRE.pdb'.format(outputDir, chains, targetname, resseqs), '{0}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress_pre[0], ress_pre[len(ress_pre)-1]))
    else:
        logging.info('There are two other regions BEFORE and AFTER the previous region.')
        chain_not_aligned_POST = open('{0}/{1}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress_post[0], ress_post[len(ress_post) - 1]), 'w+')
        chain_not_aligned_PRE = open('{0}/{1}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress_pre[0], ress_pre[len(ress_pre) - 1]), 'w+')

        for i in range(len(ress_post)):
            for line in pl:
                if line[22:26] != '':
                    if line[0:6] == "ATOM  ":
                        if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == ress_post[i]:  # remove whitespace from a sting
                            chain_not_aligned_POST.write(line)
        chain_not_aligned_POST.close()
        #chain_not_aligned_PRE = open('{0}/{1}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress_pre[0], ress_pre[len(ress_pre) - 1]), 'w+')
        for i in range(len(ress_pre)):
            for line in pl:
                if line[22:26] != '':
                    if line[0:6] == "ATOM  ":
                        if line[22:26].translate(dict.fromkeys(map(ord, whitespace))) == ress_pre[i]:  # remove whitespace from a sting
                            chain_not_aligned_PRE.write(line)
        chain_not_aligned_PRE.close()
        create_second_region_folder(outputDir, chains, align, ress_pre, mustang_align)
        create_second_region_folder(outputDir, chains, align, ress_post, mustang_align)
        os.system('mv {0}/{1}/{2}_{1}_{3}_{4}.pdb {0}/{1}/Region_{3}_{4}/{2}_{1}_{3}_{4}.pdb 2>/dev/null'.format(outputDir, chains, targetname, ress_pre[0], ress_pre[len(ress_pre)-1]))# copyfile('{0}/{2}_{1}_{3}_PRE.pdb'.format(outputDir, chains, targetname, resseqs), '{0}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress_pre[0], ress_pre[len(ress_pre)-1]))
        os.system('mv {0}/{1}/{2}_{1}_{3}_{4}.pdb {0}/{1}/Region_{3}_{4}/{2}_{1}_{3}_{4}.pdb 2>/dev/null'.format(outputDir, chains, targetname, ress_post[0], ress_post[len(ress_post)-1]))# copyfile('{0}/{2}_{1}_{3}_PRE.pdb'.format(outputDir, chains, targetname, resseqs), '{0}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress_pre[0], ress_pre[len(ress_pre)-1]))

    return ress_pre, ress_post


def iterative_core(outputDir, targetname, standard, align, mustang_align, ress, chains, SRUL, repeatsDB, xcent, tm_score, other_region, res_ins, threshold_unit, ins_threshold, trash_unit, region_ids):#, intermediate_file_reg_2):
    if not ress:
        return
    print('REGION_{0}_{1}'.format(ress[0], ress[len(ress)-1]))
    logging.info('Find the units of the region {0}-{1}...'.format(ress[0], ress[len(ress)-1]))
    for j in os.listdir(SRUL):
        os.system('{0} {1}/{2} {3}/{4}/Region_{8}_{9}/{5}_{4}_{8}_{9}.pdb >> {7}/{4}/Region_{8}_{9}/tm_output.txt'.format(TMalignexe, SRUL, j, outputDir, chains, targetname, 0, align, ress[0], ress[len(ress)-1]))
    fl = open('{0}/{1}/Region_{2}_{3}/tm_output.txt'.format(align, chains, ress[0], ress[len(ress)-1])).readlines()
    max_score = master_choice(fl)
    df = pd.DataFrame({'unit': max_score})
    df = pd.DataFrame(df.unit.str.split('\n', 1).tolist(), columns=['unit', 'tm_score'])
    df.unit = df.unit.apply(lambda x: x.strip())
    df['tm_score'] = df['tm_score'].astype(float)
    df.to_csv('{0}/{1}/Region_{2}_{3}/prova_df_max_tmscore.txt'.format(outputDir, chains, ress[0], ress[len(ress)-1]), sep='\t', index=False)
    tm_score_max = df['tm_score'].max()
    master = df[df['tm_score'] == tm_score_max]
    master_unit = master.iloc[0]['unit']
    # if repeatsDB:
    #     os.system('{0} {1}/{2} {3}/{4}/Region_{8}_{9}/{5}_{4}_{8}_{9}.pdb -o {3}/{4}/Region_{8}_{9}/{5}_{4}_{8}_{9}.sup >> {7}/{4}/Region_{8}_{9}/tm_output_master.txt'.format(TMalignexe, SRUL, master_unit, outputDir, chains, targetname, 0, align, ress[0], ress[len(ress) - 1]))
    #     pl = open('{0}/{1}/Region_{3}_{4}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress[0], ress[len(ress)-1])).readlines()
    #     pdb_chain = PDBParser().get_structure('{0}_{1}_{2}_{3}'.format(targetname, chains, ress[0], ress[len(ress)-1]), '{0}/{1}/Region_{3}_{4}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress[0], ress[len(ress)-1]))
    # else:
    os.system('{0} {1}/{2} {3}/{4}/Region_{8}_{9}/{5}_{4}_{8}_{9}.pdb >> {7}/{4}/Region_{8}_{9}/tm_output_master.txt'.format(TMalignexe, SRUL, master_unit, outputDir, chains, targetname, 0, align, ress[0], ress[len(ress) - 1]))
    pl = open('{0}/{1}/Region_{3}_{4}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress[0], ress[len(ress)-1])).readlines()
    pdb_chain = PDBParser().get_structure('{0}_{1}_{2}_{3}'.format(targetname, chains, ress[0], ress[len(ress)-1]), '{0}/{1}/Region_{3}_{4}/{2}_{1}_{3}_{4}.pdb'.format(outputDir, chains, targetname, ress[0], ress[len(ress)-1]))

    pre = open('{0}/{1}/Region_{2}_{3}/{4}_{1}_pre_0.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), 'w+')
    post = open('{0}/{1}/Region_{2}_{3}/{4}_{1}_post_0.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), 'w+')
    master_pdb = open('{0}/{1}/Region_{2}_{3}/{4}_{1}_temp.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), 'w+')
    file_tm_master = open('{0}/{1}/Region_{2}_{3}/tm_output_master.txt'.format(align, chains, ress[0], ress[len(ress)-1])).readlines()

    # print(ress)
    resseqs = [residue.id[1] for residue in pdb_chain.get_residues()]
    # print(resseqs)
    master_seq = master_on_pdb(file_tm_master)
    start_seq = num_gap_pre_seq(master_seq, file_tm_master)
    in_seq = num_gap_in_seq(master_seq, file_tm_master)
    tot_gap = start_seq + in_seq

    pre_master = pre_master_gaiden(pl, pre, resseqs, master_seq, start_seq)
    post_master = post_master_gaiden(pl, post, resseqs, master_seq, tot_gap)
    protein_master = master_gaiden(pl, master_pdb, resseqs, master_seq, start_seq, tot_gap, no_master=True)
    coverage_master = find_coverage(file_tm_master)
    RMSD_master = extract_RMSD(file_tm_master)
    in_master = gap_master(file_tm_master, master_seq)
    percentage = (int(xcent) * len(master_seq)) / 100
    intermediate_file_reg_2 = []
    print('PM', protein_master)
    print('tot_res', ress)

    # if not pre_master:
    #     resseqs_pre=[]
    # else:
    #     pdb_pre_m = PDBParser().get_structure('{0}_{1}_{2}_{3}_pre'.format(targetname, chains, ress[0], ress[len(ress)-1]), '{0}/{1}/Region_{2}_{3}/{4}_{1}_pre_0.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname))
    #     resseqs_pre = [residue.id[1] for residue in pdb_pre_m.get_residues()]
    # if not post_master:
    #     resseqs_post=[]
    # else:
    #     pdb_post_m = PDBParser().get_structure('{0}_{1}_{2}_{3}_post'.format(targetname, chains, ress[0], ress[len(ress)-1]), '{0}/{1}/Region_{2}_{3}/{4}_{1}_post_0.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname))
    #     resseqs_post = [residue.id[1] for residue in pdb_post_m.get_residues()]

    if not pre_master:
        if len(post_master) >= percentage:
            logging.info('Once aligned the master against the whole chain, enough length remains AFTER the "first unit".')
            logging.info('Realign on the chain remained...')
            intermediate_file_reg_2.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(int(resseqs[master_seq[0] - start_seq]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]) - int(resseqs[master_seq[0] - start_seq]), float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))

            iterative_function('{0}/{1}/Region_{2}_{3}/{4}_{1}_temp.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), '{0}/{1}/Region_{2}_{3}/{4}_{1}_post_0.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), post_master, '{0}/{2}/Region_{1}_{3}'.format(align, ress[0], chains, ress[len(ress)-1]), '{0}/{1}/Region_{2}_{3}'.format(mustang_align, chains, ress[0], ress[len(ress)-1]), chains, targetname, intermediate_file_reg_2, percentage, other_region=True)
        else:
            logging.info('There is NOT enough chain to align more than "first unit".')
            print('No region length enough to analyze')
            pass

    elif not post_master:
        if len(pre_master) >= percentage:
            logging.info('Once aligned the master against the whole chain, enough length remains BEFORE the "first unit".')
            logging.info('Realign on the chain remained...')
            iterative_function('{0}/{1}/Region_{2}_{3}/{4}_{1}_temp.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), '{0}/{1}/Region_{2}_{3}/{4}_{1}_pre_0.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), pre_master, '{0}/{2}/Region_{1}_{3}'.format(align, ress[0], chains, ress[len(ress)-1]), '{0}/{1}/Region_{2}_{3}'.format(mustang_align, chains, ress[0], ress[len(ress)-1]), chains, targetname, intermediate_file_reg_2, percentage, other_region=True)
            intermediate_file_reg_2.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(int(resseqs[master_seq[0] - start_seq]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]) - int(resseqs[master_seq[0] - start_seq]), float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))
        else:
            logging.info('There is NOT enough chain to align more than "first unit".')
            print('No region length enough to analyze')
            pass

    else:
        if len(pre_master) >= percentage and len(post_master) >= percentage:
            logging.info('Once aligned the master against the whole chain, enough length remains BEFORE and AFTER the "first unit".')
            logging.info('Realign on the chain remained...')
            iterative_function('{0}/{1}/Region_{2}_{3}/{4}_{1}_temp.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), '{0}/{1}/Region_{2}_{3}/{4}_{1}_pre_0.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), pre_master, '{0}/{2}/Region_{1}_{3}'.format(align, ress[0], chains, ress[len(ress)-1]), '{0}/{1}/Region_{2}_{3}'.format(mustang_align, chains, ress[0], ress[len(ress)-1]), chains, targetname, intermediate_file_reg_2, percentage, other_region=True)
            intermediate_file_reg_2.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(int(resseqs[master_seq[0] - start_seq]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]) - int(resseqs[master_seq[0] - start_seq]), float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))
            iterative_function('{0}/{1}/Region_{2}_{3}/{4}_{1}_temp.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), '{0}/{1}/Region_{2}_{3}/{4}_{1}_post_0.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), post_master, '{0}/{2}/Region_{1}_{3}'.format(align, ress[0], chains, ress[len(ress)-1]), '{0}/{1}/Region_{2}_{3}'.format(mustang_align, chains, ress[0], ress[len(ress)-1]), chains, targetname, intermediate_file_reg_2, percentage, other_region=True)

        if len(pre_master) >= percentage and len(post_master) <= percentage:
            iterative_function('{0}/{1}/Region_{2}_{3}/{4}_{1}_temp.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), '{0}/{1}/Region_{2}_{3}/{4}_{1}_pre_0.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), pre_master, '{0}/{2}/Region_{1}_{3}'.format(align, ress[0], chains, ress[len(ress)-1]), '{0}/{1}/Region_{2}_{3}'.format(mustang_align, chains, ress[0], ress[len(ress)-1]), chains, targetname, intermediate_file_reg_2, percentage, other_region=True)
            intermediate_file_reg_2.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(int(resseqs[master_seq[0] - start_seq]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]) - int(resseqs[master_seq[0] - start_seq]), float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))

        if len(pre_master) <= percentage and len(post_master) >= percentage:
            intermediate_file_reg_2.append('{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7} %\tmaster_file\n'.format(int(resseqs[master_seq[0] - start_seq]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]), int(resseqs[master_seq[len(master_seq) - 1] - tot_gap]) - int(resseqs[master_seq[0] - start_seq]), float(master.iloc[0]['tm_score']), float(RMSD_master), tot_gap, in_master, coverage_master))
            iterative_function('{0}/{1}/Region_{2}_{3}/{4}_{1}_temp.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), '{0}/{1}/Region_{2}_{3}/{4}_{1}_post_0.pdb'.format(align, chains, ress[0], ress[len(ress)-1], targetname), post_master, '{0}/{2}/Region_{1}_{3}'.format(align, ress[0], chains, ress[len(ress)-1]), '{0}/{1}/Region_{2}_{3}'.format(mustang_align, chains, ress[0], ress[len(ress)-1]), chains, targetname, intermediate_file_reg_2, percentage, other_region=True)

        if len(pre_master) < percentage and len(post_master) < percentage:
            logging.info('There is NOT enough chain to align more than "first unit".')
            print('No region length enough to analyze')

    if not intermediate_file_reg_2:
        print('This region will not be analyzed because, after extrapolating the master, there is a region not length enough to be aligned. pre_master : {0}, post_master : {1}'.format(len(pre_master), len(post_master)))
        logging.info('This region will not be analyzed because, after extrapolating the master, there is a region not length enough to be aligned. pre_master : {0}, post_master : {1}'.format(len(pre_master), len(post_master)))

        intermediate_file_reg_2.append('{0}\t{1}\t{2}\t-\t-\t-\t-\t- %\tNone'.format(int(resseqs[0]), int(resseqs[len(resseqs)-1]), len(resseqs)))
        data, repeat_data = repeat_file(intermediate_file_reg_2, '{0}/{1}/Region_{2}_{3}'.format(outputDir, chains, ress[0], ress[len(ress)-1]), chains, tm_score, other_region=True)
        data_2 = data
    else:
        data, repeat_data = repeat_file(intermediate_file_reg_2, '{0}/{1}/Region_{2}_{3}'.format(outputDir, chains, ress[0], ress[len(ress)-1]), chains, tm_score, other_region=True)
        ins, data= find_insertion_between_units(data, resseqs, res_ins)
        # print(data)
        data, junk_names = remove_unit_under_threshold(data, targetname, chains, threshold_unit)
        create_file_for_mustang(data, '{0}/{1}/Region_{2}_{3}'.format(mustang_align, chains, ress[0], ress[len(ress) - 1]), resseqs, pl, targetname, chains)

        if data.empty or (len(data.START) == 1):
            data_2 = data
        else:
            len_mus_des_file = mustang_description_file('{0}/{1}/Region_{2}_{3}'.format(mustang_align, chains, ress[0], ress[len(ress)-1]), chains, targetname)
            if len_mus_des_file <= 2:
                id_ins=[]
            else:
                data, id_ins = first_mustang_to_remove_fake_unit('{0}/{1}/Region_{2}_{3}'.format(mustang_align, chains, ress[0], ress[len(ress)-1]), chains, targetname, data, repeatsDB, ins_threshold, res_ins, trash_unit)
                data.END = data.END.astype(int)

            ins_2, data_1 = find_insertion_between_units(data, resseqs, res_ins)
            data_2, ins = insertion_between_unit_manage_data(data_1, resseqs, ins_2, res_ins)
            if len(data_2) > 2:
                remove_too_far_apart_unit(data, data_2, f'{mustang_align}/{chains}/Region_{ress[0]}_{ress[len(ress)-1]}', targetname, chains)  # rimuovi dalla cartella di mustang

            all_insertion = order_all_insertion(ins, id_ins)
            ave_tmscore = average(data_2)
            if round(ave_tmscore, 2) < float(tm_score):
                print(f'This region will not be analyzed cause the mean of the tm_score of the units is less than {tm_score}: {round(ave_tmscore, 2)}')
                logging.info(f'This region will not be analyzed cause the mean of the tm_score of the units is less than 0.5: {round(ave_tmscore, 2)}')
                pass
            else:
                region_ids.append(f'{ress[0]}\t{ress[len(ress) - 1]}')
                header = assemble_repeat_file(master_unit, data_2, f'{outputDir}/{chains}/Region_{ress[0]}_{ress[len(ress)-1]}', chains, all_insertion, targetname, repeatsDB, standard)

                final_ins = last_management_ins(data_2, all_insertion)

                ############### WRITE .DB FILE #################
                make_db_file(targetname, chains, final_ins, data_2, master_seq, resseqs, f'{outputDir}/{chains}/Region_{ress[0]}_{ress[len(ress)-1]}', master_unit)

                ress_pre, ress_post = possibly_other_region(data_2, targetname, outputDir, chains, pl, resseqs, align, mustang_align)#, resseqs)

                if len(ress_pre) > int(other_region) and len(ress_post) < int(other_region):
                    logging.info('There is a region BEFORE the previous region')
                    iterative_core(outputDir, targetname, standard, align, mustang_align, ress_pre, chains, SRUL, repeatsDB, xcent, tm_score, other_region, res_ins, threshold_unit, ins_threshold, trash_unit, region_ids)#, intermediate_file_reg_2)

                if len(ress_post) > int(other_region) and len(ress_pre) < int(other_region):
                    logging.info('There is a region AFTER the previous region')
                    iterative_core(outputDir, targetname, standard, align, mustang_align, ress_post, chains, SRUL, repeatsDB, xcent, tm_score, other_region, res_ins, threshold_unit, ins_threshold, trash_unit, region_ids)#, intermediate_file_reg_2)

                if len(ress_pre) > int(other_region) and len(ress_post) > int(other_region):
                    logging.info('There is a region AFTER and BEFORE the previous region')
                    iterative_core(outputDir, targetname, standard, align, mustang_align, ress_pre, chains, SRUL, repeatsDB, xcent, tm_score, other_region, res_ins, threshold_unit, ins_threshold, trash_unit, region_ids)
                    iterative_core(outputDir, targetname, standard, align, mustang_align, ress_post, chains, SRUL, repeatsDB, xcent, tm_score, other_region, res_ins, threshold_unit, ins_threshold, trash_unit, region_ids)
                else:
                    logging.info('There is NOT other regions to analyze.')
                    print('No other regions to analyze')
                    return


def create_second_region_folder(outputDir, chains, align, ress, mustang_align):
    try:
        os.makedirs('{0}/{1}/Region_{2}_{3}'.format(align, chains, ress[0], ress[len(ress)-1]))
        os.makedirs('{0}/{1}/Region_{2}_{3}'.format(mustang_align, chains, ress[0], ress[len(ress)-1]))
        os.mkdir('{0}/{1}/Region_{2}_{3}'.format(outputDir, chains, ress[0], ress[len(ress)-1]))
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        pass