import subprocess
from configurationFile import *
import os, time
import logging, sys


def sub_tmalign(master_unit, SRUL, outputDir, chains, targetname, repeatsDB, library):
    if library is True:
        # if repeatsDB:
        #     print('{0} {3}/{2} {1}/{}'.format(TMalignexe, outputDir, master_unit, SRUL))
        #     # cmd = '{0} {1}/{2} {3}/{4}/{5}_{4}.pdb -o {3}/{4}/{5}_{4}.sup'.format(TMalignexe, SRUL, master_unit, outputDir, chains, targetname).split()
        #     cmd = '{0} {1}/{2} {1}/{3}'.format(TMalignexe, outputDir, master_unit, SRUL).split()
        # else:
        cmd = '{0} {1}/{2} {3}/{4}/{5}_{4}.pdb'.format(TMalignexe, SRUL, master_unit, outputDir, chains, targetname).split()

        with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8") as proc:
            out = proc.stdout.readlines()
            err = proc.stderr.readlines()
        if err:
            logging.error(f'{err}. Check again...')
            sys.exit()
    else:
        if repeatsDB:
            cmd = '{0} {1}/{2} {1}/{3}'.format(TMalignexe, outputDir, master_unit, SRUL).split()
        else:
            cmd = '{0} {1}/{2} {1}/{3}/{4}_{3}.pdb'.format(TMalignexe, outputDir, master_unit, chains, targetname).split()

        with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8") as proc:
            out = proc.stdout.readlines()
            err = proc.stderr.readlines()
            if err:
                logging.error(f'{err}. Check again...')
                sys.exit()
    return out


def sub_mustang(mustang_align, targetname, chains, d, repeatsDB):
    # print(mustang_align)
    # exit()
    if os.path.basename(os.path.normpath(mustang_align)) != 'mustang_alignment':
        if repeatsDB is True:
            # cmd = 'mustang ' \
            #       '-f {}/{0}_{1}_{2}/{0}_{1}_{2}_mustang_description_file ' \
            #       '-o /home/sara/Documents/pre_submission/multiple_structural_align_mustang/data/{3}/{0}_{1}_{2} ' \
            #       '-F fasta html ' \
            #       '-s ON' \
            #       '-r ON'.format(targetname, regions[k][0], regions[k][1], folder).split()
            cmd = '{0} -f {1}/{3}_{2}region{4}.mus -o {1}/{3}_{2}region{4}.mustang -F fasta -r ON '.format(mustangexe, mustang_align, chains, targetname, d).split()
            # cmd = '{0} -f {1}/mustang_description_file -o {1}/{3}{2} -F fasta -r ON'.format(mustangexe, mustang_align, chains, targetname).split()
        else:
            cmd = '{0} -f {1}/mustang_description_file -o {1}/{3}{2} -F fasta'.format(mustangexe, mustang_align, chains, targetname).split()
    else:
        # print('ciao')
        if repeatsDB is True:
            cmd = '{0} -f {1}/{3}_{2}region{4}.mus -o {1}/{3}_{2}region{4}.mustang -F fasta -r ON '.format(mustangexe, mustang_align, chains, targetname, d).split()
        else:
            cmd = '{0} -f {1}/{2}/mustang_description_file -o {1}/{2}/{3}{2} -F fasta'.format(mustangexe, mustang_align, chains, targetname).split()

    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding="utf-8")
    try:
        # Filter stdout
        stdout_mustang = open('{}/stdout_mustang_{}.log'.format(mustang_align, chains), '+w')
        for line in iter(proc.stdout.readline, ''):
            sys.stdout.flush()
            # Print status
            #print(line.rstrip())
            stdout_mustang.write('{}\n'.format(line.rstrip()))
            sys.stdout.flush()
        stdout_mustang.close()
    except:
        sys.stdout.flush()

    while proc.poll() is None:
        # Process hasn't exited yet, let's wait some
        time.sleep(0.5)

    return_code = proc.returncode

    return return_code


def manage_mustang_error(err, mustang_align, chains):
    if err == 0:
        logging.info('Mustang runned in the right way. The insertions in the unit will be discover.')
        os.remove('{}/stdout_mustang_{}.log'.format(mustang_align, chains))
    elif err == -11:
        logging.error('Mustang Error: {}'.format(err))
    else:
        logging.error('Mustang will exit with an unpredictable error: {}'.format(err))
        sys.exit(err)


def sub_blast(sequence, outputDir):
    cmd = 'blastp -query {0}.fasta -subject {1}/databasePDB.fasta -outfmt 6'.format(sequence, outputDir).split()
    with subprocess.Popen(cmd, stdout=subprocess.PIPE, encoding="utf-8") as proc:
        out = proc.stdout.readlines()
        # print(out)
        # exit()
        output_blast = open('{1}/{0}_blast_result.txt'.format(sequence, outputDir), 'w+')
        for i in out:
            output_blast.write(i)
        output_blast.close()
    return out


def sub_mtmalign(outputDir, targetname, chains, d, dirlist):
    # print(outputDir)
    os.system(f'cp {os.path.dirname(outputDir)}/Region{d}/{targetname}_{chains}region{d}.mus {outputDir}/{targetname}_{chains}region{d}.mus')
    os.chdir(outputDir)
    cmd = '/opt/mTM-align/mTM-align ' \
          '-i {}_{}region{}.mus'.format(targetname, chains, d).split()
    proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, encoding="utf-8")

    while proc.poll() is None:
        # Process hasn't exited yet, let's wait some
        time.sleep(0.5)

    return_code = proc.returncode
    return return_code

